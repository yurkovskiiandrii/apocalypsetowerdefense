
[System.Serializable]
public class StateInfo
{
    public string stateName;
    public float stateValue;

    public StateInfo Clone()
    {
        return new StateInfo()
        {
            stateName = this.stateName,
            stateValue = this.stateValue
        };
    }
}
