using UnityEngine;
using UnityEngine.Events;

public class UpgradeManager : SingletonComponent<UpgradeManager>
{
    [SerializeField] private LevelManager _levelManager;
    private UnityEvent _onUpgrade = new UnityEvent();
    private UnityEvent _onUpgradeEnd = new UnityEvent();


    public bool CanBeUpgraded(LevelProgressionConfig config)
    {
        return config.CanBeUpgraded();
    }

    public void UpgradeForCoins(int priceCoins, LevelProgression progression)
    {
        UpdateResourses(priceCoins);
        progression.open = true;
        _onUpgrade?.Invoke();
        _onUpgradeEnd?.Invoke();
    }

    public void SubscribeOnUpgrade(UnityAction callBack) => _onUpgrade.AddListener(callBack);

    public void SubscribeOnUpgradeEnd(UnityAction callBack) => _onUpgradeEnd.AddListener(callBack);


    public bool IsEnoughMoney(int price)
    {
        return _levelManager.IsEnoughMoney(price);
    }

    public void UpdateResourses(int priceCoins)
    {
        _levelManager.UpdateResourses(priceCoins);
    }
}
