
public static class GameStatesKey
{
    public static string MaxHp => "MaxHp";
    public static string Armor => "Armor";
    public static string Speed => "Speed";
    public static string Damage => "Damage";
    public static string AttackRange => "AttackRange";
    public static string AttackCooldown => "AttackCooldown";
    public static string RecoveryTime => "RecoveryTime";
    public static string SkillTime => "SkillTime";
    public static string SkillCooldown => "SkillCooldown";
    public static string ActivationTime => "ActivationTime";
    public static string DetonationTime => "DetonationTime";
}
