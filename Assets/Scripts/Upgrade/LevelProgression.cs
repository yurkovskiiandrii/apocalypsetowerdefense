using System.Collections.Generic;

[System.Serializable]
public class LevelProgression
{
    public int priceCoins;
    public bool open;
    public List<StateInfo> states = new List<StateInfo>();
    //public DefenderUpgrade defendersUpgradeData;
    //public PropUpgrade propsUpgradeData;
   
    public LevelProgression Clone()
    {
        return new LevelProgression()
        {
            priceCoins = this.priceCoins,
            open = this.open,
            states = this.states
        };
    }

    public float MaxHp => GetValueState(GameStatesKey.MaxHp);
    public float Armor => GetValueState(GameStatesKey.Armor);
    public float Speed => GetValueState(GameStatesKey.Speed);
    public float Damage => GetValueState(GameStatesKey.Damage);
    public float AttackRange => GetValueState(GameStatesKey.AttackRange);
    public float AttackCooldown => GetValueState(GameStatesKey.AttackCooldown);
    public float RecoveryTime => GetValueState(GameStatesKey.RecoveryTime);
    public float SkillTime => GetValueState(GameStatesKey.SkillTime);
    public float SkillCooldown => GetValueState(GameStatesKey.SkillCooldown);
    public float ActivationTime => GetValueState(GameStatesKey.ActivationTime);
    public float DetonationTime => GetValueState(GameStatesKey.DetonationTime);

    public float GetValueState(string stateName)
    {
        var index = states.FindIndex(x => x.stateName == stateName);

        if (index == -1)
            return 0;
        return states[index].stateValue;
    }

}
