using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class DamagePopup : MonoBehaviour
{   
    private float _popupTime = 0.2f;//0.5f

    private TMP_Text _text;
    private Transform _transform;
    private Vector3 _originalPosition;

    private bool _isEnabled = true;

    private void Awake()
    {
        _transform = transform;
        _text = GetComponent<TMP_Text>();
        _text.enabled = false;
        _originalPosition = transform.localPosition;
    }

    public void Show(float damage)
    {
        if (!gameObject.activeSelf)
            return;

        _text.enabled = true;
        _transform.LookAt(Camera.main.transform);
        _text.text = damage.ToString();
        if (_isEnabled)
            StartCoroutine(ShowCoroutine());       
    }
    private IEnumerator ShowCoroutine()
    {
        //_transform.DOMoveY(_transform.position.y + 1f, _popupTime);
     //   _transform.DOScale(newScale, _popupTime);
        yield return new WaitForSeconds(_popupTime);
        _text.text = "";
        _text.enabled = false;
        _transform.localPosition = _originalPosition;
        yield return null;
        // _transform.DOScale(oldScale, 0);
    }

    private void OnDisable()
    {
        _isEnabled = false;
        _text.enabled = false;
        StopAllCoroutines();
        _transform.localPosition = _originalPosition;       
    }
}
