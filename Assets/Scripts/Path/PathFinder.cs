using System.Collections.Generic;
using UnityEngine;

public class PathFinder : SingletonComponent<PathFinder>
{
    private List<Cell> _map = new List<Cell>();
    bool _haveMap = false;

    private List<PathCell> _openList = new List<PathCell>();
    private List<PathCell> _closedList = new List<PathCell>();

    private PathCell _lastPathCell;

    public void SetMap(List<Cell> map)
    {
        _map = map;
        _haveMap = true;
    }

    public bool CheckPathPassability(List<Vector3> path)
    {
        foreach (var point in path)
        {
            Cell cell = TerrainGrid.Instance.GetCell(TerrainGrid.Instance.GetGridPosition(point));
            if(!cell.PathCell.isWalkable)
                return false;
        }
        return true;
    }


    public List<Vector3> GetVectorPath(Cell startCell, Cell endCell)
    {
        PathCell startPathCell = null;
        PathCell endPathCell = null;


        if (startCell == null || endCell == null)
            return null;

        foreach (var item in _map)
        {
            if (item.Position == endCell.Position)
                endPathCell = item.PathCell;
            if (item.Position == startCell.Position)
                startPathCell = item.PathCell;
        }

        if (startPathCell == null || endPathCell == null)
            return null;

        var path = FindPath(startPathCell, endPathCell);
        if (path == null)
            return null;
        else
        {
            List<Vector3> vectorPath = new List<Vector3>();
            foreach (PathCell pathCell in path)
            {
                vectorPath.Add(TerrainGrid.Instance.GetCellPosition(pathCell.cell.Position));
            }
            return vectorPath;
        }
    }

    private List<PathCell> FindPath(PathCell startCell, PathCell endCell)
    {
        if (startCell == null || endCell == null || !_haveMap)
        {
            return null;
        }

        _openList = new List<PathCell> { startCell };
        _closedList = new List<PathCell>();

        foreach (var cell in _map)
        {
            cell.PathCell.gCost = int.MaxValue;
            cell.PathCell.CalculateFCost();
            cell.PathCell.cameFromCell = null;
        }

        startCell.gCost = 0;
        startCell.hCost = CalculateDistanceCost(startCell, endCell);
        startCell.CalculateFCost();

        _lastPathCell = startCell;

        while (_openList.Count > 0)
        {
            PathCell currentCell = GetLowestFCostNode(_openList);
            if (currentCell == endCell)
            {
                // Reached final node
                return CalculatePath(endCell);
            }

            _openList.Remove(currentCell);
            _closedList.Add(currentCell);

            if(_lastPathCell.hCost > currentCell.hCost)
                _lastPathCell = currentCell;

            foreach (PathCell neighbourCell in GetNeighbourList(currentCell))
            {
                if (_closedList.Contains(neighbourCell)) continue;
                if (!neighbourCell.isWalkable && neighbourCell != endCell)
                {
                    _closedList.Add(neighbourCell);
                    continue;
                }

                int tentativeGCost = currentCell.gCost + CalculateDistanceCost(currentCell, neighbourCell);
                if (tentativeGCost < neighbourCell.gCost)
                {
                    neighbourCell.cameFromCell = currentCell;
                    neighbourCell.gCost = tentativeGCost;
                    neighbourCell.hCost = CalculateDistanceCost(neighbourCell, endCell); neighbourCell.CalculateFCost();

                    if (!_openList.Contains(neighbourCell))
                    {
                        _openList.Add(neighbourCell);
                    }
                }
            }
        }
        //path to obstacle
        //PathCell farestCell = GetLowestFCostNode(_closedList);
        return CalculatePath(_lastPathCell);

        // Out of nodes on the openList
        //return null;
    }

    private List<PathCell> GetNeighbourList(PathCell current�ell)
    {
        if (current�ell.neighbors.Count == 0)
            current�ell.SetNeighbours();

        return current�ell.neighbors;
    }

    private List<PathCell> CalculatePath(PathCell endCell)
    {
        List<PathCell> path = new List<PathCell>();
        path.Add(endCell);
        PathCell currentCell = endCell;
        while (currentCell.cameFromCell != null)
        {
            path.Add(currentCell.cameFromCell);
            currentCell = currentCell.cameFromCell;
        }
        path.Reverse();
        return path;
    }

    private PathCell GetLowestFCostNode(List<PathCell> pathCellList)
    {
        PathCell lowestFCostCell = pathCellList[0];
        for (int i = 1; i < pathCellList.Count; i++)
        {
            if (pathCellList[i].fCost < lowestFCostCell.fCost)
            {
                lowestFCostCell = pathCellList[i];
            }
        }
        return lowestFCostCell;
    }


    private int CalculateDistanceCost(PathCell first, PathCell second)
    {
        return (int)Vector2.Distance(first.cell.Position, second.cell.Position);
    }
}
