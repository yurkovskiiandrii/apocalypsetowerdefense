using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Ragdoll;
using Units;

public class Mine : Prop
{
    private const string EnemyTag = "Invader";

    [SerializeField] private TMP_Text _timer;
    [SerializeField] private Slider _timerProgressBar;
    [SerializeField] private SphereCollider _collider;

    [SerializeField] private float _activationTime;
    [SerializeField] private float _detonationTime;
    [SerializeField] private float _explosionDamage;

    [SerializeField] private int _price;

    [SerializeField] private DamageType _damageType;//Magic
    [SerializeField] private RagdollPowerType _ragdollPowerType;
    [SerializeField] private RagdollParams _ragdollParams;

    [SerializeField] private UnityEvent onActivated;
    [SerializeField] private UnityEvent onExplosion;

    private List<Unit> _invadersInArea = new List<Unit>();

    private bool _isActivate = false;
    private bool _isDetonationStart = false;
    private RagdollConfig _ragdollConfig;

    public override void UpdateParameners()
    {
        base.UpdateParameners();
        var currentProgress = _currentUpgradeConfig.GetCurrentProgress();
        _activationTime = currentProgress.ActivationTime;
        _detonationTime = currentProgress.DetonationTime;
        _explosionDamage = currentProgress.Damage;
    }


    public override void Init(Action OnDestroy)
    {
        Debug.Log("Init MINE");
        base.Init(OnDestroy);

        StartCoroutine(TimerBrogressBarCoroutine(_activationTime));
        StartCoroutine(TimerCoroutine((int)_activationTime));
        Invoke(nameof(Activate), _activationTime);
        _ragdollConfig = RagdollConfig.Instance;
        _collider.radius = _ragdollParams.ExplosionRange;
        //Debug.Log("Init MINE End");
    }

    private IEnumerator TimerBrogressBarCoroutine(float time)
    {
        _timerProgressBar.value = 0;
        _timerProgressBar.gameObject.SetActive(true);
        float progressPart = 0.1f / time;

        while (time > 0)
        {
            _timerProgressBar.value += progressPart;
            time -= 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        _timerProgressBar.gameObject.SetActive(false);
    }

    private IEnumerator TimerCoroutine(int time)
    {
        //print("timer cor " +time);
        _timer.gameObject.SetActive(true);

        while (time > 0)
        {
            _timer.text = time.ToString();
            time--;
            yield return new WaitForSeconds(1f);
        }
        _timer.gameObject.SetActive(false);
    }

    private void Activate()
    {
        _isActivate = true;
        _collider.enabled = true;
        onActivated?.Invoke();
        //Debug.Log("MINE isActivate ");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(EnemyTag) && _isActivate)
        {
            if (!_isDetonationStart)
            {
                Invoke(nameof(Explosion), _detonationTime);
                _isDetonationStart = true;
            }
            //print("MINE OnTriggerEnter - enemy: " + other.gameObject.name);
            _invadersInArea.Add(other.GetComponent<Unit>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(EnemyTag) && _isActivate)
        {
            //print("MINE OnTriggerExit - enemy: " + other.gameObject.name);
            _invadersInArea.Remove(other.GetComponent<Unit>());
        }
    }

    private void Explosion()
    {
        onExplosion?.Invoke();

        foreach (var invader in _invadersInArea)
        {
            //print("MINE isDetonation - UseRagdoll > enemy: " + invader.gameObject.name);

            if (invader.RagdollController.HasRagdoll())
            {
                var ragdollPower = _ragdollConfig.GetCurrentRagdollPower(_ragdollPowerType, invader.RagdollController.GetMassObjectType());
                if (ragdollPower != 0)
                    invader.UseRagdoll(ragdollPower, transform.position, _ragdollParams.ExplosionRange, _ragdollParams.UpwardsModifier);
            }
            invader.GetDamage(_explosionDamage, _damageType);
        }
        onDestroy?.Invoke();
        DestroyProp();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _ragdollParams.ExplosionRange);
    }
}
