using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Units;

public class DamagingPoint : MonoBehaviour
{
    const string PropsTag = "Props";
    const string PropsLayer = "Props";

    private bool _isBusy;
    public bool IsBusy => _isBusy;

    private Unit _unit;
    public Unit Unit => _unit;

    private bool _isInCollider = false;

    public void Init()
    {
        gameObject.SetActive(!_isInCollider);
    }

    public void SetUnit(Unit unit)
    {
        _unit = unit;
        _isBusy = true; 
    }

    public void RemoveUnit()
    {
        _unit = null;
        _isBusy = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PropsTag) && other.gameObject.layer == LayerMask.NameToLayer(PropsLayer))
        {
            //print("enter props");
            _isInCollider = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(PropsTag) && other.gameObject.layer == LayerMask.NameToLayer(PropsLayer))
        {
            //print("out props");
            _isInCollider = false;
        }
    }
}
