using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Pool;
using UnityEngine.UI;
using Units;

public class Prop : MonoBehaviour
{
    private const string ThisLayer = "Props";
    private const string PassableLayer = "PassableUnit";

    [SerializeField] private UnityEvent onDamageGet;
    [SerializeField] private UnityEvent onPlace;
    [SerializeField] private UnityEvent onDead;

    [SerializeField] private GameObject _normalAppearence;
    [SerializeField] private GameObject _destroyedAppearence;

    [SerializeField] private Transform _damagingPointParent;

    [Header("Placement Stats")]
    [SerializeField] protected Material _placementManterial;
    [SerializeField] protected Material _mainMaterial;

    [Header("UI Options")]
    [SerializeField] private float _hpShowTime = 1f;
    [SerializeField] private bool _showHpAlways = true;
    [SerializeField] private UnitGameUI _gameUIPrefab;
    [SerializeField] protected DefenderOptions _defenderOptions;
    [SerializeField] protected LevelProgressionConfig _upgradeConfig;
    [SerializeField] protected float _maxHealthPoints;
    [SerializeField] protected ArmorType _armorType;
    [SerializeField] protected float _armor;
    [SerializeField] protected bool _isAttackable;
    [SerializeField] protected bool _isUpgradeMenu;
    [SerializeField] protected Vector3 _gameUIPosition = new Vector3(1, 2.5f, 0);

    protected float _currentHealthPoints;
    protected UnitGameUI _gameUI;
    protected Slider _hpSlider;
    protected LevelProgressionConfig _currentUpgradeConfig;
    protected Action onDestroy;

    private Transform _transform;
    private List<Unit> _attackingUnits = new List<Unit>();
    private List<DamagingPoint> _damagingPoints = new List<DamagingPoint>();
    private List<DamagingPoint> _activeDamagingPoints;
    public List<DamagingPoint> DamagingPoints => _activeDamagingPoints;
    public DefenderOptions DefenderOptions => _defenderOptions;
    public List<Unit> AttackingUnits => _attackingUnits;
    public ObjectPool<UnitGameUI> GameUIObjectPool { get; set; }
    public LevelProgressionConfig UpgradeConfig => _currentUpgradeConfig;
    public float MaxHealthPoints => _maxHealthPoints;
    public bool IsAttackable => _isAttackable;
    public bool IsInit { get; private set; }

    protected void Awake()
    {
        IsInit = false;

        _transform = transform;
        foreach (Transform child in _damagingPointParent)
        {
            if (child.TryGetComponent(out DamagingPoint point))
            {
                _damagingPoints.Add(point);
                point.gameObject.SetActive(true);
                point.Init();
            }
        }
        _activeDamagingPoints = _damagingPoints.FindAll(p => p.gameObject.activeSelf);

        if (_isUpgradeMenu)
        {
            InitUpgradeParameters();
        }
    }

    public virtual void UpdateParameners()
    {
        var currentProgress = _currentUpgradeConfig.GetCurrentProgress();
        _maxHealthPoints = currentProgress.MaxHp;
        _armor = currentProgress.Armor;
    }

    protected virtual void InitUpgradeParameters()
    {
        _currentUpgradeConfig = _upgradeConfig.Clone();
        UpdateParameners();
        UpgradeManager.Instance.SubscribeOnUpgrade(UpdateParameners);
    }

    public virtual void Init(Action OnDestroy)
    {
        _currentHealthPoints = _maxHealthPoints;
        //Debug.Log("Init Prop");
        _transform = transform;
        onDestroy = OnDestroy;

        foreach (var point in _damagingPoints)
        {
            point.gameObject.SetActive(true);
            point.Init();
        }

        _activeDamagingPoints = _damagingPoints.FindAll(p => p.gameObject.activeSelf);

        SetMainMaterial();

        if (_showHpAlways && _gameUI == null)
        {
            _gameUI = GameUIPoolManager.Instance.GetObjectFromPool<UnitGameUI>(_gameUIPrefab);
            SetGameUI(_gameUI);
        }
        onPlace.Invoke();

        IsInit = true;
    }

    public virtual void SetGameUI(UnitGameUI gameUI)
    {
        _gameUI = gameUI;
        _gameUI.transform.SetParent(_transform);
        _gameUI.transform.localPosition = _gameUIPosition;
        _hpSlider = _gameUI.GetSliderHpBar();
        _hpSlider.value = _hpSlider.maxValue;
    }

    public void SetPlacementMode()
    {
        ChangeMaterial(_placementManterial);
        if (_normalAppearence != null)
            foreach (Transform child in _normalAppearence.transform)
            {
                child.gameObject.layer = LayerMask.NameToLayer(PassableLayer);
            }
    }

    public void SetMainMaterial()
    {
        ChangeMaterial(_mainMaterial);
        if (_normalAppearence != null)
            foreach (Transform child in _normalAppearence.transform)
            {
                child.gameObject.layer = LayerMask.NameToLayer(ThisLayer);
            }
    }

    protected void ChangeMaterial(Material material)
    {
        if (_transform.gameObject.TryGetComponent(out MeshRenderer transformRenderer))
        {
            transformRenderer.material = material;
        }

        foreach (Transform child in _transform)
        {
            if (child.gameObject.layer == LayerMask.NameToLayer(ThisLayer) && child.gameObject.TryGetComponent(out MeshRenderer renderer))
            {
                renderer.material = material;
            }
        }
    }

    public bool IsAlive() => _currentHealthPoints > 0;

    public virtual void GetDamage(float damage, DamageType damageType)
    {
        if (!IsAlive())
            return;

        onDamageGet.Invoke();
        _currentHealthPoints -= damage;
        _currentHealthPoints = Mathf.Clamp(_currentHealthPoints, 0, _maxHealthPoints);

        //Debug.Log(gameObject.name + "-----------------------------Prop  ***** GetDamage = -" +damage + " / _currentHealthPoints = " + _currentHealthPoints);
        //Debug.Log(gameObject.name + "-----------------------------Prop  ***** GetDamage/ _gameUI == null /SetNewUI" + (_gameUI == null));

        if (!_showHpAlways)
            StartCoroutine(ShowHP());

        int hpRocentage = (int)(_currentHealthPoints / _maxHealthPoints * 100f);
        _hpSlider.value = hpRocentage / 100f;

        CheckDeath();
    }

    private IEnumerator ShowHP()
    {
        _hpSlider.gameObject.SetActive(true);
        yield return new WaitForSeconds(_hpShowTime);
        _hpSlider.gameObject.SetActive(false);
    }

    public void CheckDeath()
    {
        if (_currentHealthPoints <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        IsInit = false;
        //_synchronizer.Tick();
        //onDie?.Invoke();
        //onDie = null;

        if (_gameUI != null)
        {
            GameUIPoolManager.Instance.ReleaseObjectToPool(_gameUIPrefab.gameObject, _gameUI);
            _gameUI = null;
        }

        if (_normalAppearence != null && _destroyedAppearence != null)
        {
            _normalAppearence.SetActive(false);
            _destroyedAppearence.SetActive(true);
        }
        onDestroy?.Invoke();
        onDead?.Invoke();
        //Debug.Log(gameObject.name + "-----------------------------Prop  *****  Die()");
    }

    public DamagingPoint SetAttackingUnit(Unit unit)
    {
        if (_attackingUnits.Count < _activeDamagingPoints.Count)
        {
            _attackingUnits.Add(unit);
            List<DamagingPoint> freePoints = _activeDamagingPoints.FindAll(x => x.IsBusy == false);
            DamagingPoint damagingPoint = freePoints[0];
            foreach (DamagingPoint point in freePoints)
            {
                if (Vector3.Distance(point.transform.position, unit.transform.position) < Vector3.Distance(damagingPoint.transform.position, unit.transform.position))
                    damagingPoint = point;
            }
            damagingPoint.SetUnit(unit);
            unit.SubscribeOnDeath(RemoveAttackingUnit);
            unit.SubscribeOnDeath(damagingPoint.RemoveUnit);
            return damagingPoint;
        }
        return null;
        //   _attackingUneit = unit;
    }

    public bool IsThereFreePoint()
    {
        return _attackingUnits.Count < _activeDamagingPoints.Count;
    }

    public DamagingPoint GetNearestPoint(Vector3 position)
    {
        DamagingPoint nearest = _activeDamagingPoints[0];
        foreach (var point in _activeDamagingPoints)
        {
            if (Vector3.Distance(nearest.transform.position, position) > Vector3.Distance(point.transform.position, position))
                nearest = point;
        }
        return nearest;
    }

    private void RemoveAttackingUnit(Unit unit)
    {
        if (unit != null)
            _attackingUnits.Remove(unit);
    }

    public void SubscribeOnDeath(UnityAction onDeath)
    {
        onDead.AddListener(onDeath);
    }

    protected void DestroyProp()
    {
        IsInit = false;
        Destroy(gameObject);
    }
    //private void OnDestroy()
    //{
    //if (TerrainGrid.Instance != null)
    //    TerrainGrid.Instance.UpdateNavMeshGrid();
    //}
}
