using UnityEngine;

public class Follower : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public float followSpeed = 5f;
    public float followDelay = 0.5f;

    private Vector3 targetPosition;

    void Start()
    {
    
        targetPosition = target.position + offset;
    }

    void Update()
    {
        if (target != null)
        {
            targetPosition = Vector3.Lerp(targetPosition, target.position + offset, Time.deltaTime * followSpeed / followDelay);
            transform.position = targetPosition;
        }
    }
}
