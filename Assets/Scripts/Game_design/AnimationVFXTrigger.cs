using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AnimationVFXTrigger : MonoBehaviour
{
    [Header("References")]
    public Animator animator;
    public ParticleSystem vfx;
    public GameObject projectilePrefab;
    public Transform projectileSpawnPoint;
    public Button triggerButton;

    [Header("Projectile Settings")]
    public string animationTrigger = "Attack"; // Назва тріггера для анімації
    public float projectileSpeed = 10f;
    public float projectileDelay = 0.2f;
    public float projectileLifetime = 3f;
    public Vector3 projectileRotationOffset = Vector3.zero; // Офсет ротації проджектайлу

    [Header("Loop Settings")]
    public bool loop = false;

    private void Start()
    {
        if (triggerButton != null)
            triggerButton.onClick.AddListener(TriggerAction);
    }

    public void TriggerAction()
    {
        animator.SetTrigger(animationTrigger);
        if (vfx != null)
            vfx.Play();

        StartCoroutine(FireProjectileWithDelay());

        if (loop)
            Invoke(nameof(TriggerAction), animator.GetCurrentAnimatorStateInfo(0).length);
    }

    private IEnumerator FireProjectileWithDelay()
    {
        yield return new WaitForSeconds(projectileDelay);

        if (projectilePrefab != null && projectileSpawnPoint != null)
        {
            // Коригуємо ротацію
            Quaternion finalRotation = projectileSpawnPoint.rotation * Quaternion.Euler(projectileRotationOffset);
            
            GameObject projectile = Instantiate(projectilePrefab, projectileSpawnPoint.position, finalRotation);
            Rigidbody rb = projectile.GetComponent<Rigidbody>();

            if (rb == null)
                rb = projectile.AddComponent<Rigidbody>();

            rb.useGravity = false;
            
            // Випускаємо вперед відносно спавн-точки
            rb.velocity = projectileSpawnPoint.TransformDirection(Vector3.forward) * projectileSpeed;

            Destroy(projectile, projectileLifetime);
        }
    }
}
