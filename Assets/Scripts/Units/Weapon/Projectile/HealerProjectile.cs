using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Units;

namespace Projectile
{
    public class HealerProjectile : BaseProjectile
    {
        [Header("FarInDirection Projectile parameters")]
        [SerializeField] private string _targetTag = "Defender";
      
        public override void Init()
        {
            base.Init();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform == _target && other.gameObject.tag == _targetTag)
            {
                Unit unit = other.gameObject.GetComponent<Unit>();
                _onContactWithTarget?.Invoke(unit);
                StopProjectile();
                //Debug.Log("Shoot Heal Projectile + OnTriggerEnter Is Target => Invader = "+unit.gameObject.name + " / other.mame =  "+ other.name + "  //// "+gameObject.name);
            }
        }

        public override void StartShot(Vector3 position, Quaternion rotation, Action OnReachTarget, Transform target)
        {
            //Debug.Log(" Start Unit Projectile ");

            base.StartShot(position, rotation, OnReachTarget, target);

            StartShootCoroutine(position, target.position);
        }

        private void StartShootCoroutine(Vector3 position, Vector3 targetPosition)
        {
            _transform.DOMove(targetPosition, Vector3.Distance(position, targetPosition) / _speed).SetEase(Ease.Linear);

            StartCoroutine(base.ShootCoroutine(targetPosition));
        }

        protected override void StopProjectile()
        {
            base.StopProjectile();

            _transform.DOKill();
            _target = null;
        }
    }
}
