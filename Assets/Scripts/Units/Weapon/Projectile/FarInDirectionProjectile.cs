using System;
using System.Collections;
using UnityEngine;
using Units;

namespace Projectile
{
    public class FarInDirectionProjectile : BaseProjectile
    {
        [Header("FarInDirection Projectile parameters")]
        [SerializeField] private string _targetTag = "Invader";
        [SerializeField] private SlowDebuffParams _slowDebuffParams;
        private Coroutine _moveCoroutine;
       
        public override void Init()
        {
            base.Init();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == _targetTag)
            {
                Unit unit = other.gameObject.GetComponent<Unit>();
                unit.ActivateSlowDebuff(_slowDebuffParams.TimeDebuff, _slowDebuffParams.MultiplierSlowSpeed, _slowDebuffParams.MultiplierSlowAttack);
                _onContactWithTarget?.Invoke(unit);
                //Debug.Log("Shoot FAR Projectile + OnTriggerEnter Is Target => Invader = "+unit.gameObject.name + " / other.mame =  "+ other.name + "  //// "+gameObject.name);
            }
        }

        public override void StartShot(Vector3 position, Quaternion rotation, Action OnReachTarget, Vector3 direction)
        {
            base.StartShot(position, rotation, OnReachTarget, direction);

            _moveCoroutine = StartCoroutine(MoveCoroutine());
        }

        private IEnumerator MoveCoroutine()
        {
            while (!_isTimeIsUp)
            {
                _transform.Translate(_direction * Time.deltaTime * _speed, Space.World);
                yield return null;
            }
            StopProjectile();
        }

        protected override void StopProjectile()
        {
            base.StopProjectile();
            StopProjectileCoroutine(_moveCoroutine);
        }
    }
}
