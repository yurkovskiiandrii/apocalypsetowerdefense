using System;
using System.Collections;
using UnityEngine;
using Ragdoll;
using UnityEngine.Events;
using Units;

namespace Projectile
{
    public class BaseProjectile : MonoBehaviour
    {
        [Header("Base Projectile parameters")]
        [SerializeField] protected RagdollPowerType _ragdollPowerType;
        [SerializeField] protected RagdollParams _ragdollParams;
        [SerializeField] protected float _speed = 3;
        [SerializeField] protected float _timeProjectile = 3;

        protected Transform _target;
        protected Transform _transform;
        protected Action _onReachTarget;
        protected bool _isTimeIsUp;
        protected Coroutine _timerCoroutine;
        protected WaitForSeconds _delayTimeProjectile;
        protected Vector3 _direction;
        protected RagdollConfig _ragdollConfig;
        protected UnityAction<Unit> _onContactWithTarget;

        private float _minDistance = 0.05f;

        public virtual void Init()
        {
            _transform = transform;
            _ragdollConfig = RagdollConfig.Instance;
            _delayTimeProjectile = new WaitForSeconds(_timeProjectile);
        }

        public void SetProjectileParameters(float speed)
        {
            _speed = speed;
        }

        public virtual void StartShot(Vector3 position, Quaternion rotation, Action OnReachTarget, Transform target = null)
        {
            _transform.position = position;
            _transform.localRotation = rotation;
            _target = target;
            _onReachTarget = OnReachTarget;
            _isTimeIsUp = false;
            _timerCoroutine = StartCoroutine(TimerUpdater());
        }
       
        public virtual void StartShot(Vector3 position, Quaternion rotation, Action OnReachTarget, Vector3 direction)
        {
            StartShot(position, rotation, OnReachTarget);
            _direction = direction;
        }

        protected virtual IEnumerator ShootCoroutine(Vector3 targetPosition)
        {
            var offset = Vector3.Distance(targetPosition, _transform.position);
            yield return new WaitUntil(() => (Vector3.Distance(targetPosition, _transform.position) < _minDistance) || _isTimeIsUp);
            //Debug.Log("Projectile +_isTimeIsUp");
            //Debug.Log("Projectile +_transform.position == targetPosition");
            StopProjectile();
        }

        protected virtual void StopProjectile()
        {
            StopProjectileCoroutine(_timerCoroutine);

            _onReachTarget?.Invoke();
            _onReachTarget = null;
            //Debug.Log("Stop BASE Projectile()*****  " +gameObject.name);
        }

        protected IEnumerator TimerUpdater()
        {
            yield return _delayTimeProjectile;
            //Debug.Log("Projectile +_isTimeIsUp*******  " +gameObject.name);

            _isTimeIsUp = true;
        }

        protected void StopProjectileCoroutine(Coroutine coroutine)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }

        public void SubscribeOnContactWithTarget(UnityAction<Unit> onContactWithTarget)
        {
            _onContactWithTarget = onContactWithTarget;
        }
    }
}
