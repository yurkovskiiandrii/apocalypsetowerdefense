using DG.Tweening;
using System;
using UnityEngine;
using Projectile;
using static UnityEngine.GraphicsBuffer;

//namespace Projectile
//{
public class UnitProjectile : BaseProjectile
{
    public override void Init()
    {
        base.Init();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_target == null)
            return;
        if (other.transform == _target)
        {
            StopProjectile();
        }
    }

    public override void StartShot(Vector3 position, Quaternion rotation, Action OnReachTarget, Transform target)
    {
        //Debug.Log(" Start Unit Projectile ");

        base.StartShot(position, rotation, OnReachTarget, target);

        StartShootCoroutine(position, target.position);
    }

    private void StartShootCoroutine(Vector3 position, Vector3 targetPosition)
    {
        _transform.DOMove(targetPosition, Vector3.Distance(position, targetPosition) / _speed).SetEase(Ease.Linear);

        StartCoroutine(base.ShootCoroutine(targetPosition));
    }

    protected override void StopProjectile()
    {
        base.StopProjectile();

        _transform.DOKill();
        _target = null;
    }
}
//}
