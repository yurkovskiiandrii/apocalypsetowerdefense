using System;
using UnityEngine;
using UnityEngine.Events;
using Projectile;
using Units;

//namespace Projectile
//{
public class ShotgunProjectile : BaseProjectile
{
    [Header("ShotGun Projectile parameters")]
    [SerializeField] protected string _targetTag = "Invader";
    private bool _isShotgun;
    private Coroutine _shootSkillCoroutine;

    public override void Init()
    {
        base.Init();
    }

    private void Update()
    {
        if (_isShotgun)
            _transform.Translate(_direction * Time.deltaTime * _speed, Space.World);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isShotgun && other.gameObject.tag == _targetTag)
        {
            Debug.Log("Shoot + OnTriggerEnter Is Target => Invader");
            base.StopProjectileCoroutine(_shootSkillCoroutine);

            Unit unit = other.gameObject.GetComponent<Unit>();
            if (unit.RagdollController.HasRagdoll())
            {
                var ragdollPower = _ragdollConfig.GetCurrentRagdollPower(_ragdollPowerType, unit.RagdollController.GetMassObjectType());
                if (ragdollPower != 0)
                    unit.UseRagdoll(ragdollPower, _transform.position, _ragdollParams.ExplosionRange, _ragdollParams.UpwardsModifier);
            }
            _onContactWithTarget?.Invoke(unit);

            StopProjectile();
            return;
        }
    }

    public override void StartShot(Vector3 position, Quaternion rotation, Action OnReachTarget, Vector3 direction)
    {
        //Debug.Log(" Start Projectile ShootShotgun");

        base.StartShot(position, rotation, OnReachTarget, direction);

        StartShootCoroutine(direction);
    }

    private void StartShootCoroutine(Vector3 targetPosition)
    {
        _isShotgun = true;

        _shootSkillCoroutine = StartCoroutine(base.ShootCoroutine(targetPosition));
    }

    protected override void StopProjectile()
    {
        _isShotgun = false;

        base.StopProjectile();
    }
}
//}
