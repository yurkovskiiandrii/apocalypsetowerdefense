using UnityEngine;

[System.Serializable]
public class SlowDebuffParams
{
    [SerializeField] private float _timeDebuff;
    [Range(0f, 1f)]
    [SerializeField] private float _multiplierSlowSpeed;
    [Range(0f, 1f)]
    [SerializeField] private float _multiplierSlowAttack;

    public float TimeDebuff => _timeDebuff;

    public float MultiplierSlowSpeed => _multiplierSlowSpeed;

    public float MultiplierSlowAttack => _multiplierSlowAttack;
}
