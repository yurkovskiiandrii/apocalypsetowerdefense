using UnityEngine;
using Units;

public static class RegistrationUnitInCell//Test
{
    public static void SetPlaseStatusAndRegistrationInCell(Unit unit, Vector3 pos, bool registered) //during returning to the cell(registered = true) , after death(registered = false)
    {
        if (registered)
            Debug.Log("Unit Set Registration Status in Cell and PlaceStatus");
        else
            Debug.Log("Unit Dead -> Remove registration in Cell and UnplaceStatus");
        var currentCell = TerrainGrid.Instance.GetCell(GetPositionUnitInCell(pos));
        currentCell.SetRegistrationStatusOfObjectIn—ell(unit, registered);
        //currentCell.ShowIndicator();//test
    }
  
    public static void SetUnplaceStatusForCell(Vector3 pos)
    {
        var currentCell = TerrainGrid.Instance.GetCell(GetPositionUnitInCell(pos));
        currentCell.SetUnplaceStatus();
        //currentCell.ShowIndicator();//test
        Debug.Log("Unit Set Unplace Status");
    }

    public static Vector3 GetPositionUnitInCell(Vector3 position)
    {
        Vector3 positionUnit = position;
        var gridPosition = TerrainGrid.Instance.GetNearestCellPosition(position);
        var positionCell = TerrainGrid.Instance.GetCellPosition(gridPosition);
        positionUnit = positionCell;
        return positionUnit;
    }
}
