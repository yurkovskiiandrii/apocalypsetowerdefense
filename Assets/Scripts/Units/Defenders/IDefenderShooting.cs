using UnityEngine;

interface IDefenderShooting
{
    public void ShootProjectile(Transform target);

    public void TriggerAttack();
}
