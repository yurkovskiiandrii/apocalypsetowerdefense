using Projectile;
using System;
using UnitAnimation;
using UnityEngine;
using UnityEngine.Events;
using Units;

[RequireComponent(typeof(RecoveryController))]

public class FarInDirectionDefender : RangedUnit
{
    [Header("FarInDirection Defender")]
    //private const string THIS_LAYER = "Unit";
    //private const string SKILL_LAYER = "PassableUnit";
    //private const string THIS_TAG = "Defender";
    protected UnityEvent onPlace;
    public Vector3 InitialPosition { get; set; }

    public override void Init(Action<bool> OnDie, Action onReachDestination, Func<bool> OnHeroGetDamage = null)
    {
        base.Init(OnDie, onReachDestination);
        //gameObject.tag = THIS_TAG;
        //onDie += delegate { gameObject.tag = "Untagged"; };
        InitialPosition = _transform.position;
        _navMeshAgent.enabled = true;

        onPlace?.Invoke();
    }

    public override void GetDamage(float damage, DamageType damageType)
    {
        base.GetDamage(damage, damageType);
    }

    protected override void Die(bool isKilled)
    {
        base.Die(isKilled);
        //ChangeMaterial(_deadMaterial);

        //gameObject.SetActive(false);
        //Destroy(this.gameObject);
    }

    public override void Attack(Transform target)
    {
        if (_prefabProjectile == null)
            return;

        ShootProjectile(target);
    }

    public override void TriggerAttack()//for Animation Event
    {
        if (_target == null)
            return;
        //Debug.Log("TriggerAttack()   "+gameObject.name);
        var distanceNormalized = (_target.position - _transform.position).normalized;
        var direction = new Vector3(distanceNormalized.x, 0, distanceNormalized.z);

        FarInDirectionProjectile newProjectile = ProjectilePoolManager.Instance.GetObjectFromPool<FarInDirectionProjectile>(_prefabProjectile as FarInDirectionProjectile);

        newProjectile.SetProjectileParameters(ProjectileSpeed);
        newProjectile.SubscribeOnContactWithTarget(value => DamageTarget(value));
        newProjectile.StartShot(ProjectilePosition.position, ProjectilePosition.rotation,
            () => ProjectilePoolManager.Instance.ReleaseObjectToPool(_prefabProjectile.gameObject, newProjectile),
            direction);
    }

    private void DamageTarget(Unit unitTarget)
    {
        if (unitTarget.IsAlive())
        {
            var unit = unitTarget;
            if (unit.DamagePopup != null && unit.DamagePopup.gameObject.activeSelf)
                unit.DamagePopup.Show(_attackDamage);

            unit.GetDamage(_attackDamage, DamageType);
        }
    }
}
