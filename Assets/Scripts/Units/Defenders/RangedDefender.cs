using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;
using UnityEngine.Events;
using UnitAnimation;
using Units;

[RequireComponent(typeof(RecoveryController))]
public class RangedDefender : RangedUnit
{
    //private const string THIS_LAYER = "Unit";
    //private const string SKILL_LAYER = "PassableUnit";
    //private const string THIS_TAG = "Defender";

    [SerializeField] protected UnityEvent onPlace;

    [Header("Blitz Raid Animator States")]
    [SerializeField] protected string _idleStateBlitzRaid;
    [SerializeField] protected string _runStateBlitzRaid;
    [SerializeField] protected string _getDamageStateBlitzRaid;
    [SerializeField] protected string _attackStateBlitzRaid;
    [SerializeField] protected string _deathStateBlitzRaid;

    [Header("Blitz Raid Stats")]
    [SerializeField] private bool _canGoThroughUnits = false;

    [SerializeField] private ParticleSystem _dealDamageParticles;
    [SerializeField] private ParticleSystem _getDamageParticles;

    [SerializeField] private UnityEvent onSkillStarted;
    [SerializeField] private UnityEvent onSkillEnded;

    [SerializeField] private UnityEvent onSkillDamageDeal;
    [SerializeField] private UnityEvent onSkillDamageGet;

    [SerializeField] private float _blitzRaidDistance;
    [SerializeField] private int _minTargetsCount;
    [SerializeField] private int _damageMultiplicator;
    [SerializeField] private float _speedMultiplicator;
    [SerializeField] private float _coolDownTime;
    
    public float BlitzRaidDistance { get { return _blitzRaidDistance; } }
    public int MinTargetsCount { get { return _minTargetsCount; } }
    public float SpeedMultiplicator { get { return _speedMultiplicator; } }
    public Vector3 InitialPosition { get { return _initialPosition; } }

    private bool _skillAvailable = true;
    private bool _skillActive = false;
    private Vector3 _initialPosition;
    private Unit _enemy;

    public override void Init(Action<bool> OnDie, Action onReachDestination, Func<bool> OnHeroGetDamage = null)
    {
        base.Init(OnDie, onReachDestination);
        //gameObject.tag = THIS_TAG;
        //onDie += delegate { gameObject.tag = "Untagged"; };
        _initialPosition = _transform.position;
        _navMeshAgent.enabled = true;

        onPlace?.Invoke();
    }

    public bool TryActivateSkill()
    {
        if (!_skillAvailable)
            return false;

        onSkillStarted.Invoke();
        if (_canGoThroughUnits)
        {
            _navMeshAgent.avoidancePriority = 99;
            _navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
        }

        _skillActive = true;
        _skillAvailable = false;
        return true;
    }

    public void DeactivateSkill()
    {
        //RegistrationUnitInCell.SetPlaseStatusAndRegistrationInCell(this, _initialPosition, true);//test
        _skillActive = false;
        onSkillEnded.Invoke();
        if (_canGoThroughUnits)
        {
            _navMeshAgent.avoidancePriority = 50;
            _navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
        }
        StartCoroutine(CoolDownCoroutine());
    }

    private IEnumerator CoolDownCoroutine()
    {
        yield return new WaitForSeconds(_coolDownTime);
        _skillAvailable = true;
    }

    public void BlitzRaidAttack(Unit enemy)
    {
        //RegistrationUnitInCell.SetUnplaceStatusForCell(_initialPosition);//test
        SetAnimatorBlitzRaidBools(false);
        SetAnimatorBoolByName(_attackStateBlitzRaid, true);

        _enemy = enemy;
        TriggerBlitzRaidAttack();//test

        Vector3 rotate = _transform.rotation.eulerAngles;
        rotate.y += 360;

        _transform.DORotate(rotate, 0.5f);
        onSkillDamageDeal.Invoke();
    }

    public void TriggerBlitzRaidAttack()//for Animation Event
    {
        if (!_enemy)
            return;
        if (_damagePopup != null && _damagePopup.gameObject.activeSelf)
            _damagePopup.Show(_attackDamage * _damageMultiplicator);
        _enemy.GetDamage(_attackDamage * _damageMultiplicator, _damageType);
        _enemy = null;
    }

    public override void GetDamage(float damage, DamageType damageType)
    {
        base.GetDamage(damage, damageType);
        if (_skillActive)
            onSkillDamageGet.Invoke();
    }

    protected override void Die(bool isKilled)
    {
        base.Die(isKilled);
    }

    public void SetAnimatorBlitzRaidRun(bool status) => SetAnimatorBoolByName(_runStateBlitzRaid, status);

    public void SetAnimatorBlitzRaidIdle(bool status) => SetAnimatorBoolByName(_idleStateBlitzRaid, status);

    public void SetAnimatorBlitzRaidBools(bool status)
    {
        SetAnimatorBoolByName(_idleStateBlitzRaid, status);
        SetAnimatorBoolByName(_runStateBlitzRaid, status);
        //SetAnimatorBoolByName(_getDamageStateBlitzRaid, status);
        SetAnimatorBoolByName(_attackStateBlitzRaid, status);
        SetAnimatorBoolByName(_deathStateBlitzRaid, status);
    }
}
