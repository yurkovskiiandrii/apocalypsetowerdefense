using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;
using Units;

public class RecoveryController : MonoBehaviour
{
    [SerializeField] private Unit _unit;
    [SerializeField] private UnitGameUI _dead_gameUI;
    [SerializeField] private GameObject _recoveryPanel;
    [SerializeField] private static float _recoveryTime = 15.0f;
    [SerializeField] private float _recoveryPriceMultiplier = 1.0f;
    [SerializeField] private LayerMask _unitLayer;

    private int _recoveryPrice;
    private RecoveryTimer _recoveryTimer;
    protected Coroutine _touchHandler;

    public static float RecoveryTime { get => _recoveryTime; set => _recoveryTime = value; }

    public void Init(UnityAction onRecoveryEnd)
    {
        _recoveryTimer = new RecoveryTimer();
        _recoveryTimer.Init(_dead_gameUI);
        _recoveryTimer.SubscribeOnHeroRecoveryEnd(onRecoveryEnd);
        _dead_gameUI.gameObject.SetActive(false);
    }

    public void SetRecoveryPrice()
    {
        _dead_gameUI.gameObject.SetActive(true);

        var totalCostOfUpgrades = _unit.UpgradeConfig.GetTotalCostOfUpgrades();
        _recoveryPrice = (int)Mathf.Ceil(totalCostOfUpgrades * _recoveryPriceMultiplier);

        _touchHandler = StartCoroutine(TouchHandler());
    }

    public void StartRecoveryTimer()
    {
        _recoveryTimer.StartRecoveryTimer();
        StopCoroutine();
        _unit.OnRecoveryFVX();
    }

    private void ShowRestartDefenderPanel()
    {
        _recoveryPanel.SetActive(true);
        _recoveryPanel.GetComponent<RestartDefenderPanel>().Init(_unit.UpgradeConfig.GetUnitName, _unit.UpgradeConfig.GetCurrentUpgradeIndex(), _recoveryTime, _recoveryPrice, StartRecoveryTimer);
    }

    private IEnumerator TouchHandler()
    {
        var camera = Camera.main;

        while (true)
        {
            yield return null;

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), camera.transform.forward, out RaycastHit hit, 100f, _unitLayer))
                {
                    if (hit.transform.gameObject == this.gameObject)
                        ShowRestartDefenderPanel();
                }
            }
        }
    }

    public void StopCoroutine()
    {
        if (_touchHandler != null)
        {
            StopCoroutine(_touchHandler);
            _touchHandler = null;
        }
    }

    public void SubscribeOnHeroRecoveryStart(UnityAction onHeroRecoveryStart)
    {
        _recoveryTimer.SubscribeOnHeroRecoveryStart(onHeroRecoveryStart);
    }

    public void SubscribeOnHeroRecoveryEnd(UnityAction onHeroRecoveryEnd)
    {
        _recoveryTimer.SubscribeOnHeroRecoveryEnd(onHeroRecoveryEnd);
    }
}