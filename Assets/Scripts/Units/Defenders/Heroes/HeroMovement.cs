using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using BehaviorDesigner.Runtime;

public class HeroMovement : MonoBehaviour
{
    [Header("HeroMovement parameters")]
    [SerializeField] private float _movingSpeed = 3.5f;
    [SerializeField] private float _maxCollisionTime = 1.5f;

    private float _minDistance = 0.5f;
    private float _collisionTime = 0;
    private HeroController _heroController;
    private HeroesManager _heroesManager;
    private NavMeshAgent _navMeshAgent;
    private NavMeshPath _navMeshPath;
    private BehaviorTree _behaviorTree;
    private Transform _transform;
    private Vector3 _startPosition;
    private Vector3 _newPosition;
    private Action _onAttack;
    private Coroutine _onPlaceCoroutineCoroutine;
    private TerrainGrid _terrainGrid;

    public void Init()
    {
        _heroesManager = HeroesManager.Instance;
        _terrainGrid = TerrainGrid.Instance;
        _heroController = GetComponent<HeroController>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _behaviorTree = GetComponent<BehaviorTree>();
        _navMeshPath = new NavMeshPath();
        _transform = transform;

        _heroController.SubscribeOnHeroDead(StopMovementHero);
        _navMeshAgent.speed = _movingSpeed;
    }

    public bool IsRelocationRequired()//after restart Hero
    {
        var heroPos = _heroesManager.GetPositionHeroInCell(_transform.position);
        var currentCell = GetCell(heroPos);

        if (currentCell.IsObjectRegistered && currentCell.IsObjectPlaced)
            return true;
        return false;
    }

    [ContextMenu("UpdateHeroPositionToNearestPoint")]

    public void UpdateHeroPositionToNearestPoint()
    {
        var nearestPos = GetNearestHeroPosition(_newPosition);
        SetNewPosition(nearestPos);
    }

    public Vector3 GetNearestHeroPosition(Vector3 pos)
    {
        var posInCell = _heroesManager.GetPositionHeroInCell(pos);
        var currentCell = _terrainGrid.GetCell(posInCell);

        var closestCell = _terrainGrid.GetCellClosestFreePoint(currentCell);
        //Debug.Log("Hero Movement / GetNearestHeroPosition |||||||||||||||");
        //closestCell.ShowIndicator();

        var heroGridPosition = _terrainGrid.GetNearestCellPosition(_transform.position);
        var direction = closestCell.Position - heroGridPosition;
        var heroPosition = _transform.position;
        var newPos = heroPosition + new Vector3(_terrainGrid.CellSize.x * direction.x, 0, _terrainGrid.CellSize.y * direction.y);
        return newPos;
    }

    public void SetNewPosition(Vector3 newPositionInCell, Action OnAttack = null)
    {
        _startPosition = _heroesManager.GetPositionHeroInCell(_transform.position);
        _onAttack = OnAttack;
        _newPosition = newPositionInCell;
        _collisionTime = 0;

        var newCell = GetCell(newPositionInCell);
        if (!newCell.IsCanPlaced())
            _newPosition = GetNearestHeroPosition(newPositionInCell);

        StartMovementNewPosition(_newPosition);
    }

    public void StartMovementNewPosition(Vector3 newPosition)
    {
        if (_navMeshAgent.isActiveAndEnabled)
        {
            _behaviorTree.DisableBehavior(false);
            _navMeshAgent.isStopped = false;
           
            //_navMeshAgent.CalculatePath(newPosition, _navMeshPath);
            //if (_navMeshPath.status == NavMeshPathStatus.PathInvalid)
            //{
            //    _newPosition = GetNearestHeroPosition(newPosition);
            //    Debug.Log("Hero Movement / after get Nearest pos /_meshPath.status = " + _navMeshPath.status + " / new Cell =  " + GetSell(_newPosition).Position.ToString() + "/ Registered = " + GetSell(_newPosition).IsObjectRegistered);
            //}

            _navMeshAgent.SetDestination(_newPosition);
            _navMeshAgent.CalculatePath(newPosition, _navMeshPath);
            //Debug.Log("Hero Movement /_meshPath.status = " + _navMeshPath.status + " / new Cell =  " + GetCell(_newPosition).Position.ToString() + "/ Registered = " + GetCell(_newPosition).IsObjectRegistered);
            _onPlaceCoroutineCoroutine = StartCoroutine(OnPlaceCoroutine(_newPosition));
        }
    }

    private IEnumerator OnPlaceCoroutine(Vector3 newPosition)
    {
        yield return null;
        yield return new WaitUntil(() => _navMeshAgent.remainingDistance <= _minDistance);
        //Debug.Log("OnPlace Coroutine++");

        _onPlaceCoroutineCoroutine = null;

        MovementEnd();

        if (_navMeshPath.status == NavMeshPathStatus.PathComplete)
            SetNewStatusForCell(_startPosition, newPosition);
        else
            SetNewStatusCurrentPosition();
    }

    private void MovementEnd()
    {
        if (_onAttack != null)
        {
            Debug.Log("OnPlace MovementEnd()  _onAttack != null");
            _onAttack?.Invoke();
            return;
        }
        Debug.Log("OnPlace MovementEnd()  EnableBehavior");

        _behaviorTree.EnableBehavior();
    }

    private void SetNewStatusForCell(Vector3 startPos, Vector3 endPos)
    {
        var currentCell = GetCell(startPos);
        currentCell.SetUnplaceStatus();
        if (currentCell.IsObjectRegistered)
            currentCell.RemoveOnUpdateHeroPosition(UpdateHeroPositionToNearestPoint);
        //Debug.Log("Hero Movement / startCell = " + currentCell.Position.ToString() + "/ Registered = " + currentCell.IsObjectRegistered);


        var newCell = GetCell(endPos);
        newCell.SetPlaceStatus();
        if (newCell.IsObjectRegistered)
            newCell.SubscribeOnUpdateHeroPosition(UpdateHeroPositionToNearestPoint);
        //Debug.Log("Hero Movement / newCell = " + newCell.Position.ToString() + "/ Registered = " + newCell.IsObjectRegistered);
    }

    private Cell GetCell(Vector3 position)
    {
        var cell = _terrainGrid.GetCell(position);
        return cell;
    }

    //public bool IsCalculatePathComplete(Vector3 newPosition)
    //{
    //    _navMeshAgent.CalculatePath(newPosition, _navMeshPath);
    //    Debug.Log("Hero Stopped / _meshPath.status = " + _navMeshPath.status);
    //    if (_navMeshPath.status == NavMeshPathStatus.PathComplete)
    //        return true;
    //    return false;
    //}

    private void StopMovementHero()//hero dead
    {
        if (_onPlaceCoroutineCoroutine != null)
        {
            StopCoroutineMovementHero();
            SetNewStatusForCell(_startPosition, _heroesManager.GetPositionHeroInCell(_transform.position));
        }
    }

    private void StopCoroutineMovementHero()
    {
        if (_onPlaceCoroutineCoroutine != null)
        {
            StopCoroutine(_onPlaceCoroutineCoroutine);
            _onPlaceCoroutineCoroutine = null;
        }
    }

    private void StopMovementToNewPosition()
    {
        StopCoroutineMovementHero();
        _navMeshAgent.isStopped = true;
        MovementEnd();
        SetNewStatusCurrentPosition();
    }

    private void SetNewStatusCurrentPosition()
    {
        var posInCell = _heroesManager.GetPositionHeroInCell(_transform.position);
        //var newCell = GetSell(posInCell);
        //if (!newCell.IsCanPlaced())
        //{
        //    _newPosition = GetNearestHeroPosition(posInCell);
        //    SetNewPosition(posInCell);
        //    return;
        //}
        SetNewStatusForCell(_startPosition, posInCell);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (_onPlaceCoroutineCoroutine == null)
            return;
        //if (collision.gameObject.CompareTag(_heroController.BaseRangedUnit.EnemyTag))
        //{
        _collisionTime += Time.deltaTime;
        //Debug.Log(" Hero OnCollision Stay >>>> / time = " + _collisionTime);
        if (_collisionTime > _maxCollisionTime)
        {
            //Debug.Log(" Hero OnCollision Stay >>>> / MaxTime = " + _maxCollisionTime);
            StopMovementToNewPosition();
        }
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_onPlaceCoroutineCoroutine == null)
            return;
        //if (collision.gameObject.CompareTag(_heroController.BaseRangedUnit.EnemyTag))
        //{
        _collisionTime = 0;
        var posInCell = _heroesManager.GetPositionHeroInCell(collision.transform.position);
        if (posInCell == _newPosition)
        {
            Debug.Log(" Hero OnCollision Enter <<< " + collision.gameObject.name + " cellCollision = " + GetCell(posInCell).Position.ToString() + " == newCell = " + GetCell(_newPosition).Position.ToString());
            StopMovementToNewPosition();
        }
        //}
    }
}
