using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using HeroSkills;
using Units;

[RequireComponent(typeof(HeroMovement))]
[RequireComponent(typeof(RangedUnit))]
[RequireComponent(typeof(RecoveryController))]

public class HeroController : MonoBehaviour
{
    //private const string THIS_LAYER = "Unit";
    //private const string THIS_TAG = "Defender";

    [Range(0f, 100f)]
    [SerializeField] private float _dodge;//This is a % chance to dodge damage when received

    [SerializeField] private ParticleSystem _dealDamageParticles;
    [SerializeField] private ParticleSystem _getDamageParticles;

    [SerializeField] private List<ActiveSkill> _activeSkills;
    [SerializeField] private List<PassiveSkill> _passiveSkills;

    [SerializeField] private UnityEvent _onHeroSelectedVFX;
    [SerializeField] private UnityEvent _onHeroNotSelectedVFX;

    private RangedUnit _baseRangedUnit;
    private HeroUILabelPref _heroUILabel;
    private HeroMovement _heroMovement;
    private UnityEvent _onHeroDead = new UnityEvent();
    private HeroesManager _heroesManager;
    private TerrainGrid _terrainGrid;
    private float _minDodge = 0;
    private float _maxDodge = 100;
    public HeroMovement HeroMovement { get { return _heroMovement; } }
    public HeroUILabelPref HeroUILabel { get { return _heroUILabel; } }
    public RangedUnit BaseRangedUnit { get { return _baseRangedUnit; } }
    public int Index { get; set; }

    public void InitUILabel()
    {
        _heroUILabel = _heroesManager.CreateHeroUILabel();
        _heroUILabel.InitHeroSettigs(_baseRangedUnit.DefenderOptions.iconSprite, Index, () => _onHeroSelectedVFX?.Invoke(), () => _onHeroNotSelectedVFX?.Invoke());
        SubscribeOnHeroDead(() => { _heroUILabel.HeroDead(); });
        SubscribeOnHeroRecoveryStart(_heroUILabel.StartTimersCoroutinesHeroDead);
    }

    public void ActivateHero()
    {
        if (_heroesManager.IsActiveHero(Index))
        {
            _heroUILabel.DeActivateHero();
            _heroesManager.DeactiveCurrentHeroInScene();
            return;
        }
        _heroUILabel.ActivateHero();
    }

    public void SetStartPositionHero(Vector3 posInCell)
    {
        _baseRangedUnit.Transform.position = posInCell;
    }

    public void StartHero(Vector3 startPosInCell)
    {
        Debug.Log("Start Hero() " + gameObject.name);

        _terrainGrid = TerrainGrid.Instance;
        _heroesManager = HeroesManager.Instance;

        _heroMovement = GetComponent<HeroMovement>();
        _baseRangedUnit = GetComponent<RangedUnit>();

        InitBaseUnit();

        SetStartPositionHero(startPosInCell);

        _heroMovement.Init();

        SubscribeOnHeroRecoveryEnd(RestartHero);

        InitUILabel();

        if (_activeSkills.Count > 0)
        {
            var currentConfig = BaseRangedUnit.UpgradeConfig;
            foreach (var skill in _activeSkills)
            {
                skill.SetParameters(currentConfig.GetCurrentProgress().SkillTime, currentConfig.GetCurrentProgress().SkillCooldown);
                skill.Init();
                UpgradeManager.Instance.SubscribeOnUpgradeEnd(() =>
                    {
                        skill.SetParameters(currentConfig.GetCurrentProgress().SkillTime,
                        currentConfig.GetCurrentProgress().SkillCooldown);
                    });
            }
        }

        if (_passiveSkills.Count > 0)
        {
            foreach (var skill in _passiveSkills)
                skill.Init();
        }
    }

    public void InitBaseUnit()
    {
        _baseRangedUnit.Init(delegate { Die(); }, _heroesManager.OnHeroReachDestination(), () => IsDodge());
        //gameObject.tag = THIS_TAG;
        //_baseRangedUnit.onDie += delegate { gameObject.tag = "Untagged"; };
        //_baseRangedUnit.SetMainMaterial();
        LevelController.Instance.AddDefender(_baseRangedUnit);
    }

    private void RestartHero()
    {
        if (_heroMovement.IsRelocationRequired())
            _heroMovement.UpdateHeroPositionToNearestPoint();
    }

    public bool IsDodge()
    {
        var currentDodge = UnityEngine.Random.Range(_minDodge, _maxDodge);
        return currentDodge < _dodge;
    }

    public void Die()
    {
        _onHeroDead?.Invoke();
        LevelController.Instance.RemoveDefender(_baseRangedUnit);
    }

    public void SubscribeOnHeroRecoveryStart(UnityAction onHeroRecoveryStart)
    {
        _baseRangedUnit.RecoveryController.SubscribeOnHeroRecoveryStart(onHeroRecoveryStart);
    }

    public void SubscribeOnHeroRecoveryEnd(UnityAction onHeroRecoveryEnd)
    {
        _baseRangedUnit.RecoveryController.SubscribeOnHeroRecoveryEnd(onHeroRecoveryEnd);
    }

    public void SubscribeOnHeroDead(UnityAction onHeroDead)
    {
        _onHeroDead.AddListener(onHeroDead);
    }

    public void SetNewPositionInCell(Vector3 newPosition)
    {
        _heroMovement.SetNewPosition(newPosition);
    }
}
