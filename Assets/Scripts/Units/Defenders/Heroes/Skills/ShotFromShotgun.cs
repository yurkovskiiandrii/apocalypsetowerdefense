using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnitAnimation;
using Units;

namespace HeroSkills
{
    public class ShotFromShotgun : ActiveSkill
    {
        [Header("Shots Of Shotgun parameters")]
        [SerializeField] private HeroController _heroController;
        [SerializeField] private float _rotationDuration = 0.5f;
        [SerializeField] private float _attackDamage = 3.0f;
        [SerializeField] private float _projectileSpeed = 2;
        [SerializeField] private int _numberOfShotgunShell = 5;
        [SerializeField] public float _angleOfShotsOfShotgun = 30;
        [SerializeField] private ShotgunProjectile _prefabProjectile;
        [Header("ShotFromShotgun Animator States")]
        [SerializeField] private string _attackStateShotFromShotgun;

        private HeroUILabelPref _heroUILabel;
        private Coroutine _touchHandlerShotPositionCoroutine;
        private float _currentAngleBetweenHeroAndTarget;
        private float _stepAngleOfShotsOfShotgun;
        private Transform _transform;
        private HeroesManager _heroesManager;
        private Vector3 _offsetShotPosition = new Vector3(0, 0.5f, 0);
        private Vector3 _target;

        public override bool SkillAvailable { get; protected set; } = true;
        public override bool SkillActive { get; protected set; } = false;

        public override void Init()
        {
            //Debug.Log("Init ShotFromShotgun");
            _heroesManager = HeroesManager.Instance;
            _heroUILabel = _heroController.HeroUILabel;
            _heroUILabel.InitActiveSkill(_iconSkill, _coolDownTime,
                () => { if (CanUseSkill()) UseSkill(); },
                () => { DeactivateSkill(); },
                () => { DeactivateSkill(); });

            SubscribeOnSkillStarted(_heroUILabel.StartSkillTimersCoroutines);

            _heroController.SubscribeOnHeroDead(StopSkill);

            _transform = _heroController.BaseRangedUnit.Transform;
        }

        public override bool CanUseSkill()
        {
            if (!IsInit())
            {
                _heroesManager.ShowHeroDeadPanel(true);
                return false;
            }

            if (SkillAvailable && !SkillActive)
                return true;
            return false;
        }

        public bool IsInit() => _heroController.BaseRangedUnit.IsInit;

        public override void UseSkill()
        {
            if (!SkillAvailable)
                return;

            Debug.Log("Hero Use Active Skill");
            _heroController.BaseRangedUnit.BehaviorTree.DisableBehavior();

            ActivateShotFromShotgun();

            _heroUILabel.StartSkill();

            SkillActive = true;
        }

        private void SkillEnded()
        {
            Debug.Log("Hero Active Skill Ended");

            if (_touchHandlerShotPositionCoroutine != null)
                StopCoroutine(_touchHandlerShotPositionCoroutine);

            SkillActive = false;
            SkillAvailable = true;
        }

        public void DeactivateSkill()//Event at the end of the timer
        {
            SkillEnded();
            //_onSkillEnded?.Invoke();
            RecoveryOfHeroBehavior();
        }

        private void StopSkill()//Event in case of death
        {
            SkillEnded();
        }

        private void ActivateShotFromShotgun()
        {
            _touchHandlerShotPositionCoroutine = StartCoroutine(TouchHandlerShotPosition());
        }

        private void StartShotFromShotgun(Vector3 target)
        {
            //Debug.Log("Shot From Shotgun");

            Rotate(target, () =>
            {
                //����� ��'��� ��������� (���� ������� ��������� ����� �������� - �� ������ ������ � ��� � ���);
                _heroController.BaseRangedUnit.SetAnimatorBoolByType(AnimatorType.Attack, true);
                //_heroController.BaseRangedUnit.SetAnimatorBoolByName(_attackStateShotFromShotgun, true);

                _target = target;
                TriggerAttack();//test
            });

            _heroesManager.IsActiveSkill = false;
        }

        private void Rotate(Vector3 target, Action OnComplete)
        {
            var tweenerLookAt = _transform.DOLookAt(target, _rotationDuration);
            tweenerLookAt.onComplete += () => { tweenerLookAt = null; OnComplete?.Invoke(); };
        }

        private void RecoveryOfHeroBehavior()
        {
            _heroController.BaseRangedUnit.BehaviorTree.EnableBehavior();
            //Debug.Log(" RecoveryOfHeroBehavior()");
        }

        public void TriggerAttack()//for Animation Event
        {
            //Debug.Log("target position: " + target);

            //_skillParticles.Play();
            //���������� ��������� ����� ������� ����������;

            var startAngle = Vector3.Angle((_target - _transform.position).normalized, Vector3.right);

            if (_target.z > _transform.position.z)
            {
                startAngle = 360 - startAngle;
                //if (target.position.x < _transform.position.x)
                //    startAngle += 90;
                //else
                //    startAngle += 270;
            }

            startAngle = Mathf.Abs(startAngle - 360);

            var angleStep = _angleOfShotsOfShotgun / _numberOfShotgunShell;

            startAngle -= angleStep * _numberOfShotgunShell / 2;

            for (int i = 0; i < _numberOfShotgunShell; i++)
            {
                var angle = startAngle + i * angleStep;
                float x = Mathf.Cos((angle * Mathf.PI) / 180f);
                float y = Mathf.Sin((angle * Mathf.PI) / 180f);

                var direction = new Vector3(x, 0, y);
                ShootProjectile(direction);
            }

            RecoveryOfHeroBehavior();
        }

        public void SubscribeOnSkillStarted(UnityAction onSkillStarted)
        {
            _onSkillStarted.AddListener(onSkillStarted);
        }

        public void SubscribeOnSkillEnded(UnityAction onSkillEnded)
        {
            _onSkillEnded.AddListener(onSkillEnded);
        }

        public void SubscribeOnSkillStopped(UnityAction onSkillEnded)
        {
            _onSkillStopped.AddListener(onSkillEnded);
        }

        protected IEnumerator TouchHandlerShotPosition()
        {
            //Debug.Log("Start Coroutine TouchHandler ShotPosition/Skill");

            var camera = Camera.main;

            while (SkillAvailable)
            {
                yield return null;

                if (Input.GetMouseButtonDown(0))
                {
                    Vector3 target = GetWorldMousePosition(_transform.position);
                    //Debug.Log("Mouse Position / Target = " + target);

                    if (MouseDetectorAboveUI.IsMouseOverUI(Input.mousePosition))
                        continue;

                    _onSkillStarted?.Invoke();

                    var distance = Vector3.Distance(target, _transform.position);
                    var attackRange = _heroController.BaseRangedUnit.AttackRange;

                    if (distance > attackRange)
                    {
                        //Debug.Log("distance > _hero AttackRange / SetNewPosition /Attack");
                        var direction = (target - _transform.position).normalized;
                        var offset = Vector3.Distance(target, _transform.position) - attackRange;
                        var shotPosition = _transform.position + (direction * offset);
                        //Debug.Log(" shotPos = " + shotPosition.ToString() + " / target " + target.position.ToString());
                        _heroController.HeroMovement.SetNewPosition(_heroesManager.GetPositionHeroInCell(shotPosition), delegate { SkillAvailable = false; StartShotFromShotgun(target); });
                    }
                    else
                    {
                        //Debug.Log("distance == _hero AttackRange / Attack");
                        //Debug.Log("Target skill "+target.gameObject.name);
                        SkillAvailable = false;
                        StartShotFromShotgun(target);
                    }
                }
            }
        }

        public Vector3 GetWorldMousePosition(Vector3 objectPos)
        {
            var camera = Camera.main;
            Plane plane = new Plane(Vector3.up, objectPos);
            Ray cursorRay = camera.ScreenPointToRay(Input.mousePosition);
            float rayDist;

            if (plane.Raycast(cursorRay, out rayDist))
                return cursorRay.GetPoint(rayDist);

            return Vector3.zero;
        }

        public void ShootProjectile(Vector3 targetPosition)
        {
            if (!IsInit())
                return;

            Debug.Log("ShootProjectile Skill");
            ShotgunProjectile newProjectile = ProjectilePoolManager.Instance.GetObjectFromPool<ShotgunProjectile>(_prefabProjectile);
            
            newProjectile.SetProjectileParameters(_heroController.BaseRangedUnit.ProjectileSpeed);
            var projectilePosition = _heroController.BaseRangedUnit.ProjectilePosition;
            
            newProjectile.SubscribeOnContactWithTarget(value => DamageTarget(value));
            
            newProjectile.StartShot(projectilePosition.position, projectilePosition.rotation,
                  () => ProjectilePoolManager.Instance.ReleaseObjectToPool(_prefabProjectile.gameObject, newProjectile),
                  targetPosition);
        }

        private void DamageTarget(Unit unitTarget)
        {
            if (unitTarget.IsAlive())
            {
                var unit = unitTarget;
                if (unit.DamagePopup != null && unit.DamagePopup.gameObject.activeSelf)
                    unit.DamagePopup.Show(_attackDamage);

                unit.GetDamage(_attackDamage, _heroController.BaseRangedUnit.DamageType);
            }
        }
    }
}

