using UnityEngine;

namespace HeroSkills
{
    public abstract class PassiveSkill : MonoBehaviour, IUnitSkill
    {
        [Header("Skill parameters")]
        [SerializeField] protected Sprite _iconSkill;

        public abstract void Init();

        public abstract bool CanUseSkill();

        public abstract void UseSkill();
    }
}
        