using UnityEngine;
using UnityEngine.Events;

namespace HeroSkills
{
    public abstract class ActiveSkill : MonoBehaviour, IUnitSkill
    {
        [Header("Skill parameters")]
        [SerializeField] protected Sprite _iconSkill;
        [SerializeField] protected float _skillTime;
        [SerializeField] protected float _coolDownTime;
        [SerializeField] protected string _skillAttackState;
        [SerializeField] protected ParticleSystem _skillParticles;
        [SerializeField] protected UnityEvent _onSkillStarted;
        [SerializeField] protected UnityEvent _onSkillEnded;
        [SerializeField] protected UnityEvent _onSkillStopped;

        public abstract bool SkillAvailable { get; protected set; }
        public abstract bool SkillActive { get; protected set; }

        public virtual void SetParameters(float skillTime, float coolDownTime)
        {
            _skillTime = skillTime;
            _coolDownTime = coolDownTime;
        }

        public abstract void Init();

        public abstract bool CanUseSkill();

        public abstract void UseSkill();


    }
}
