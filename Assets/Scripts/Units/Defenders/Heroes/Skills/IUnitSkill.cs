
public interface IUnitSkill
{
    public void Init();

    public bool CanUseSkill();

    public  void UseSkill();
}
