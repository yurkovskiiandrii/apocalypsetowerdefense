using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroSkills
{
    public class TestPassiveSkill : PassiveSkill
    {
        [Header("TestSkill parameters")]
        [SerializeField] private HeroController _heroController;
        private HeroUILabelPref _heroUILabel;

        public override void Init()
        {
            _heroUILabel = _heroController.HeroUILabel;
            _heroUILabel.InitPassiveSkill(_iconSkill);
        }

        public override bool CanUseSkill()
        {
            return true;
        }

        public override void UseSkill()
        {
            Debug.Log("Hero use passive skill");
        }
    }
}