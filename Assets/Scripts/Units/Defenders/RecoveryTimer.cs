using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RecoveryTimer
{
    [SerializeField] private UnitGameUI _dead_gameUI;
    private float _recoveryTime;
    private TMP_Text _timerText;
    private Slider _hpSlider;
    private UnityEvent _onHeroRecoveryStart = new UnityEvent();
    private UnityEvent _onHeroRecoveryEnd = new UnityEvent();
    private Vector3 _gameUIPosition = new Vector3(0, 2.5f, 0);

    public void Init(UnitGameUI unitDeadGameUI)
    {
        _dead_gameUI = unitDeadGameUI;
        _hpSlider = _dead_gameUI.GetSliderHpBar();
        _timerText = _dead_gameUI.GetTimerText();
        _dead_gameUI.ActivateHealthPointsBar(false);
    }

    private void SetActiveRecoveryGameUI()
    {
        _dead_gameUI.ActivateHealthPointsBar(true);

        _hpSlider.value = _hpSlider.minValue;
    }

    public void SubscribeOnHeroRecoveryStart(UnityAction onHeroRecoveryStart)
    {
        _onHeroRecoveryStart.AddListener(onHeroRecoveryStart);
    }

    public void SubscribeOnHeroRecoveryEnd(UnityAction onHeroRecoveryEnd)
    {
        _onHeroRecoveryEnd.AddListener(onHeroRecoveryEnd);
    }

    public void StartRecoveryTimer()
    {
        _recoveryTime = RecoveryController.RecoveryTime;
        _onHeroRecoveryStart?.Invoke();

        SetActiveRecoveryGameUI();

        var updateTimerBrogressBar = DOVirtual.Float(0, 1, _recoveryTime, (float value) => _hpSlider.value = value).SetEase(Ease.Linear); ;
        updateTimerBrogressBar.OnStart<Tweener>(() => { _hpSlider.gameObject.SetActive(true); });
        updateTimerBrogressBar.onComplete += () => { _hpSlider.gameObject.SetActive(false); };

        var updateTimerText = DOVirtual.Int((int)_recoveryTime, 0, _recoveryTime, (int value) => _timerText.text = (value).ToString()).SetEase(Ease.Linear); ;
        updateTimerText.OnStart<Tweener>(() => { _timerText.gameObject.SetActive(true); });
        updateTimerText.onComplete += () => { _timerText.gameObject.SetActive(false); _dead_gameUI.ActivateHealthPointsBar(false); _dead_gameUI.gameObject.SetActive(false); _onHeroRecoveryEnd?.Invoke(); };
    }
}
