using Projectile;
using System;
using UnitAnimation;
using UnityEngine;
using UnityEngine.Events;
using Units;

[RequireComponent(typeof(RecoveryController))]

public class HealerDefender : RangedUnit
{
    [Header("Healer Defender")]
    //private const string THIS_LAYER = "Unit";
    //private const string THIS_TAG = "Defender";
    protected UnityEvent onPlace;

    public override void Init(Action<bool> OnDie, Action onReachDestination, Func<bool> OnHeroGetDamage = null)
    {
        base.Init(OnDie, onReachDestination);
        //gameObject.tag = THIS_TAG;
        //onDie += delegate { gameObject.tag = "Untagged"; };
        _navMeshAgent.enabled = true;

        onPlace?.Invoke();
    }

    public override void GetDamage(float damage, DamageType damageType)
    {
        base.GetDamage(damage, damageType);
    }

    protected override void Die(bool isKilled)
    {
        base.Die(isKilled);
        //ChangeMaterial(_deadMaterial);

        //Destroy(this.gameObject);
    }

    public override void Attack(Transform target)
    {
        if (_prefabProjectile == null)
            return;

        ShootProjectile(target);
    }

    public override void TriggerAttack()//for Animation Event
    {
        if (_target == null)
            return;
        //Debug.Log("TriggerAttack()   " + gameObject.name);

        HealerProjectile newProjectile = ProjectilePoolManager.Instance.GetObjectFromPool<HealerProjectile>(_prefabProjectile as HealerProjectile);
        newProjectile.SetProjectileParameters(ProjectileSpeed);
        newProjectile.SubscribeOnContactWithTarget(value => HealTarget(value));
        newProjectile.StartShot(ProjectilePosition.position, ProjectilePosition.rotation,
            () => ProjectilePoolManager.Instance.ReleaseObjectToPool(_prefabProjectile.gameObject, newProjectile),
            _target);
    }

    private void HealTarget(Unit unitTarget)
    {
        if (unitTarget.IsAlive())
        {
            var unit = unitTarget;
            if (unit.DamagePopup != null && unit.DamagePopup.gameObject.activeSelf)
                unit.DamagePopup.Show(_attackDamage);
            unit.Heal(_attackDamage);
        }
    }
}

