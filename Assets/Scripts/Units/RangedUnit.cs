using Projectile;
using System;
using UnitAnimation;
using UnityEngine;

namespace Units
{
    public class RangedUnit : Unit, IDefenderShooting
    {
        [Header("Ranged Stats")]
        [SerializeField] protected float _rotationSpeed;

        [Header("Projectile parameters")]
        [SerializeField] private float _projectileSpeed;
        [SerializeField] private Transform _projectilePosition;
        [SerializeField] protected BaseProjectile _prefabProjectile;
        public float RotationSpeed { get { return _rotationSpeed; } }
        public float ProjectileSpeed { get { return _projectileSpeed; } }
        public Transform ProjectilePosition { get { return _projectilePosition; } }

        protected Transform _target;

        protected override void Awake()
        {
            base.Awake();
            if (_unitAnimationController.AttackClipHasAnimationEvent())
            {
                _unitAnimationController.AddAnimationEventToAttackClip(nameof(TriggerAttack));
            }
        }

        public virtual void Attack(Transform target)
        {
            if (_prefabProjectile == null)
                return;

            ShootProjectile(target);
        }

        public void ShootProjectile(Transform target)
        {
            if (!IsInit)
                return;
            //Debug.Log("------RangedUnit Start Animation Attack--------TIME =  " + DateTime.Now + " / " + gameObject.name);
           
            OnStartAttackVFX();
            SetAnimatorBoolByType(AnimatorType.Attack, true);
       
            _target = target;

            if (!_unitAnimationController.HasAttackClip())
                this.TriggerAttack();
        }

        public virtual void TriggerAttack()//for Animation Event
        {
            if (_target == null)
                return;
            //Debug.Log("Base.TriggerAttack()   " + gameObject.name);

            UnitProjectile newProjectile = ProjectilePoolManager.Instance.GetObjectFromPool<UnitProjectile>(_prefabProjectile as UnitProjectile);
            newProjectile.SetProjectileParameters(ProjectileSpeed);

            newProjectile.StartShot(_projectilePosition.position, _projectilePosition.rotation, delegate
            {
                ProjectilePoolManager.Instance.ReleaseObjectToPool(_prefabProjectile.gameObject, newProjectile);

                DamageTarget();

            }, _target);
        }

        private void DamageTarget()
        {
            if (_target == null)
                return;

            if (_target.TryGetComponent(out Unit targetUnit))
            {
                if (targetUnit.IsAlive())
                {
                    if (_damagePopup != null && _damagePopup.gameObject.activeSelf)
                        _damagePopup.Show(_attackDamage);
                    targetUnit.GetDamage(_attackDamage, _damageType);
                }
            }
            else if (_target.parent.parent.TryGetComponent(out Prop targetProp))
            {
                if (targetProp.IsAlive())
                {
                    if (_damagePopup != null && _damagePopup.gameObject.activeSelf)
                        _damagePopup.Show(_attackDamage);
                    targetProp.GetDamage(_attackDamage, _damageType);
                }
            }
            else
                print("target null");
            _target = null;
        }

        public override void SetPlacementMode()
        {
            //Debug.Log("Set Placement Mode() -----");
            base.SetPlacementMode();
            SetAnimatorBoolByType(AnimatorType.Placemode, true);
        }

        public override void SetMainMaterial()
        {
            //Debug.Log("Set Main Material() +++++");
            base.SetMainMaterial();
            SetAnimatorBoolByType(AnimatorType.Idle, true);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _attackRange);
        }
    }
}
