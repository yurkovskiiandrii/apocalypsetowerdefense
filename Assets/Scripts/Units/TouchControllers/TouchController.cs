using UnityEngine;

public class TouchController : MonoBehaviour
{
    [SerializeField] protected LayerMask _touchObjectLayer;

    protected Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    public virtual void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (MouseDetectorAboveUI.IsMouseOverUI(Input.mousePosition))
                return;

            var mousePositionToWorldPoint = _camera.ScreenToWorldPoint(Input.mousePosition);

            if (Physics.Raycast(mousePositionToWorldPoint, _camera.transform.forward, out RaycastHit hit, 100f, _touchObjectLayer, QueryTriggerInteraction.Ignore))
            {
                if (hit.transform.gameObject != this.gameObject)
                    return;

                var touchMenuHit = hit.transform.GetComponentInChildren<TouchMenu>();

                if (!touchMenuHit)
                    return;
                OpenTouchMenu(hit, touchMenuHit);
            }
        }
    }

    public virtual void OpenTouchMenu(RaycastHit hit, TouchMenu touchMenuHit)
    {
        //Debug.Log("--- Open Touch Menu / "+gameObject.name);
    }
}
