using UnityEngine;
using Units;

public class DefenderTouchMenuController : TouchController
{
    public override void OpenTouchMenu(RaycastHit hit,TouchMenu touchMenuHit)
    {
        base.OpenTouchMenu(hit,touchMenuHit);

        if (hit.transform.TryGetComponent(out Unit unit) && unit.IsInit)
        {
            touchMenuHit.OpenDefenderMenu(unit);
        }
    }

    public void EndPlacing()//event for close menu
    {
        HeroesManager.Instance.EndPlacingDefenderMenu();
    }
}
