using System;
using UnityEngine;

public class PropTouchController : TouchController
{
    [SerializeField] private LayerMask _passableLayer;
    [SerializeField] private PlaceableObject _placeableObject;
    [SerializeField] private Prop _prop;
    [SerializeField] private TouchMenu _touchMenu;

    public override void Update()
    {
        if (TerrainGrid.Instance.IsRedacting)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (MouseDetectorAboveUI.IsMouseOverUI(Input.mousePosition))
                return;

            RaycastHit hit;
            var mousePositionToWorldPoint = _camera.ScreenToWorldPoint(Input.mousePosition);

            if (Physics.Raycast(mousePositionToWorldPoint, _camera.transform.forward, out hit, 100f, _touchObjectLayer, QueryTriggerInteraction.Ignore))                                                                                                                           //if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, propsLayer))
            {
                //Debug.Log("hit.transform " + hit.transform.parent.parent.gameObject.name + " / hit.colider " + hit.collider.name + " / " + this.gameObject.name);
                if (hit.transform.parent.parent.gameObject == this.gameObject)
                {
                    OpenTouchMenu(() => _placeableObject.StartRedacting());
                }
                return;
            }
            else if (Physics.Raycast(mousePositionToWorldPoint, _camera.transform.forward, out hit, 100f, _passableLayer, QueryTriggerInteraction.Ignore))                                                                                                                                           //else if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, passableLayer))
            {
                if (hit.transform.parent.parent.gameObject == this.gameObject)
                {
                    OpenTouchMenu(() => _placeableObject.StartRedactingDestroyed());
                }
            }
        }
    }

    private void OpenTouchMenu(Action onStartPlacing)
    {
        if (_prop.IsInit)
        {
            _touchMenu.OpenPropMenu(_prop.UpgradeConfig, onStartPlacing);
        }
    }
}
