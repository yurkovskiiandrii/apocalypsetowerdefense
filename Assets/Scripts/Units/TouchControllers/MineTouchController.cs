using UnityEngine;

public class MineTouchController : TouchController
{
    public override void OpenTouchMenu(RaycastHit hit, TouchMenu touchMenuHit)
    {
        base.OpenTouchMenu(hit, touchMenuHit);

        if (hit.transform.TryGetComponent(out Mine mineHit) && mineHit.IsInit)
            touchMenuHit.OpenPropMenu(mineHit.UpgradeConfig);
    }
}
