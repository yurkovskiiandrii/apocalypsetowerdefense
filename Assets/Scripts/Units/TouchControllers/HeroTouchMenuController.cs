using UnityEngine;
using Units;

public class HeroTouchMenuController : TouchController
{
    public override void OpenTouchMenu(RaycastHit hit, TouchMenu touchMenuHit)
    {
        base.OpenTouchMenu(hit, touchMenuHit);

        if (hit.transform.TryGetComponent(out Unit unit) && unit.IsInit)
        {
            if (hit.transform.TryGetComponent(out HeroController heroHit))
            {
                touchMenuHit.ShowButtonHeroMenu();

                heroHit.ActivateHero();
            }
        }
    }
}

