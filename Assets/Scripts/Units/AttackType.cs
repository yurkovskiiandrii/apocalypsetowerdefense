
namespace Units
{
    public enum AttackType
    {
        Direct,
        AOE,
        Radius
    }
}