
namespace Units
{
    public enum DamageType
    {
        Impact,
        Shot,
        Magic,
        Pure
    }
}