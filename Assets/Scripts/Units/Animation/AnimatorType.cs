namespace UnitAnimation
{
    public enum AnimatorType
    {
        Idle,
        Run,
        Attack,
        GetDamage,
        Death,
        Placemode
    }
}
