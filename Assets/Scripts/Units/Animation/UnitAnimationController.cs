using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace UnitAnimation
{
    public class UnitAnimationController : MonoBehaviour
    {
        [SerializeField] private Animator _unitAnimator;
        [Header("Animator States")]
        [SerializeField] protected string _idleState;
        [SerializeField] protected string _runState;
        [SerializeField] protected string _attackState;
        [SerializeField] protected string _deathState;
        [SerializeField] protected AnimationClip _attackClip;
        [Header("Defender Animator States")]
        [SerializeField] protected string _placemodeState;
        [Header("Blinking effect during get damage")]
        [SerializeField] private float _timeBlinkingEffectOn = 0.1f;
        [SerializeField] private float _timeBlinkingEffectOff = 0.2f;
        [SerializeField] private Color _blinkEmissionColor = Color.white;
        [SerializeField] private Color _defoultEmissionColor = Color.black;
        private List<Material> _materials = new List<Material>();
        //private Color _originalEmissionColor;
        private string _emissionColorName = "_EmissionColor";
        private string _emissionKeyword = "_EMISSION";

        public float AttackAnimationDuration { get; set; }

        public Animator UnitAnimator { get => _unitAnimator; }

        public void Init()
        {
            _unitAnimator = GetComponent<Animator>();
            if (_attackClip != null)
            {
                AttackAnimationDuration = _attackClip.length;
                //Debug.Log(" AttackAnimationDuration = " + _attackClip.length + " / " + gameObject);
            }
            InitMaterials();
        }
     
        private void InitMaterials()
        {
            var renderers = GetComponentsInChildren<SkinnedMeshRenderer>();

            for (int i = 0; i < renderers.Length; i++)
            {
                if (!renderers[i].material.HasProperty(_emissionColorName))
                    continue;
                var material = renderers[i].material;
                _materials.Add(material);
                //_originalEmissionColor = _materials[i].GetColor(_emissionColorName);
                _materials[i].EnableKeyword(_emissionKeyword);
            }
        }

        public void SetAnimatorBools(bool status)
        {
            //Debug.Log(" Set Animator Bools ");
            _unitAnimator.SetBool(_idleState, status);
            _unitAnimator.SetBool(_runState, status);
            _unitAnimator.SetBool(_attackState, status);
            if (!string.IsNullOrEmpty(_placemodeState))
                _unitAnimator.SetBool(_placemodeState, status);
        }

        public void SetAnimatorBoolByName(string animationState, bool status)
        {
            _unitAnimator.SetBool(animationState, status);
        }

        public void SetAnimatorBoolByType(AnimatorType state, bool status)
        {
            if (!IsNullOrEmptyParametersName(state))
                _unitAnimator.SetBool(GetAnimatorStateByType(state), status);
        }

        private void SetAnimatorTrigger(AnimatorType state)
        {
            if (!IsNullOrEmptyParametersName(state))
                _unitAnimator.SetTrigger(GetAnimatorStateByType(state));
        }

        private bool IsNullOrEmptyParametersName(AnimatorType state)
        {
            var parametersName = GetAnimatorStateByType(state);
            return string.IsNullOrEmpty(parametersName);
        }

        public void PlayAnimationGetDamage()
        {
            PlayBlinkEffect();
        }

        public void PlayAnimationDeath()
        {
            SetAnimatorBools(false);
            SetAnimatorTrigger(AnimatorType.Death);
        }

        public string GetAnimatorStateByType(AnimatorType state) => state switch
        {
            AnimatorType.Idle => _idleState,
            AnimatorType.Run => _runState,
            AnimatorType.Attack => _attackState,
            AnimatorType.Death => _deathState,
            AnimatorType.Placemode => _placemodeState,
            _ => null
        };

        public void AddAnimationEventToAttackClip(string animEventName)
        {
            if (AttackClipAnimationEventIsEmpty())
            {
                var animEvents = _attackClip.events;
                animEvents[0].functionName = animEventName;
                _attackClip.events = animEvents;
            }
            else
            {
                AnimationEvent animEvent = new AnimationEvent();
                animEvent.time = _attackClip.length / 2;
                animEvent.functionName = animEventName;
                _attackClip.AddEvent(animEvent);
            }
        }

        public bool AttackClipHasAnimationEvent()
        {
            return ((HasAttackClip() && _attackClip.events.Length == 0) || AttackClipAnimationEventIsEmpty());
        }

        private bool AttackClipAnimationEventIsEmpty()
        {
            return (HasAttackClip() && _attackClip.events.Length > 0 && string.IsNullOrEmpty(_attackClip.events[0].functionName));
        }

        public bool HasAttackClip() => _attackClip != null;

        [ContextMenu("GetDamage")]
        public void PlayBlinkEffect()
        {
            foreach (var material in _materials)
            {
                material.DOColor(_blinkEmissionColor, _emissionColorName, _timeBlinkingEffectOn)
                        .OnComplete(() => material.DOColor(_defoultEmissionColor, _emissionColorName, _timeBlinkingEffectOff));
            }
        }
    }
}
