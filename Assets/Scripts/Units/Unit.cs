using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.Events;
using UnitAnimation;
using Ragdoll;

namespace Units
{
    [RequireComponent(typeof(RagdollController))]

    public class Unit : MonoBehaviour
    {
        [Header("Unit parameters")]
        [SerializeField] protected UnitAnimationController _unitAnimationController;
        [SerializeField] protected string _enemyTag;
        [SerializeField] protected string _unitName;
        [Space]
        [SerializeField] protected ArmorType _armorType;
        [SerializeField] protected AttackType _attackType;
        [SerializeField] protected DamageType _damageType;
        [Min(1)]
        [SerializeField] protected float _healthPoints;
        [SerializeField] protected float _armor;
        [SerializeField] protected float _speedByCell;
        [SerializeField] protected float _attackDamage;
        [SerializeField] protected float _attackCooldown;
        [SerializeField] protected float _attackRange;
        [SerializeField] protected float _distanceDecision = 2f;
        [SerializeField] protected float _speedAcceleraion = 10f;
        [Header("UI Options")]
        [SerializeField] private float _hpShowTime = 1f;
        [SerializeField] private bool _showHpAlways = true;
        [SerializeField] private UnitGameUI _gameUIPrefab;

        [Header("Debug")]
        [SerializeField] protected DamagePopup _damagePopup;

        [Header("Invader Options")]
        [SerializeField] private SlowDebuffVFXController _slowDebuffVFX;
        [SerializeField] private Sprite _iconInvader;
        [SerializeField] private int _forceInvader;

        [Header("Defender Options")]
        [SerializeField] protected bool _isDefender;
        [SerializeField] private UnitPlacingUI _placingUI;
        [SerializeField] private LevelProgressionConfig _upgradeConfig;
        [SerializeField] private DefenderOptions _defenderOptions;
        [SerializeField] private UnityEvent _onHeal;
        [Header("State")]
        [SerializeField] protected Material _placementManterial;
        [SerializeField] protected Material _mainMaterial;
        [SerializeField] protected Material _deadMaterial;
        [Space]
        [SerializeField] private UnityEvent onDamageGet;

        [Header("Events for visual effects")]
        [Space]
        [SerializeField] private UnityEvent _onDeadVFX;
        [SerializeField] private UnityEvent _onRecoveryVFX;
        [SerializeField] private UnityEvent _onLiveVFX;
        [SerializeField] private UnityEvent _onStartAttackVFX;

        public string EnemyTag { get { return _enemyTag; } }
        public float SpeedByCell { get { return _speedByCell; } }
        public float AttackRange { get { return _attackRange; } }
        public float AttackCooldown { get { return _attackCooldown; } }
        public float AttackDamage { get { return _attackDamage; } }
        public float HealthPoints { get { return _healthPoints; } }
        public float Armor { get { return _armor; } }
        public int ForceInvader { get => _forceInvader; }
        public float DistanceDecision => _distanceDecision;
        public bool IsInit { get; private set; }

        public string Name { get => _unitName; }
        public List<Transform> PossibleTargets { get { return _possibleTargets; } }
        public DefenderOptions DefenderOptions { get { return _defenderOptions; } }
        public Sprite SpriteInvader { get => _iconInvader; }
        public DamageType DamageType { get { return _damageType; } }
        public DamagePopup DamagePopup { get { return _damagePopup; } }
        public UnitPlacingUI PlacingUI { get { return _placingUI; } }
        public Transform Transform { get { return _transform; } }
        public BehaviorTree BehaviorTree { get { return _behaviorTree; } }
        public NavMeshAgent NavMeshAgent { get { return _navMeshAgent; } }
        public UnitGameUI GameUI { get { return _gameUI; } }
        public Slider HpSlider { get { return _hpSlider; } }
        public RagdollController RagdollController { get; set; }
        public LevelProgressionConfig UpgradeConfig => _currentUpgradeConfig;
        public RecoveryController RecoveryController => _recoveryController;

        public Cell CurrentCell { get; set; }

        private const string ThisLayer = "Unit";

        private bool _ragdollIsUsed = false;
        private float _startByCellSpeed;
        private float _startAttackCooldown;
        private float _startAnimatorSpeed;

        private VariableSynchronizer _synchronizer;
        private Func<bool> _onHeroIsDodge;
        private List<Transform> _possibleTargets;

        protected LevelProgressionConfig _currentUpgradeConfig;
        protected Action onReachDestination;
        protected Transform _transform;
        protected NavMeshAgent _navMeshAgent;
        protected BehaviorTree _behaviorTree;
        protected UnitGameUI _gameUI;
        protected Slider _hpSlider;
        protected Rigidbody _rigidbody;
        protected Collider _collider;
        protected Vector3 _gameUIPosition = new Vector3(0, 2.5f, 0);
        protected float _currentHealthPoints;
        protected RecoveryController _recoveryController;

        public Action<bool> onDie;

        protected virtual void Awake()
        {
            if (IsInit)
                return;

            Debug.Log("Awake() Unit / " + gameObject.name);
            IsInit = false;

            RagdollController = GetComponent<RagdollController>();
            _rigidbody = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();
            _behaviorTree = GetComponent<BehaviorTree>();
            _synchronizer = GetComponent<VariableSynchronizer>();
            _navMeshAgent = GetComponent<NavMeshAgent>();

            _unitAnimationController.Init();
            _navMeshAgent.updatePosition = true;
            _navMeshAgent.acceleration = _speedAcceleraion;
            _transform = transform;
            _gameUI = null;

            if (TryGetComponent(out RecoveryController recoveryController))
                _recoveryController = recoveryController;

            if (_upgradeConfig != null)
            {
                InitDefenderUpgradeParameters();
            }

            _startByCellSpeed = _speedByCell;
            _startAttackCooldown = _attackCooldown;
            _startAnimatorSpeed = _unitAnimationController.UnitAnimator.speed;
        }

        private void UpdateDefenderParameters()
        {
            var currentProgress = _currentUpgradeConfig.GetCurrentProgress();
            _unitName = _currentUpgradeConfig.GetUnitName;
            _healthPoints = currentProgress.MaxHp;
            _armor = currentProgress.Armor;
            _speedByCell = currentProgress.Speed;
            _attackDamage = currentProgress.Damage;
            _attackRange = currentProgress.AttackRange;
            _attackCooldown = currentProgress.AttackCooldown;
            RecoveryController.RecoveryTime = currentProgress.RecoveryTime;
        }

        private void InitDefenderUpgradeParameters()
        {
            //Debug.Log("InitDefenderUpgradeParameters() / " + gameObject.name);
            _currentUpgradeConfig = _upgradeConfig.Clone();
            UpdateDefenderParameters();
            UpgradeManager.Instance.SubscribeOnUpgrade(UpdateDefenderParameters);
        }

        public virtual void Init(Action<bool> OnDie, Action OnUnitReachDestination, Func<bool> OnHeroIsDodge = null)
        {
            Debug.Log(" Init UNIT / " + gameObject.name);

            onDie += OnDie;
            _onHeroIsDodge += OnHeroIsDodge;
            onReachDestination += OnUnitReachDestination;
            //_synchronizer.Tick();
            _currentHealthPoints = _healthPoints;
            _ragdollIsUsed = false;

            if (RagdollController.HasRagdoll())
                RagdollController.SubscribeOnEndOfRagdoll(delegate
                {
                    if (!IsAlive())
                    { UnitIsDie(true); Debug.Log("Shoot +  UnitIsDie After Ragdoll>>>>  " + gameObject.name); }
                });

            IsInit = true;

            InitGameUI();

            if (_isDefender && _recoveryController != null)
                _recoveryController.Init(RestartDefender);
        }

        protected void InitGameUI()
        {
            if (_showHpAlways && _gameUI == null)
            {
                _gameUI = GameUIPoolManager.Instance.GetObjectFromPool<UnitGameUI>(_gameUIPrefab);
                SetGameUI(_gameUI);
            }
        }

        public virtual void SetGameUI(UnitGameUI gameUI)
        {
            _gameUI = gameUI;
            _gameUI.transform.SetParent(_transform);
            _gameUI.transform.localPosition = _gameUIPosition;
            _hpSlider = _gameUI.GetSliderHpBar();
            _hpSlider.value = _hpSlider.maxValue;
        }

        public void UseRagdoll(float explosionForce, Vector3 explosionPosition, float explosionRadius, float upwardsModifier)
        {
            RagdollController.UseRagdoll(explosionForce, explosionPosition, explosionRadius, upwardsModifier);
            _ragdollIsUsed = true;
            //Debug.Log("UseRagdoll() + _ragdollIsUsed == true");
        }

        public virtual void GetDamage(float damage, DamageType damageType)
        {
            if (!IsInit)
                return;
            if (!IsAlive())
                return;
            if (_onHeroIsDodge?.Invoke() == true)
                return;

            var currentDamage = damage - _armor;

            if (currentDamage < 0)
                return;

            onDamageGet.Invoke();


            _unitAnimationController.PlayAnimationGetDamage();

            _currentHealthPoints -= currentDamage;
            _currentHealthPoints = Mathf.Clamp(_currentHealthPoints, 0, _healthPoints);
            //Debug.Log("///currentDamage =  " + currentDamage + "_currentHealthPoints = " + _currentHealthPoints + " / " + gameObject.name);

            ShowHP();

            onDamageGet.Invoke();

            CheckDeath();
        }

        private void ShowHP()
        {
            if (!_showHpAlways)
                StartCoroutine(ShowHPCoroutine());

            int hpRocentage = (int)(_currentHealthPoints / _healthPoints * 100f);
            _hpSlider.value = hpRocentage / 100f;
        }

        private IEnumerator ShowHPCoroutine()
        {
            _hpSlider.gameObject.SetActive(true);
            yield return new WaitForSeconds(_hpShowTime);
            _hpSlider.gameObject.SetActive(false);
        }

        public void CheckDeath()
        {
            if (_currentHealthPoints <= 0)
            {
               _unitAnimationController.PlayAnimationDeath();

                Die(true);
                return;
            }
            _ragdollIsUsed = false;
        }

        public bool IsAlive() => _currentHealthPoints > 0;
        public bool IsDamaged() => IsAlive() && _currentHealthPoints < _healthPoints;

        protected virtual void Die(bool isKilled)
        {
            if (!IsInit)
                return;

            if (_gameUI != null)
            {
                GameUIPoolManager.Instance.ReleaseObjectToPool(_gameUIPrefab.gameObject, _gameUI);
                _gameUI = null;
            }

            IsInit = false;
            _synchronizer.Tick();

            if (isKilled && _ragdollIsUsed)
            {
                Debug.Log("Die +  Unit USE Ragdoll >>>>");
                return;
            }

            Debug.Log("Die +  NOT Using Ragdoll !!!!!!  " + gameObject.name);
            UnitIsDie(isKilled);
        }

        public void UnitIsDie(bool isKilled)
        {
            _ragdollIsUsed = false;
            _navMeshAgent.enabled = false;
            onDie?.Invoke(isKilled);
            onDie = null;
            OnDeadFVX();

            if (_isDefender)
            {
                DefenderIsDie();
                return;
            }
        }

        public void DefenderIsDie()
        {
            _recoveryController.SetRecoveryPrice();
            _behaviorTree.DisableBehavior(false);
            SetDeadMaterial();
            Debug.Log("Defender Dead !!!!");
        }

        private void RestartDefender()
        {
            Debug.Log("Restart Defender");
            OnLiveFVX();
            BehaviorTree.EnableBehavior();
            InitDefenderAfterDie();
            NavMeshAgent.enabled = true;
        }

        public void InitDefenderAfterDie()
        {
            Init(delegate { LevelController.Instance.RemoveDefender(this); }, onReachDestination);
            SetMainMaterial();
            LevelController.Instance.AddDefender(this);
        }

        public void OnReachDestination()
        {
            //Debug.Log(" OnReachDestination() / Die /  " + gameObject.name);
            onReachDestination?.Invoke();
            Die(false);

            if (!_isDefender)
                onReachDestination = null;
        }

        public void ActivateSlowDebuff(float time, float multiplierSlowSpeed, float multiplierSlowAttack)
        {
            if (_slowDebuffVFX == null)
                return;

            _speedByCell = _speedByCell == _startByCellSpeed ? _speedByCell * multiplierSlowSpeed : _startByCellSpeed;
            _attackCooldown = _attackCooldown == _startAttackCooldown ? _attackCooldown * multiplierSlowAttack : _startAttackCooldown;
            _navMeshAgent.speed = _speedByCell;

            _unitAnimationController.UnitAnimator.speed = _speedByCell;//test

            _slowDebuffVFX.ActivateSlowDebuff(time, () =>
            {
                _speedByCell = _startByCellSpeed;
                _attackCooldown = _startAttackCooldown;
                _navMeshAgent.speed = _speedByCell;

                _unitAnimationController.UnitAnimator.speed = _startAnimatorSpeed;//test
            });
        }

        public void Heal(float coeficientHP)
        {
            if (!IsAlive() || _currentHealthPoints == _healthPoints)
                return;

            _onHeal?.Invoke();

            _currentHealthPoints += coeficientHP;
            _currentHealthPoints = Mathf.Clamp(_currentHealthPoints, 0, _healthPoints);

            ShowHP();

            Debug.Log("HEALED > " + gameObject.name);
        }

        protected void ChangeMaterial(Material material)
        {
            if (material == null)
                return;
            foreach (Transform child in _transform)
            {
                if (child.gameObject.layer == LayerMask.NameToLayer(ThisLayer))
                {
                    //Debug.Log("ChangeMaterial " + child.gameObject.name);
                    if (child.gameObject.TryGetComponent(out MeshRenderer meshRenderer))
                        meshRenderer.material = material;
                }
            }
        }

        public virtual void SetDeadMaterial() => ChangeMaterial(_deadMaterial);

        public virtual void SetMainMaterial()
        {
            ChangeMaterial(_mainMaterial);
        }

        public virtual void SetPlacementMode()
        {
            ChangeMaterial(_placementManterial);
        }

        public void PlayAnimationRun()
        {
            SetAnimatorBoolByType(AnimatorType.Run, true);
            _navMeshAgent.speed = _speedByCell;
            // Debug.Log("UNIT Animation Run /_navMeshAgent.speed ======" + _navMeshAgent.speed + " / " + gameObject.name);
        }

        public void SetAnimatorBoolByName(string animationState, bool status)
        {
            _unitAnimationController.SetAnimatorBoolByName(animationState, status);
        }

        public void SetAnimatorBoolByType(AnimatorType state, bool status)
        {
            _unitAnimationController.SetAnimatorBools(false);
            _unitAnimationController.SetAnimatorBoolByType(state, status);
        }

        public void SubscribeOnDeath(Action onDeath)
        {
            onDie += delegate (bool isKiiled) { onDeath.Invoke(); };
        }

        public void SubscribeOnDeath(Action<Unit> onDeath)
        {
            onDie += delegate (bool isKiiled) { onDeath.Invoke(this); };
        }

        public void OnDeadFVX() => _onDeadVFX?.Invoke();

        public void OnRecoveryFVX() => _onRecoveryVFX?.Invoke();

        public void OnLiveFVX() => _onLiveVFX?.Invoke();

        protected void OnStartAttackVFX() => _onStartAttackVFX?.Invoke();

        private void OnDrawGizmos()
        {
            if (!IsInit)
                return;
            Gizmos.color = Color.red;
            Gizmos.DrawLineList(_navMeshAgent.path.corners);
        }
    }
}
