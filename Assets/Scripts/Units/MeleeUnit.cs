using System;
using UnitAnimation;
using UnityEngine;

namespace Units
{
    public class MeleeUnit : Unit
    {
        [Header("Melee Stats")]
        [SerializeField] private float _detectionRange;
        public float DetectionRange { get { return _detectionRange; } }

        private Transform _target;

        protected override void Awake()
        {
            base.Awake();
            if (_unitAnimationController.AttackClipHasAnimationEvent())
            {
                _unitAnimationController.AddAnimationEventToAttackClip(nameof(TriggerAttack));
            }
        }

        public void Attack(Transform target)
        {         
            //Unit targetUnit = target.GetComponent<Unit>();
            //print(gameObject.name + " attack " + target.name);
            SetAnimatorBoolByType(AnimatorType.Attack, true);

            _target = target;

            if (!_unitAnimationController.HasAttackClip())
                TriggerAttack();
        }

        public void TriggerAttack()//for Animation Event
        {
            DamageTarget();
        }
       
        private void DamageTarget()
        {
            if (_target == null)
                return;
            //Debug.Log("-------------MeleeUnit / DamageAfterAttack()"+ gameObject.name);

            if (_target.TryGetComponent(out Unit targetUnit))
            {
                if (targetUnit.IsAlive())
                {
                    if (_damagePopup != null && _damagePopup.gameObject.activeSelf)
                        _damagePopup.Show(_attackDamage);
                    targetUnit.GetDamage(_attackDamage, _damageType);
                }
            }
            else if (_target.TryGetComponent(out Prop targetProp))
            {
                if (targetProp.IsAlive())
                {
                    if (_damagePopup != null && _damagePopup.gameObject.activeSelf)
                        _damagePopup.Show(_attackDamage);
                    targetProp.GetDamage(_attackDamage, _damageType);
                }
            }
            else
            {
                //print("-------MeleeUnit Attack------------ target null");
                //SetAnimatorBoolByType(AnimatorType.Attack, false);
            }
            _target = null;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _detectionRange);

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _attackRange);
        }
    }
}
