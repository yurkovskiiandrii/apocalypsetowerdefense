using UnityEngine;
using Units;

public class MeleeInvader : MeleeUnit
{
    [SerializeField]
    private string _mainTargetTag = "MainTarget";
    public Transform MainTarget { get; private set; }

    [SerializeField]
    private string _propsTag = "Props";

    protected override void Awake()
    {
        MainTarget = GameObject.FindWithTag(_mainTargetTag).GetComponent<Transform>();

        base.Awake();
        _enemyTag = "Defender";
    }

    public override void GetDamage(float damage, DamageType damageType)
    {
        base.GetDamage(damage, damageType);
        //Debug.Log("* MeleeInvader / GetDamage /_currentHealthPoints = "+ _currentHealthPoints);
    }
}