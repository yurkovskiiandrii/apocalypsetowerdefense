using UnityEngine;
using Units;

public class RangedInvader : RangedUnit
{
    [SerializeField]
    private string _mainTargetTag = "MainTarget";
    public Transform MainTarget{ get; private set; }

    [SerializeField] private string _propsTag = "Props";

    protected override void Awake ()
    {
        MainTarget = GameObject.FindWithTag(_mainTargetTag).GetComponent<Transform>();

        base.Awake();
        _enemyTag = "Defender"; 
    }

    /*private void Init()
    {
        Cell currentCell = TerrainGrid.Instance.GetCell(TerrainGrid.Instance.GetGridPosition(_transform.position));
        Cell poiCell = TerrainGrid.Instance.GetCell(TerrainGrid.Instance.GetGridPosition(_poi.transform.position));

        List<Vector3> path = PathFinder.Instance.GetVectorPath(currentCell, poiCell);

        //MoveOnPath(path);
    }

    private void MoveOnPath(List<Vector3> path)
    {
        //transform.DOPath(path.ToArray(), _speedByCell * path.Count);
        //StartCoroutine(ShootAtEnemy(_poi));
        
    }

    private IEnumerator ShootAtEnemy(GameObject poi)
    {
        while(true)
        {
            //if (_attackDistance < Vector3.Distance(_transform.position, poi.transform.position))
            //{
                //var newProjectile = _projectilesPool.Get();
                //newProjectile.ShootProjectile(_projectilePosition.position, _poi.transform.position, delegate { _projectilesPool.Release(newProjectile); });

                yield return new WaitForSeconds(_attackSpeed);
            //}
            var newProjectile = _projectilesPool.Get();
            newProjectile.ShootProjectile(_projectilePosition.position, _poi.transform.position, delegate { _projectilesPool.Release(newProjectile); });

            yield return new WaitForSeconds(_attackSpeed);
        }
    }*/


}
