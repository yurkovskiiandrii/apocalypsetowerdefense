using System;
using System.Collections;
using UnityEngine;

public class SlowDebuffVFXController : MonoBehaviour
{
    [SerializeField] private GameObject _slowDebuffVFX;
    [SerializeField] private ParticleSystem _debuffEndEffect;
    private Coroutine _coroutineSlowDebuff;
    protected Action _onSlowDebuffEnd;

    void Awake()
    {
        _coroutineSlowDebuff = null;
        _slowDebuffVFX.SetActive(false);
    }

    public void ActivateSlowDebuff(float time, Action onSlowDebuffEnd)
    {
        _onSlowDebuffEnd = onSlowDebuffEnd;

        if (_coroutineSlowDebuff != null)
            StopProjectileCoroutine(_coroutineSlowDebuff);

        _coroutineSlowDebuff = StartCoroutine(TimerSlowDebuff(time));

        ActivateSlowDebuffEffect();
    }

    public void ActivateSlowDebuffEffect()
    {
        _slowDebuffVFX.SetActive(true);
    }

    protected IEnumerator TimerSlowDebuff(float time)
    {
        yield return new WaitForSeconds(time);

        _slowDebuffVFX.SetActive(false);
        _debuffEndEffect.Play();
        _onSlowDebuffEnd?.Invoke();
    }

    protected void StopProjectileCoroutine(Coroutine coroutine)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
    }
}
