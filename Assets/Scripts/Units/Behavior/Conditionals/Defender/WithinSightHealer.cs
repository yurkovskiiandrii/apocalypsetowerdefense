using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
using System.Linq;
using Units;

public class WithinSightHealer : Conditional
{
    public float fieldOfViewAngle;
    public SharedString targetTag;
    public SharedTransform target;

    public SharedTransformList possibleTargets;

    public SharedBool isEnemyNear;

    public override void OnAwake()
    {
        //Debug.Log("BehaviorTree WithinSight / Awake / " + gameObject.name);
        LevelController.Instance.SubscribeOnUnitAdd(targetTag.Value, value => AddPossibleTarget(value));
        LevelController.Instance.SubscribeOnUnitRemove(targetTag.Value, value => RemovePossibleTarget(value));
    }

    public override void OnStart()
    {
        //Debug.Log("BehaviorTree WithinSight / Start / " + gameObject.name);
        if (possibleTargets.Value.Count == 0)
            target.Value = null;
        
        possibleTargets.Value = LevelController.Instance.GetUnitsTransformsByTag(targetTag.Value);
    }

    public override TaskStatus OnUpdate()
    {
        if (possibleTargets.Value.Count == 0)
            return ReturnFailure();

        UpdateTarget();

        //Debug.Log("Target / " + " / " + target.Value.name + " / " + target.Value.gameObject.activeSelf + "  /  " + gameObject.name);
        if (!target.IsNone && target.Value != null)
            return TaskStatus.Success;

        return ReturnFailure();
    }

    public void UpdateTarget()
    {
        for (int i = 0; i < possibleTargets.Value.Count; ++i)
        {
            if (possibleTargets.Value[i] == null)
                return;

            if (possibleTargets.Value[i] == this.transform || !possibleTargets.Value[i].GetComponent<Unit>().IsDamaged())
                continue;

            if (IsWithinSight(possibleTargets.Value[i], fieldOfViewAngle))
            {
                if (target.Value == null)
                {
                    target.Value = possibleTargets.Value[i];
                }
                else if (Vector3.Distance(transform.position, target.Value.position) > Vector3.Distance(transform.position, possibleTargets.Value[i].position))
                    target.Value = possibleTargets.Value[i];
            }
        }
        //Debug.Log("Target / " + " / " + target.Value.name + " / " + target.Value.gameObject.activeInHierarchy + "  /  " + gameObject.name);
    }

    private void AddPossibleTarget(Unit value)
    {
        possibleTargets.Value.Add(value.transform);
        UpdateTarget();
        OnUpdate();
    }

    private void RemovePossibleTarget(Unit value)
    {
        possibleTargets.Value.Remove(value.transform);
        target.Value = null;

        if (possibleTargets.Value.Count == 0)
            return;

        UpdateTarget();
        OnUpdate();
    }

    public bool IsWithinSight(Transform targetTransform, float fieldOfViewAngle)
    {
        if (transform == null)
            return false;
        Vector3 direction = targetTransform.position - transform.position;
        return Vector3.Angle(direction, transform.forward) < fieldOfViewAngle;
    }

    public TaskStatus ReturnFailure()
    {
        isEnemyNear.Value = false;
        return TaskStatus.Failure;
    }
}
