using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

public class WithinReachManyEnemies : Conditional
{
    public SharedString targetTag;

    public SharedInt minTargetsCount;
    public SharedInt nearTargetsCount;
    //public SharedInt ActionsCount;

    public SharedFloat reachDistance;

    public SharedTransformList possibleTargets;
    public SharedTransformList nearTargets;

    public override void OnStart()
    {
        nearTargetsCount.SetValue(0);
        //if (possibleTargets.Value.Count == 0 || possibleTargets.Value.Count != LevelController.Instance.GetUnitsCountByTag(targetTag.Value))
        //possibleTargets.Value = LevelController.Instance.GetUnitsTransformsByTag(targetTag.Value);
    }

    public override TaskStatus OnUpdate()
    {
        nearTargets.Value.Clear();
        nearTargetsCount.SetValue(0);

        if (GetNearTargetsCount() >= minTargetsCount.Value)
        {
            string debugLog = "near enemies: ";
            nearTargets.Value.ForEach(x => debugLog += ", " + x.name);
            Debug.Log(debugLog);
            return TaskStatus.Success;
        }
  
        return TaskStatus.Failure;
    }

    private int GetNearTargetsCount()
    {
        possibleTargets.Value = LevelController.Instance.GetUnitsTransformsByTag(targetTag.Value);

        int count = 0;
        foreach (var target in possibleTargets.Value)
            if (Vector3.Distance(transform.position, target.position) <= reachDistance.Value)
            {              
                nearTargets.Value.Add(target);
                count++;
            }

        nearTargetsCount.Value = count;
        return count;
    }
}
