using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class IsRangerDefenderSkillActive : Conditional
{
    private RangedDefender _defender;

    public override void OnStart()
    {
        _defender = GetComponent<RangedDefender>();
    }


    public override TaskStatus OnUpdate()
    {
        if (_defender.TryActivateSkill())
            return TaskStatus.Success;
        return TaskStatus.Failure;
    }
}
