using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

public class WithinReach : Conditional
{
    public SharedFloat reachDistance;

    public SharedTransform target;

    public SharedBool isEnemyNear;

    public override TaskStatus OnUpdate()
    {
        if (target.Value != null && (Vector3.Distance(transform.position, target.Value.position) <= reachDistance.Value))
        {
            isEnemyNear.Value = true;
            return TaskStatus.Success;
        }
        isEnemyNear.Value = false;
        return TaskStatus.Failure;
    }
}
