using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnitAnimation;
using UnityEngine;
using Units;

public class PlayIdleAnimation : Conditional
{
    private Unit _unit;
    public SharedTransform target;
    public SharedTransformList possibleTargets;

    public override void OnAwake()
    {
        _unit = GetComponent<Unit>();
    }

    public override TaskStatus OnUpdate()
    {
        _unit.SetAnimatorBoolByType(AnimatorType.Idle, true);

        if (target.Value == null && possibleTargets.Value.Count == 0)
        {
            return TaskStatus.Failure;
        }
        return TaskStatus.Success;
    }
}
