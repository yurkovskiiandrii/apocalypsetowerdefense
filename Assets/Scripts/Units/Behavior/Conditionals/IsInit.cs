using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;

public class IsInit : Conditional
{
    public SharedBool isInit;

    public override TaskStatus OnUpdate()
    {
        if (isInit.Value)
            return TaskStatus.Success;
        return TaskStatus.Failure;
    }
}
