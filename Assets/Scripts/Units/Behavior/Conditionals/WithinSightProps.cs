using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AI;
using Units;

public class WithinSightProps : Conditional
{
    public LayerMask propsLayer;

    public SharedTransform mainTarget;
    public SharedTransform target;

    public SharedBool isEnemyNear;
    public SharedBool isPathComplete;

    private NavMeshAgent _navMeshAgent;

    private Unit _unit;

    private Vector3 _unitPositionRay;
    private Vector3 _mainTargetPositionRay;

    public override void OnStart()
    {
        _unit = GetComponent<Unit>();
        _navMeshAgent = GetComponent<NavMeshAgent>();

        _unitPositionRay = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        _mainTargetPositionRay = new Vector3(mainTarget.Value.position.x, mainTarget.Value.position.y - 0.5f, mainTarget.Value.position.z);
    }
    public override TaskStatus OnUpdate()
    {
        if (isEnemyNear.Value || isPathComplete.Value)
        {
            if (isPathComplete.Value)
            {
                //Debug.Log(_unit.name + " if (isPathComplete.Value) target.Value = null");
                target.Value = null;
            }
            return TaskStatus.Failure;
        }
        //Debug.DrawLine(_unitPositionRay, _mainTargetPositionRay, Color.black);
        //Debug.Log(" transform.position " + _unitPositionRay.ToString() + " / mainTarget.Value.position " + _mainTargetPositionRay.ToString());
        if (Physics.Linecast(_unitPositionRay, _mainTargetPositionRay, out RaycastHit hit, propsLayer))
        {
            //if (hit.transform.TryGetComponent(out Mine mine))
            //{
            //    return TaskStatus.Failure;
            //}

            if (hit.transform.CompareTag("Props"))
            {
                Prop prop = hit.transform.parent.parent.GetComponent<Prop>();
                //Debug.Log(prop.gameObject.name + "/ hit");

                if (!prop.IsAttackable && _unit.TryGetComponent(out MeleeInvader melee))
                {
                    Debug.Log(_unit.name  + " prop isn't attackable " + prop.name);
                    StartMovingToDefoultTarget();
                    return TaskStatus.Running;
                }

                if (prop.IsAttackable && _unit.TryGetComponent(out RangedInvader ranger))
                {
                    target.Value = prop.GetNearestPoint(_unit.transform.position).transform;
                    Debug.Log(_unit.name + " shooting " + prop.name);
                    return TaskStatus.Success;
                }
                else if (prop.DamagingPoints.Find(x => x.Unit == _unit) != null)
                {
                    target.Value = prop.DamagingPoints.Find(x => x.Unit == _unit).transform;
                    Debug.Log(_unit.name + " continue attacking " + prop.name);
                    return TaskStatus.Success;
                }
                else if (prop.IsAttackable && prop.IsThereFreePoint())
                {
                    target.Value = prop.SetAttackingUnit(_unit).transform;
                    Debug.Log(_unit.name + " start attacking " + prop.name);//****************************
                    //Debug.Log(target.Value.parent.parent.name);
                    //Debug.Log(target.Value + " new target");
                    return TaskStatus.Success;
                }
                else
                {
                    //Debug.Log(prop.gameObject.name + "/ hit /var propsTransformHitted = Physics.SphereCastAll(");
                    var propsTransformHitted = Physics.SphereCastAll(transform.position, _unit.AttackRange, transform.forward, 10f, propsLayer);

                    List<Prop> propsHitted = new List<Prop>();
                    foreach (var propTransform in propsTransformHitted)
                    {
                        //Debug.Log("WithinSightProps / propTransform. Tag " + propTransform.transform.tag +" name = "+propTransform.transform.name);
                        if (propTransform.transform.CompareTag("Props"))
                        {
                            var propHit = propTransform.transform.parent.parent.GetComponent<Prop>();
                            if (propHit.IsAttackable)
                                propsHitted.Add(propHit);
                        }
                    }
                    foreach (var propHit in propsHitted)
                    {
                        prop = propHit;
                        if (prop.IsAttackable && _unit.TryGetComponent(out ranger))
                        {
                            target.Value = prop.GetNearestPoint(_unit.transform.position).transform;
                            Debug.Log(_unit.name + " shooting " + propHit.name);
                            return TaskStatus.Success;
                        }
                        else if (prop.DamagingPoints.Find(x => x.Unit == _unit) != null)
                        {
                            target.Value = prop.DamagingPoints.Find(x => x.Unit == _unit).transform;
                            Debug.Log(_unit.name + " continue attacking " + propHit.name);
                            return TaskStatus.Success;
                        }
                        else if (prop.IsAttackable && prop.IsThereFreePoint())
                        {
                            target.Value = prop.SetAttackingUnit(_unit).transform;
                            Debug.Log(_unit.name + " start attacking " + propHit.name);
                            //Debug.Log(target.Value.parent.parent.name);
                            //Debug.Log(target.Value + " new target");
                            return TaskStatus.Success;
                        }

                        propsHitted.Remove(propHit);
                    }
                }
            }

            //Debug.Log(prop.name + " has attacking units: " + gameObject.name);
            //if (!prop.IsAttackable)
            //{
            //    Debug.Log("prop isn't attackable");
            //    return TaskStatus.Running;
            //}
            //else
            //Debug.Log("Failure on end prop " + prop.name + " unit " + _unit.name);
            return TaskStatus.Failure;

        }
        //Debug.Log(_unit.name + " NOT HIT / TaskStatus.Running;");
        //Debug.Log(" distanceAround = " + _navMeshAgent.remainingDistance);
        StartMovingToDefoultTarget();
        return TaskStatus.Running;
    }
    private void StartMovingToDefoultTarget()
    {
        if (_navMeshAgent.remainingDistance == _navMeshAgent.stoppingDistance)
        {
            Debug.Log(_unit.name + " NOT HIT /WithinSightProps/ _unit.OnReachDestination();");
            _unit.OnReachDestination();
            return;
        }
        target.Value = mainTarget.Value;

        _unit.PlayAnimationRun();

        _navMeshAgent.isStopped = false;
        _navMeshAgent.SetDestination(target.Value.position);
        //Debug.Log(_unit.name + " StartMovingTo DEFOULT Target " + _navMeshAgent.SetDestination(target.Value.position));
    }
}
