using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
using Units;

public class WithinSight : Conditional
{
    public float fieldOfViewAngle;
    public SharedFloat reachDistance;
    public SharedString targetTag;
    public SharedTransform target;

    public SharedTransformList possibleTargets;

    public SharedBool isEnemyNear;
    private Unit _unit;
    private string _invaderTag = "Invader";
    private string _defenderTag = "Defender";

    public override void OnAwake()
    {
        _unit = GetComponent<Unit>();
        LevelController.Instance.SubscribeOnUnitAdd(targetTag.Value, value => AddPossibleTarget(value));
        LevelController.Instance.SubscribeOnUnitRemove(targetTag.Value, value => RemovePossibleTarget(value));
    }

    public override void OnStart()
    {
        if (possibleTargets.Value.Count != 0)
            return;

        possibleTargets.Value = LevelController.Instance.GetUnitsTransformsByTag(targetTag.Value);
    }

    public override TaskStatus OnUpdate()
    {
        if (possibleTargets.Value.Count == 0)
            return ReturnFailure();

        UpdateTarget();

        if (!target.IsNone && target.Value != null)
            return TaskStatus.Success;

        return ReturnFailure();
    }

    public void UpdateTarget()
    {
        if (_unit.gameObject.tag == _invaderTag)
            UpdateTargetForInvader();
        else if (_unit.gameObject.tag == _defenderTag)
            UpdateTargetForDefender();      
    }
  
    private void UpdateTargetForInvader()
    {
        for (int i = 0; i < possibleTargets.Value.Count; ++i)
        {
            if (IsWithinSight(possibleTargets.Value[i], fieldOfViewAngle))
            {
                if (target.Value == null)
                {
                    target.Value = possibleTargets.Value[i];
                    //UnityEngine.Debug.Log("Target.name =  " + possibleTargets.Value[i].name + " / " + gameObject.name);
                }
                else if (Vector3.Distance(transform.position, target.Value.position) > Vector3.Distance(transform.position, possibleTargets.Value[i].position))
                {
                    target.Value = possibleTargets.Value[i];
                }
            }
        }
    }

    private void UpdateTargetForDefender()
    {
        for (int i = 0; i < possibleTargets.Value.Count; ++i)
        {
            if (IsWithinSight(possibleTargets.Value[i], fieldOfViewAngle))//(IsWithinReach(possibleTargets.Value[i].position))
            {
                if (target.Value == null)
                {
                    target.Value = possibleTargets.Value[i];
                    //UnityEngine.Debug.Log("Target.name =  " + possibleTargets.Value[i].name + " / " + gameObject.name);
                }
            }
        }
    }

    private bool IsWithinReach(Vector3 targetPosition)
    {
        return Vector3.Distance(transform.position, targetPosition) <= reachDistance.Value;
    }

    private void AddPossibleTarget(Unit value)
    {
        possibleTargets.Value.Add(value.transform);
    }

    private void RemovePossibleTarget(Unit value)
    {
        possibleTargets.Value.Remove(value.transform);
        target.Value = null;
    }

    public bool IsWithinSight(Transform targetTransform, float fieldOfViewAngle)
    {
        if (transform == null)
            return false;
        Vector3 direction = targetTransform.position - transform.position;
        return Vector3.Angle(direction, transform.forward) < fieldOfViewAngle;
    }

    public TaskStatus ReturnFailure()
    {
        isEnemyNear.Value = false;
        return TaskStatus.Failure;
    }
}
