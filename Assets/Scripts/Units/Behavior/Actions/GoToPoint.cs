using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.AI;
using UnitAnimation;
using Units;

public class GoToPoint : Action
{
    public LayerMask propsLayer;

    public SharedFloat speed;
    public SharedTransform target;

    public SharedVector3List path;

    public SharedBool isEnemyNear;
    public SharedBool isPathComplete;
    private bool _isMoving = false;

    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    private Unit _unit;

    private Vector3 _unitPositionRay;
    private float _minDistance = 0.5f;

    public override void OnStart()
    {
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.speed = speed.Value;
        _unitPositionRay = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        _unit = GetComponent<Unit>();

        if (!isEnemyNear.Value && !_isMoving && isPathComplete.Value)
            StartMoving();
        //MoveNextCell();
    }

    public override TaskStatus OnUpdate()
    {
        //if (!_isMoving && speed.Value > 0f)        
        //  StartMove();

        bool isCanGoStraigh = CheckDistanceStraight();

        if (!_navMeshAgent.pathPending)
        {
            //Debug.Log(_unit.name + " > !_navMeshAgent.pathPending");
            if (_navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance + _unit.AttackRange / 2)
            {
                if (_navMeshAgent.path.status != NavMeshPathStatus.PathComplete && isPathComplete.Value)
                {
                    StopMoving();
                    isPathComplete.Value = false;

                    //Debug.Log(" > GoToPoint   path isnt complete for  " + _unit.name);
                    return TaskStatus.Failure;
                }
                else if ((!_navMeshAgent.hasPath || _navMeshAgent.velocity.sqrMagnitude <= 0.5f) && isPathComplete.Value && _navMeshAgent.remainingDistance < _minDistance)
                {
                    //transform.DOKill();
                    //_isMoving = false;
                    //Debug.Log(_unit.name + " > GoToPoint  invader reach Main target /OnReachDestination() " + " TargetName = " + target.Value.name + " / pos = "+ Vector3.Distance(_unit.transform.position, target.Value.position)+ " / _navMeshAgent.remainingDistance = "+ _navMeshAgent.remainingDistance);
                    _unit.OnReachDestination();
                    return TaskStatus.Success;
                }
            }
            else if (isCanGoStraigh && _isMoving)
            {
                //Debug.Log(" isCanGoStraigh && _isMoving " + isCanGoStraigh);
                //Debug.DrawLine(transform.position, target.Value.position, Color.green);

                _unitPositionRay = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
                if (Physics.Linecast(transform.position, target.Value.position, out RaycastHit hit, propsLayer))
                {
                    //Debug.Log("GoToPoint/ hit. Tag " + hit.transform.tag + " name = " + hit.transform.name);

                    if (hit.transform.CompareTag("Props"))
                    {
                        Prop prop = hit.transform.parent.parent.GetComponent<Prop>();
                        if (prop.IsAttackable && (_unit.TryGetComponent(out RangedInvader ranger) || prop.IsThereFreePoint()))
                        {
                            StopMoving();
                            isPathComplete.Value = false;

                            //Debug.Log(_unit.name + " > GoToPoint   distance straight is better, he will go through  " + prop.name);
                            return TaskStatus.Failure;
                        }
                        //Debug.Log(_unit.name + " > GoToPoint  !!!!!!!!!!  prop.  Is NOT Attackable " + prop.name);
                    }
                }
            }
            else if (_navMeshAgent.remainingDistance > _navMeshAgent.stoppingDistance && !isPathComplete.Value && !isCanGoStraigh)
            {
                //Debug.Log(_unit.name + " > GoToPoint   StartMoving !!!");
                isPathComplete.Value = true;
                StartMoving();
            }
            //Debug.Log("_navMeshAgent.remainingDistance > _navMeshAgent.stoppingDistance = " + (_navMeshAgent.remainingDistance > _navMeshAgent.stoppingDistance));
            //Debug.Log("!isCanGoStraigh = " + !isCanGoStraigh);
            //Debug.Log("!isPathComplete.Value = " + !isPathComplete.Value);
            /*else if(_navMeshAgent.path.status == NavMeshPathStatus.PathComplete && !isPathComplete.Value)
            {
                isPathComplete.Value = true;
                StartMoving();
            }*/
        }

        if (isEnemyNear.Value && _isMoving)
        {
            //Debug.Log(_unit.name + " > GoToPoint  StopMoving ");
            StopMoving();
            return TaskStatus.Failure;
        }
        else if (!isEnemyNear.Value && !_isMoving && isPathComplete.Value)
            ContinueMoving();

        //Debug.Log(_unit.name + " > GoToPoint  ContinueMoving ");
        return TaskStatus.Running;
    }

    private void StartMoving()
    {
        //Debug.Log("move start");
        _navMeshAgent.speed = speed.Value;
        _unit.PlayAnimationRun();
        _navMeshAgent.isStopped = false;
        _navMeshAgent.SetDestination(target.Value.position);
        _isMoving = true;
        //Debug.Log("start moving");
    }

    private void StopMoving()
    {
        _unit.SetAnimatorBoolByType(AnimatorType.Idle, true);
        //Debug.Log("move stop");

        _navMeshAgent.isStopped = true;
        _isMoving = false;
    }

    private void ContinueMoving()
    {
        //Debug.Log("move continue");

        if (_navMeshAgent.isStopped && _navMeshAgent.hasPath)
        {
            _navMeshAgent.speed = speed.Value;
            _unit.PlayAnimationRun();
            _navMeshAgent.isStopped = false;
        }
        else
            StartMoving();
    }

    private bool CheckDistanceStraight()
    {
        float distanceAround = _navMeshAgent.remainingDistance;
        if ((_navMeshAgent.path.status != NavMeshPathStatus.PathInvalid) && (_navMeshAgent.path.corners.Length > 1))
        {
            //Debug.Log(" distanceAround = " + distanceAround);
            distanceAround = 0;
            for (int i = 1; i < _navMeshAgent.path.corners.Length; ++i)
            {
                distanceAround += Vector3.Distance(_navMeshAgent.path.corners[i - 1], _navMeshAgent.path.corners[i]);
            }
        }

        float distanceStraight = Vector3.Distance(transform.position, target.Value.position);

        //Debug.Log(distanceAround + "/" + distanceStraight + " = " + distanceAround / distanceStraight +" >= "+ _unit.DistanceDecision);
        //Debug.Log("isCanGoStraig = "+((distanceAround / distanceStraight) >= _unit.DistanceDecision));
        return (distanceAround / distanceStraight) >= _unit.DistanceDecision;
    }

    public override void OnDrawGizmos()
    {
        if (_navMeshAgent == null)
            return;

        if (CheckDistanceStraight())
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(transform.position, target.Value.position);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, target.Value.position);

            //Debug.Log("GizmosDraw");
        }
    }
}
