using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.AI;

public class GoToInitialPosition : Action
{
    private RangedDefender _defender;

    public SharedFloat speed;
    public SharedFloat speedMultiplicator;

    public SharedVector3 initialPosition;

    private NavMeshAgent _navMeshAgent;

    public override void OnStart()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.speed = speed.Value * speedMultiplicator.Value;
        _defender = GetComponent<RangedDefender>();
        StartMove();
    }

    public override TaskStatus OnUpdate()
    {
        //if (!_isMoving && speed.Value > 0f)        
          //  StartMove();

        if (_navMeshAgent.remainingDistance == 0f)
        {
            //transform.DOKill();
            //_isMoving = false;
            _defender.DeactivateSkill();
            _navMeshAgent.speed = speed.Value;
            Debug.Log("on initial position");
            return TaskStatus.Success;          
        }

        return TaskStatus.Running;
    }

    private void StartMove()
    {
        _defender.SetAnimatorBlitzRaidBools(false);
        _defender.SetAnimatorBlitzRaidRun(true);
        _navMeshAgent.SetDestination(initialPosition.Value);
    }

}
