using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using Units;

public class BlitzRaidAttack : Action
{
    private RangedDefender defender;

    public SharedTransform target;

    public override void OnAwake()
    {
        defender = GetComponent<RangedDefender>();
    }

    public override TaskStatus OnUpdate()
    {
        if (target.IsNone || target.Value == null)
            return TaskStatus.Failure;
        defender.BlitzRaidAttack(target.Value.GetComponent<Unit>());
        target.SetValue(null);
        return TaskStatus.Success;
    }
}
