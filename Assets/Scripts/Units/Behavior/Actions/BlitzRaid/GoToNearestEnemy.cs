using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.AI;

public class GoToNearestEnemy : Action
{
    public SharedFloat speed;
    public SharedFloat speedMultiplicator;
    public SharedTransformList nearTargets;

    public SharedTransform target;

    private RangedDefender _defender;

    private NavMeshAgent _navMeshAgent;
    private NavMeshPath _navMeshPath;

    public override void OnStart()
    {
        _defender = GetComponent<RangedDefender>();

        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.speed = speed.Value * speedMultiplicator.Value;
        _navMeshPath = new NavMeshPath();
        GetNearestTarget();
        
    }

    public override TaskStatus OnUpdate()
    {
        //if (!_isMoving && speed.Value > 0f)        
        //  StartMove();
        if (target.Value == null)
        {
            _defender.SetAnimatorBlitzRaidBools(false);
            _defender.SetAnimatorBlitzRaidIdle(true);
            return TaskStatus.Failure;
        }

        if (Vector3.SqrMagnitude(transform.position - target.Value.position) < 1.5f)
        {
            //transform.DOKill();
            //_isMoving = false;
            Debug.Log("blitz raid hit " + target.Value.name);
            return TaskStatus.Success;
        }
        //if()
        
        return Move();
    }

    private void GetNearestTarget()
    {
        target.Value = nearTargets.Value[0];
        /*foreach (var nearTarget in nearTargets.Value)
        {
            if (Vector3.Distance(transform.position, target.Value.position) > Vector3.Distance(transform.position, nearTarget.position))
                target.Value = nearTarget;
        }*/

        nearTargets.Value.Remove(target.Value);
    }

    private TaskStatus Move()
    {
        if (target.Value != null)
        {
            if(!IsEnableToReach())
                return TaskStatus.Failure;
            _defender.SetAnimatorBlitzRaidBools(false);
            _defender.SetAnimatorBlitzRaidRun(true);
            _navMeshAgent.SetDestination(target.Value.position);
        }
        else
            GetNearestTarget();
        return TaskStatus.Running;
    }

    private bool IsEnableToReach()
    {
        _navMeshAgent.CalculatePath(target.Value.position, _navMeshPath);
        if (_navMeshPath.status == NavMeshPathStatus.PathComplete)
            return true;
        return false;
    }
}
