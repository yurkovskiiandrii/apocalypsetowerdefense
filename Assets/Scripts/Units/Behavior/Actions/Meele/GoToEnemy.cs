using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.AI;
using UnitAnimation;
using Units;

public class GoToEnemy : Action
{
    public SharedFloat speed;
    public SharedFloat attackRange;

    public SharedTransform target;

    public SharedBool isEnemyNear;

    private Unit _unit;

    private NavMeshAgent _navMeshAgent;
    private NavMeshPath _navMeshPath;

    public override void OnStart()
    {
        _unit = GetComponent<Unit>();

        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.speed = speed.Value;
        _navMeshPath = new NavMeshPath();
        //Debug.Log("GoToEnemy OnStart/ speed.Value ======" + speed.Value +" / "+gameObject.name);
    }

    public override TaskStatus OnUpdate()
    {
        if (target.Value == null)
            return TaskStatus.Failure;

        if (target.Value.CompareTag("Props"))
        {
            //var damagingPoint = target.Value.GetComponent<Prop>().DamagingPoints.Find(x => x.Unit == _unit);
            if (Vector3.SqrMagnitude(transform.position - target.Value.position) < attackRange.Value)
            {
                //Debug.Log(" *** GoToEnemy 1 _navMeshAgent.isStopped");
                _navMeshAgent.isStopped = true;
                return TaskStatus.Success;
            }
            /*foreach (Transform child in target.Value)
            {
                //Debug.Log("child magnitude" + Vector3.SqrMagnitude(transform.position - child.position));

                if (Vector3.SqrMagnitude(transform.position - child.position) < attackRange.Value)
                {
                    return TaskStatus.Success;
                }
            }*/
            /*Debug.Log(Vector3.SqrMagnitude(transform.position - _hit.point));
            if (Vector3.SqrMagnitude(transform.position -_hit.point) < attackRange.Value)
                return TaskStatus.Success;*/
        }
        else if (Vector3.SqrMagnitude(transform.position - target.Value.position) < attackRange.Value)
        {
            //Debug.Log(" *** GoToEnemy 2");
            _navMeshAgent.isStopped = true;
            return TaskStatus.Success;
        }
        //if()
        //Debug.Log(" *** GoToEnemy 3 Move()");
        return Move();
    }

    private TaskStatus Move()
    {
        if (target.Value != null)
        {
            if (!IsEnableToReach())
                return MoveFailed();
            _navMeshAgent.speed = speed.Value;
            _unit.PlayAnimationRun();
            _navMeshAgent.isStopped = false;
            _navMeshAgent.SetDestination(target.Value.position);
        }
        else 
            return MoveFailed();
        return TaskStatus.Running;
    }

    private TaskStatus MoveFailed()
    {
        _unit.SetAnimatorBoolByType(AnimatorType.Idle, true);

        _navMeshAgent.isStopped = true;
        return TaskStatus.Failure;
    }

    private bool IsEnableToReach()
    {
        _navMeshAgent.CalculatePath(target.Value.position, _navMeshPath);
        if (_navMeshPath.status == NavMeshPathStatus.PathComplete)
            return true;
        isEnemyNear.Value = false;
        return false;
    }
}
