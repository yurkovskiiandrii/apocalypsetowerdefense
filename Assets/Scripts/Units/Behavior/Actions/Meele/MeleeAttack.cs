using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Units;

public class MeleeAttack : Action
{
    private MeleeUnit meleeUnit;

    public SharedTransform target;

    public override void OnAwake()
    {
        meleeUnit = GetComponent<MeleeUnit>();
    }

    public override TaskStatus OnUpdate()
    {
        if (target.IsNone || target.Value == null)
            return TaskStatus.Failure;

        transform.LookAt(target.Value);
        if (target.Value.CompareTag("Props"))
        {
            meleeUnit.Attack(target.Value.parent.parent);
        }
        else
        {
            meleeUnit.Attack(target.Value);
        }
        return TaskStatus.Success;
    }
}
