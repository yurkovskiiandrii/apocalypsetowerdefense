using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Units;

public class Shoot : BehaviorDesigner.Runtime.Tasks.Action
{
    private RangedUnit shooter;

    public SharedTransform target;

    public override void OnAwake()
    {
        shooter = GetComponent<RangedUnit>();
    }

    public override TaskStatus OnUpdate()
    {
        if (target.IsNone || target.Value == null || !shooter.IsInit)
            return TaskStatus.Failure;

        //transform.LookAt(target.Value);    
        shooter.Attack(target.Value);

        return TaskStatus.Success;
    }
}
