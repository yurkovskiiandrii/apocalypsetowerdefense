using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using DG.Tweening;
using System.Collections;

public class LookAt : BehaviorDesigner.Runtime.Tasks.Action
{
    public SharedFloat reachDistance;
    public float fieldOfViewAngle;

    public SharedTransform target;
    public SharedFloat rotationSpeed;

    private Tweener _tweenerLookAt;
    private Coroutine _lookCoroutine; 
    public float _minDistance = 1f;
    public float _maxFieldOfViewAngle = 10;

    public override TaskStatus OnUpdate()
    {
        if (target.IsNone || target.Value == null)
            return TaskStatus.Failure;

        if (IsWithinReach())
        {
            if (IsWithinSight())
                return TaskStatus.Success;

            if (target.IsNone || target.Value == null)
                return TaskStatus.Failure;

            Rotate();
            //StartRotating();//test
            return TaskStatus.Running;
        }

        return TaskStatus.Failure;
    }

    private void Rotate()
    {
        if (_tweenerLookAt != null)
            return;

        var targetLoolAt = target.Value.position;
        _tweenerLookAt = transform.DOLookAt(targetLoolAt, rotationSpeed.Value);
        _tweenerLookAt.onComplete += () =>
        {
            _tweenerLookAt = null;
        };
    }

    private bool IsWithinSight()
    {
        Vector3 direction = target.Value.position - transform.position;
        var maxAngle = fieldOfViewAngle;

        if (Vector3.Distance(transform.position, target.Value.position) < _minDistance)
        {
            maxAngle = _maxFieldOfViewAngle;
        }
        bool isWithinSight = Vector3.Angle(direction, transform.forward) < maxAngle;

        return isWithinSight;
    }

    private bool IsWithinReach()
    {
        return Vector3.Distance(transform.position, target.Value.position) <= reachDistance.Value;
    }

    //test
    IEnumerator Looking()
    {
        var targetLoolAt = target.Value.position;

        Quaternion direction = Quaternion.LookRotation(targetLoolAt - transform.position);
        direction.x = 0;
        direction.z = 0;

        float time = 0;
        while (time < 1)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, direction, time);
            time += Time.deltaTime * rotationSpeed.Value;
            yield return null;
        }
        _lookCoroutine = null;
    }

    private void StartRotating()
    {
        if (_lookCoroutine != null)
            return;
        //StopCoroutineRotate();
        _lookCoroutine = StartCoroutine(Looking());
    }

    private void StopCoroutineRotate()
    {
        if (_lookCoroutine != null)
        {
            StopCoroutine(Looking());
            _lookCoroutine = null;
        }
    }
}
