
namespace Units
{
    public enum ArmorType
    {
        NoArmor,
        Light,
        Heavy,
        Magic
    }
}