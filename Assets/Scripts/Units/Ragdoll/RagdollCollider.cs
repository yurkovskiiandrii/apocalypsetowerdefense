using Ragdoll;
using UnityEngine;

public class RagdollCollider : MonoBehaviour
{
    [SerializeField] private RagdollController _ragdollController;
    private string _barrierTag = "Barrier";

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag(_barrierTag))
        {
            //Debug.Log("----Ragdoll End--------TestCollider/ OnCollisionEnter  = " + " / Collider.mame =  " + other.collider.name + "  //// " + gameObject.name);
            _ragdollController.StopRagdollCoroutine();
        }
    }
}
