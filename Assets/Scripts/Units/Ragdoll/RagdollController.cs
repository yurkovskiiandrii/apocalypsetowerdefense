using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Ragdoll
{
    public class RagdollController : MonoBehaviour
    {
        //[SerializeField] private GameObject _bodyRagdoll;
        [SerializeField] private float _timeInRagdoll = 3f;
        [SerializeField] private float _rigidbodyMassRagdoll = 3f;
        [SerializeField] private MassObjectType _massObjectType;

        private List<Collider> _ragdollColliders;
        private List<Vector3> _normalChildsPositions = new List<Vector3>();
        private Rigidbody _rigidbody;
        private Collider _collider;
        private Animator _animator;
        private NavMeshAgent _navMeshAgent;
        private Coroutine _ragdollCoroutine;

        protected Transform _transform;
        protected Action _onEndOfRagdoll;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();
            _animator = GetComponent<Animator>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _transform = transform;
            _onEndOfRagdoll = null;

            _ragdollColliders = GetComponentsInChildren<Collider>().ToList();
            foreach (var collider in _ragdollColliders)
            {
                _normalChildsPositions.Add(collider.transform.localPosition);
                //print(gameObject.name + " collider.name " + collider.gameObject.name);
            }
            ActivateRagdoll(false);
        }

        public void SubscribeOnEndOfRagdoll(Action onEndOfRagdoll)
        {
            _onEndOfRagdoll += onEndOfRagdoll;
        }

        public bool HasRagdoll()
        {
            return _massObjectType == MassObjectType.None ? false : true;
        }

        public MassObjectType GetMassObjectType() => _massObjectType;

        public void ActivateRagdoll(bool isActive)
        {
            //Debug.Log(gameObject.name + " ActivateRagdoll() " + isActive);
            foreach (Collider collider in _ragdollColliders)
            {
                collider.enabled = isActive;

                if (collider.TryGetComponent(out Rigidbody rb))
                {
                    var rigidbody = rb;//collider.GetComponent<Rigidbody>();
                    rigidbody.isKinematic = !isActive;
                    rigidbody.mass = _rigidbodyMassRagdoll;
                }
            }

            _collider.enabled = !isActive;
            _animator.enabled = !isActive;
            if (_navMeshAgent.enabled)
            {
                _navMeshAgent.updatePosition = !isActive;
                _navMeshAgent.isStopped = isActive;
            }
            _rigidbody.useGravity = !isActive;
            _rigidbody.isKinematic = true;
        }

        [ContextMenu("Ragdoll Test")]
        public void UseRagdollTest()
        {
            //ActivateRagdoll(true);
            //StartCoroutine(RagdollCoroutine());
            UseRagdoll(9, transform.position + new Vector3(0.5f, 0.5f, 0.5f), 3, 1);
        }

        public void UseRagdoll(float explosionForce, Vector3 explosionPosition, float explosionRadius, float upwardsModifier)
        {
            //Debug.Log(gameObject.name + " Use Ragdoll ");
            ActivateRagdoll(true);

            foreach (Collider collider in _ragdollColliders)
            {
                var rigidbody = collider.GetComponent<Rigidbody>();
                rigidbody.WakeUp();
                //rigidbody.velocity = Vector3.zero;
                rigidbody.AddExplosionForce(explosionForce, explosionPosition, explosionRadius, upwardsModifier, ForceMode.Impulse);
            }
            if (gameObject.activeSelf)
                _ragdollCoroutine = StartCoroutine(RagdollCoroutine());
        }

        private IEnumerator RagdollCoroutine()
        {
            yield return new WaitForSeconds(_timeInRagdoll);

            EndRagdoll();
        }

        private void EndRagdoll()
        {
            ActivateRagdoll(false);

            _transform.position = _ragdollColliders[1].transform.position;//Body Ragdoll Position

            for (int i = 1; i < _ragdollColliders.Count; i++)
            {
                //print(_ragdollColliders[i].gameObject.name);
                _ragdollColliders[i].transform.localPosition = _normalChildsPositions[i];
                _ragdollColliders[i].transform.rotation = _transform.rotation;
            }
            //Debug.Log(gameObject.name + " Ragdoll End ");

            _onEndOfRagdoll?.Invoke();
        }

        public void StopRagdollCoroutine()
        {
            if (_ragdollCoroutine != null)
            {
                StopCoroutine(_ragdollCoroutine);
                _ragdollCoroutine = null;
                EndRagdoll();
            }
        }
    }
}
