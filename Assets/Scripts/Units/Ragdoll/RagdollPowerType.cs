
namespace Ragdoll
{
    public enum RagdollPowerType
    {
        None,
        Light,
        Medium,
        Heavy
    }
}