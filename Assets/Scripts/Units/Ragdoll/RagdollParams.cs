using UnityEngine;

namespace Ragdoll
{
    [System.Serializable]
    public class RagdollParams
    {
        [SerializeField] private float _explosionRange = 5;
        [SerializeField] private float _upwardsModifier = 1;

        public float ExplosionRange => _explosionRange;

        public float UpwardsModifier => _upwardsModifier;
    }
}
