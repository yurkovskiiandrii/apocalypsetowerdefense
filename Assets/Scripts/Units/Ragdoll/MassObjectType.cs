
namespace Ragdoll
{
    public enum MassObjectType
    {
        None,
        Light,
        Medium,
        Heavy
    }
}
