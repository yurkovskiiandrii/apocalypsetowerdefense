using UnityEngine;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine.Events;

public class TerrainGrid : SingletonComponent<TerrainGrid>
{
    private static float _floorSizeCoef = 1;//5f;

    private static int _rowsAndColumnAround = 0; //5;

    [Header("Terrain")]
    [SerializeField] private Terrain _terrain;
    [SerializeField] private float _positionYRoud = -0.7f;
    [SerializeField] private Transform _decorationsContainer;
    [SerializeField] private Transform _propsContainer;

    [SerializeField] private Vector2 _size;

    [SerializeField] private int _rowsCount;
    [SerializeField] private int _columsCount;

    [Header("Cell")]
    [SerializeField] private Vector2 _cellSize;

    [SerializeField] private GameObject _cellPrefab;

    [SerializeField] private Transform _cellsContainer;

    [SerializeField] private LayerMask _cellLayer;

    [SerializeField] private Transform _cellCleanersParent;

    [Header("Visualise")]
    [SerializeField] private bool _showCells;

    [SerializeField] private GizmosDrawer.LineWidth _lineWidth;

    [SerializeField] private Color _color;

    [Header("Another")]
    [SerializeField] private string _floorLayerName;

    [Header("NavMesh")]
    [SerializeField] private NavMeshSurface _navMeshSufrace;

    private List<Cell> _cells = new List<Cell>();

    //public Vector2 CellSize => new Vector2(_size.x / _columsCount, _size.y / _rowsCount);
    public Vector2 CellSize => new Vector2(_cellSize.x, _cellSize.y);

    private TerrainData Data => _terrain.terrainData;

    public Transform DecorationContainer => _decorationsContainer;
    public Transform PropsContainer => _propsContainer;

    public Vector3 MinPosition => new Vector3(0f, 0f, 0f);
    public Vector3 MaxPosition => new Vector3(_size.x, 0f, _size.y);
    public Vector3 HalfPosition => new Vector3(_size.x / 2f, 0f, _size.y / 2f);

    public bool IsRedacting { get; set; } = false;

    public void Init()
    {
        SpawnCells();
        SpawnFloor();
        CheckObjectsPlacedFromEditor();// need to init already placed props
        //PathFinder.Instance.SetMap(_cells);
        UpdateNavMeshGrid();
        //ClearUnusedCells();
    }

    private void SpawnCells()
    {
        _cells.Clear();

        for (int i = -_rowsAndColumnAround; i < _columsCount + _rowsAndColumnAround; i++)
        {
            for (int j = -_rowsAndColumnAround; j < _rowsCount + _rowsAndColumnAround; j++)
            {
                var cell = Instantiate(_cellPrefab, _cellsContainer).GetComponent<Cell>();
                var canPlace = !(i >= _columsCount || j >= _rowsCount || i < 0 || j < 0);

                cell.Init(UpdateNavMeshGrid, new Vector2Int(i, j), delegate { _cells.Remove(cell); }, false, canPlace);

                _cells.Add(cell);
            }
        }
        //Debug.Log("Terrain Grid SpawnCells() End");
    }

    private void ClearUnusedCells()
    {
        foreach (Transform child in _cellCleanersParent)
        {
            child.GetComponent<CellCleaner>().DeactivateCells();
        }
    }

    private void SpawnFloor()
    {
        var floor = new GameObject("Floor");
        floor.transform.SetParent(transform);
        floor.layer = LayerMask.NameToLayer(_floorLayerName);
        floor.transform.position = HalfPosition;

        var collider = floor.AddComponent<BoxCollider>();
        collider.size = new Vector3(_size.x * _floorSizeCoef, 0.1f, _size.y * _floorSizeCoef);
        collider.isTrigger = true;
    }

    private void CheckObjectsPlacedFromEditor()
    {
        var placeableObjects = new List<PlaceableObject>();

        placeableObjects.AddRange(_decorationsContainer.GetComponentsInChildren<PlaceableObject>());
        placeableObjects.AddRange(_propsContainer.GetComponentsInChildren<PlaceableObject>());

        foreach (var placeableObject in placeableObjects)
        {
            if (!placeableObject.isPlaced)
                Destroy(placeableObject.gameObject);

            else
            {
                foreach (var point in placeableObject.cellsPositions)
                {
                    var cell = GetCell(point);

                    cell.SetPlaceStatus();
                }
                if (placeableObject.TryGetComponent(out Prop prop))
                {
                    //print(placeableObject.name + " init");
                    prop.Init(delegate { UpdateNavMeshGrid(); });
                }

            }
        }
    }

    public void GenerateFied()
    {
        if (_rowsCount == 0 || _columsCount == 0)
            return;

        Data.size = new Vector3(_size.x, Data.size.y, _size.y);

        var alphas = Data.GetAlphamaps(0, 0, Data.alphamapWidth, Data.alphamapHeight);

        for (int i = 0; i < Data.alphamapHeight; i++)
        {
            for (int j = 0; j < Data.alphamapWidth; j++)
            {
                for (int layerIndex = 0; layerIndex < Data.alphamapLayers; layerIndex++)
                    alphas[i, j, layerIndex] = 0f;

                alphas[i, j, 0] = 1f;
            }
        }

        Data.SetAlphamaps(0, 0, alphas);
    }

    /*public void SpawnDecoration(GameObject prefab)
    {
        SpawnObject(prefab, _decorationsContainer);
    }

    public void SpawnProps(GameObject prefab)
    {
        SpawnObject(prefab, _propsContainer);
    }*/

    public PlaceableObject SpawnObject(GameObject prefab, Transform container, UnityAction<int> OnDefenderSpawn, UnityAction OnSpawnEnd)
    {
        var camera = Camera.main;
        var spawnPosition = new Vector3();

        //if (Physics.Raycast(camera.ScreenPointToRay(new Vector3(camera.pixelWidth / 2, camera.pixelHeight / 2, 0)), out RaycastHit hit, 100f, _cellLayer))//for camera - perspective
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out RaycastHit hit, 100.0f, _cellLayer))//for camera - orthographic
        {
            var cell = hit.transform.GetComponent<Cell>();
            if (cell == null)
            {
                cell = GetSellFromAllCellsPositions(hit.transform.position);
                //Debug.Log("!!!!!! cell == null / cell.transform.position = "+ cell.transform.position);
            }
            spawnPosition = cell.transform.position;
        }
        else
        {
            //Debug.Log("!!!!!Not Raycast");
            Vector3 screenCenter = new Vector3(camera.pixelWidth / 2, camera.pixelHeight / 2, 0);
            Vector3 worldCenter = new Vector3(camera.ScreenToWorldPoint(screenCenter).x, 0, 0);
            spawnPosition = GetClosestCellPositionToWorldPoint(worldCenter);
        }
        spawnPosition.y = 0f;
        var gridObject = Instantiate(prefab, container).GetComponent<PlaceableObject>();
        gridObject.transform.position = spawnPosition;
        gridObject.Init(OnDefenderSpawn, OnSpawnEnd);
        gridObject.StartPlacing();
        //gridObject.GetComponent<Prop>().SetPlacementMode();

        return gridObject;
    }

    private void OnDrawGizmos()
    {
        if (!_showCells || !WavesManager.IsStartedWaitingTimeForWave)
            return;

        var points = new List<Vector2Int>();

        for (int i = 0; i < _size.x; i++)
            for (int j = 0; j < _size.y; j++)
                points.Add(new Vector2Int(i, j));

        GizmosDrawer.DrawCubes(transform.position + new Vector3(CellSize.x / 2f, _positionYRoud, CellSize.y / 2f), points, CellSize, _color, _lineWidth);
    }

    public Cell GetCell(Vector3 cellPosition)
    {
        var gridPos = GetGridPosition(cellPosition);
        return GetCell(gridPos);
    }


    public Cell GetCell(Vector2Int gridPositon)
    {
        var cell = _cells.Find(cell => cell.Position == gridPositon);

        return cell;
    }

    public Vector2Int GetGridPosition(Vector3 cellPosition)
    {
        var gridPositon = new Vector2Int(0, 0);

        foreach (var cell in _cells)
        {
            if (cell.transform.position.x == cellPosition.x && cell.transform.position.z == cellPosition.z)
            {
                gridPositon = cell.Position;

                break;
            }
        }

        return gridPositon;
    }

    public Cell GetSellFromAllCellsPositions(Vector3 currentCellPosition)
    {
        Cell cell;
        var closestCell = _cells[0];
        var minDistance = float.MaxValue;

        for (int i = 0; i < _cells.Count; i++)
        {
            cell = _cells[i];
            if (!cell.IsCanPlaced())
                continue;
            var distance = Vector3.Distance(GetCellPosition(cell.Position), currentCellPosition);
            if (distance < minDistance)
            {
                closestCell = cell;
                minDistance = distance;
            }
        }
        return closestCell;
    }

    public Vector3 GetCellPosition(Vector2Int gridPositon)
    {
        var startPosition = transform.position + new Vector3(CellSize.x / 2f, 0f, CellSize.y / 2f);

        return startPosition + new Vector3(gridPositon.x * CellSize.x, 0f, gridPositon.y * CellSize.y);
    }

    public List<Vector2Int> GetPlacedCellsIndexs()
    {
        var indexs = new List<Vector2Int>();
        var placeableObjects = new List<PlaceableObject>();

        placeableObjects.AddRange(_decorationsContainer.GetComponentsInChildren<PlaceableObject>());
        placeableObjects.AddRange(_propsContainer.GetComponentsInChildren<PlaceableObject>());

        foreach (var obj in placeableObjects)
        {
            if (obj.cellsPositions.Count > 0)
                indexs.AddRange(obj.cellsPositions);
        }

        return indexs;
    }

    public Vector2Int GetNearestCellPosition(Vector3 position)
    {
        var point = new Vector2Int();
        var startPosition = transform.position + new Vector3(CellSize.x / 2f, 0f, CellSize.y / 2f);

        for (int i = 0; i < _columsCount; i++)
        {
            var x = startPosition.x + CellSize.x * i;

            if (position.x >= x - CellSize.x / 2 && position.x <= x + CellSize.x / 2)
            {
                point.x = i;

                break;
            }
        }

        for (int j = 0; j < _rowsCount; j++)
        {
            var z = startPosition.z + CellSize.y * j;

            if (position.z >= z - CellSize.y / 2 && position.z <= z + CellSize.y / 2)
            {
                point.y = j;

                break;
            }
        }

        return point;
    }

    public List<Cell> GetNeighbourCells(Cell currentCell)
    {
        List<Cell> neighbourCells = new List<Cell>();

        //check left neighbour
        if (currentCell.Position.x > MinPosition.x)
            neighbourCells.Add(GetCell(new Vector2Int(currentCell.Position.x - (int)CellSize.x, currentCell.Position.y)));

        //check right neighbour
        if (currentCell.Position.x < MaxPosition.x)
            neighbourCells.Add(GetCell(new Vector2Int(currentCell.Position.x + (int)CellSize.x, currentCell.Position.y)));

        //check bot neighbour
        if (currentCell.Position.y > MinPosition.z)
            neighbourCells.Add(GetCell(new Vector2Int(currentCell.Position.x, currentCell.Position.y - (int)CellSize.y)));

        //check top neighbour
        if (currentCell.Position.y < MaxPosition.z)
            neighbourCells.Add(GetCell(new Vector2Int(currentCell.Position.x, currentCell.Position.y + (int)CellSize.y)));

        return neighbourCells;
    }
    public void ShowFreeCellsSprite() => _cells.ForEach(x => x.ShowFreeCellSprite());

    public void HideFreeCellsSprite() => _cells.ForEach(x => x.HideFreeCellSprite());

    public void ShowPlacebleCells() => _cells.ForEach(x => x.ShowFreeCell());

    public void HideFreeCells() => _cells.ForEach(x => x.HideFreeCell());

    public void HideActiveCells() => _cells.ForEach(x => x.HideIndicator());

    public Cell GetCellClosestFreePoint(Cell currentCell)
    {
        var minDistance = float.MaxValue;
        var closestFreeCell = currentCell;
        Cell cell;

        for (int i = 0; i < _cells.Count; i++)
        {
            cell = _cells[i];
            if (cell.Position == currentCell.Position || cell.IsObjectPlaced || !cell.CanPlace)
                continue;
            var distance = Vector3.Distance(GetCellPosition(cell.Position), GetCellPosition(currentCell.Position));
            if (distance < minDistance)
            {
                closestFreeCell = cell;
                minDistance = distance;
            }
        }
        return closestFreeCell;
    }

    public Vector3 GetClosestCellPositionToWorldPoint(Vector3 worldPoint)
    {
        var minDistance = float.MaxValue;
        Vector3 newPos = Vector3.zero;

        for (int i = 0; i < _cells.Count; i++)
        {
            var distance = Vector3.Distance(_cells[i].transform.position, worldPoint);
            if (distance < minDistance)
            {
                newPos = _cells[i].transform.position;
                minDistance = distance;
            }
        }
        return newPos;
    }

    [ContextMenu("UpdateNavMeshGrid()")]

    public void UpdateNavMeshGrid()
    {
        //Debug.Log(">>>>>>>>>>>>>> UpdateNavMeshGrid()");
        _navMeshSufrace.UpdateNavMesh(_navMeshSufrace.navMeshData);
    }
}