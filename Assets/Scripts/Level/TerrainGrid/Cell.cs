using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;
using Units;

[RequireComponent(typeof(PathCell))]
public class Cell : MonoBehaviour
{
    private Action onStatusChanged;
    private Action onDeactivate;

    private const string CellCleanerTag = "CellCleaner";

    [Header("Componnets")]
    [SerializeField] private BoxCollider _collider;

    [SerializeField] private MeshRenderer _renderer;

    [Header("Settings")]
    [SerializeField] private string _cellDetectorTag;

    /*[SerializeField] private Color _enableColor;
    [SerializeField] private Color _disableColor;*/

    [SerializeField] private Material _enableMaterial;
    [SerializeField] private Material _disableMaterial;

    [SerializeField] private MeshRenderer _freeCellRenderer;

    private Collider _lastDetectedCollider;

    private Vector2 _size;

    private PathCell pathCell;
    public Vector2Int Position { get; private set; }

    public bool IsObjectPlaced { get; set; }

    public bool IsObjectRegistered { get; private set; }

    public bool CanPlace { get; private set; }

    public PathCell PathCell { get; private set; }

    public bool CellInit { get; private set; }

    private Unit _ownerOfPlace;

    private UnityEvent _onUpdateHeroPosition = new UnityEvent();

    private Transform _transform;

    public void Init(Action OnStatusChanged, Vector2Int position, Action OnDeactivate, bool isObjectPlaced = true, bool canPlace = true)
    {
        //Debug.Log("Init Cell");
        onStatusChanged = OnStatusChanged;
        onDeactivate += OnDeactivate;

        PathCell = GetComponent<PathCell>();
        PathCell.Init(this, !isObjectPlaced);

        Position = position;
        IsObjectPlaced = isObjectPlaced;
        CanPlace = canPlace;
        IsObjectRegistered = false;
        //Debug.Log("CanPlace "+ CanPlace);

        _size = TerrainGrid.Instance.CellSize;
        _renderer.gameObject.transform.localScale = new Vector3(_size.x, 0.1f, _size.y);
        _freeCellRenderer.gameObject.transform.localScale = new Vector3(_size.x, 0.1f, _size.y);
        //_material = new Material(_material);  
        //_renderer.material = _material;
        _renderer.enabled = false;

        ResetCollidersSize();
        ResetPosition();

        CellInit = true;
        //Debug.Log("Cell Init");
    }

    public void SetPlaceStatus()
    {
        SetPlace();
        onStatusChanged?.Invoke();
    }

    public void SetUnplaceStatus()
    {
        SetUnplace();
        onStatusChanged?.Invoke();
    }

    public void SetPlace()
    {
        IsObjectPlaced = true;
        PathCell.isWalkable = false;
        HideIndicator();
    }

    public void SetUnplace()
    {
        IsObjectPlaced = false;
        PathCell.isWalkable = true;
    }

    public bool IsCanPlaced()
    {
        if (IsObjectPlaced || !CanPlace)
            return false;
        return true;
    }

    public bool CanRegisterObjectInCell()//first registration
    {
        if (CanPlace && !IsObjectPlaced && !IsObjectRegistered)
            return true;
        return false;
    }

    public void SetRegistrationStatusOfObjectIn—ell(Unit unit, bool registered)//during the first registration/initialization unit(registered = true), during returning to the cell(registered = true) , after death(registered = false)
    {
        if (!registered)//event for dead unit
        {
            _ownerOfPlace = null;
            SetUnplaceStatus();
            IsObjectRegistered = registered;
            _onUpdateHeroPosition?.RemoveAllListeners();
            Debug.Log("Remove Registration Unit in cell !!!" + Position.ToString());
            return;
        }

        if (IsObjectRegistered)
        {
            if (_ownerOfPlace != null && _ownerOfPlace == unit && !IsObjectPlaced)
            {
                Debug.Log("Is Registration this Unit !!!" + Position.ToString());
                Debug.Log("START HeroMovement EVENT / The owner of the point has returned, the hero changes position");
                _onUpdateHeroPosition?.Invoke();
            }
        }

        if (!IsObjectRegistered)
        {
            if (_ownerOfPlace == null && _ownerOfPlace != unit)
            {
                _ownerOfPlace = unit;
                IsObjectRegistered = registered;
                Debug.Log("First Registration Unit in cell !!!" + Position.ToString());
            }

        }
        SetPlaceStatus();
        //Debug.Log("Registration Unit in cell !!!" + Position.ToString());
    }

    private void ResetCollidersSize()
    {
        _collider.size = new Vector3(_size.x, _collider.size.y, _size.y);
    }

    private void ResetPosition()
    {
        var x = Position.x * _size.x + _size.x / 2;
        var z = Position.y * _size.y + _size.y / 2;
        var y = transform.position.y;

        transform.position = new Vector3(x, y, z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_cellDetectorTag))
        {
            _lastDetectedCollider = other;

            ShowIndicator();
        }
        else if (other.CompareTag(CellCleanerTag))
            Deactivate();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_cellDetectorTag))
        {
            if (other == _lastDetectedCollider)
            {
                HideIndicator();
            }
        }
    }

    public void ShowDisableIndicator()
    {
        var material = _disableMaterial;

        _renderer.enabled = true;
        _renderer.material = material;
        //_material.color = color;
    }

    public void ShowIndicator()
    {
        var material = !IsObjectPlaced && CanPlace ? _enableMaterial : _disableMaterial;

        _renderer.enabled = true;
        _renderer.material = material;
        //_material.color = color;
    }

    public void HideIndicator()
    {
        _renderer.enabled = false;
    }

    public void ShowFreeCell()
    {
        if (IsObjectPlaced || !CanPlace)
            return;

        _freeCellRenderer.enabled = true;
    }

    public void HideFreeCell() => _freeCellRenderer.enabled = false;

    public void HideFreeCellSprite() => _freeCellRenderer.gameObject.SetActive(false);

    public void ShowFreeCellSprite() => _freeCellRenderer.gameObject.SetActive(true);

    public void Deactivate()
    {
        onDeactivate?.Invoke();
        gameObject.SetActive(false);
    }
    //public void OnMouseDown() => TerrainGrid.Instance.GetNeighbourCells(this).ForEach(cell => cell.ShowIndicator());

    public void SubscribeOnUpdateHeroPosition(UnityAction onUpdateHeroPosition)
    {
        _onUpdateHeroPosition.AddListener(onUpdateHeroPosition);//call when the "owner" of the point returns
    }

    public void RemoveOnUpdateHeroPosition(UnityAction onUpdateHeroPosition)
    {
        _onUpdateHeroPosition?.RemoveListener(onUpdateHeroPosition);
    }
}
