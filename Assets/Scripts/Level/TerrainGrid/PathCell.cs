using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCell : MonoBehaviour
{
    [HideInInspector] public Cell cell;

    [HideInInspector] public int gCost;
    [HideInInspector] public int hCost;
    [HideInInspector] public int fCost;

    [HideInInspector] public PathCell cameFromCell;
    [HideInInspector] public List<PathCell> neighbors;
    [HideInInspector] public bool isWalkable;

    public void Init(Cell cell, bool isWalkable = true)
    {
        this.cell = cell;
        neighbors = new List<PathCell>();
        //TerrainGrid.Instance.GetNeighbourCells(cell).ForEach(x => neighbors.Add(x.pathCell));
        this.isWalkable = isWalkable;
    }

    public void SetNeighbours()
    {
        
        TerrainGrid.Instance.GetNeighbourCells(cell).ForEach(x => neighbors.Add(x.PathCell));
    }

    public void CalculateFCost() => fCost = gCost + hCost;
}
