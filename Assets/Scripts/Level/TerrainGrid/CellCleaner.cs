using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellCleaner : MonoBehaviour
{
    [SerializeField]
    private string _cellTag;
    [SerializeField]
    private LayerMask _cellLayer;

    private List<Cell> _cells = new List<Cell>();

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(_cellTag))
        {
            //other.gameObject.SetActive(false);
            _cells.Add(other.GetComponent<Cell>());
        }    
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(_cellTag))
        {
            _cells.Add(other.GetComponent<Cell>());
        }
    }

    public void DeactivateCells()
    {
        /*Collider[] hitColliders = Physics.OverlapBox(gameObject.transform.position, transform.localScale, Quaternion.identity, _cellLayer);
        foreach (Collider collider in hitColliders)
        {
            print(collider);
            if (collider.CompareTag(_cellTag))
                collider.gameObject.SetActive(false);
        }*/

        foreach (Cell cell in _cells)
            cell.Deactivate();

        gameObject.SetActive(false);
    }
}
