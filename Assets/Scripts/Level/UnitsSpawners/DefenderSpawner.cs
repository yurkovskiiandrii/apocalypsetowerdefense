using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using Units;

public class DefenderSpawner : MonoBehaviour//?
{
    private const string DETECTOR_CONTAINER_NAME = "Detectors Container";

    private Action<Unit> onUnitGet;
    private Action<Unit> onUnitRelease;

    [Header("Components")]
    [SerializeField] protected GameObject detectorPrafab;

    [Header("Settings")]
    [SerializeField] protected LayerMask cellLayer;
    [SerializeField] protected LayerMask detectorLayer;

    [Header("Visualise")]
    [SerializeField] private bool _showCells;

    [SerializeField] private Vector2 _cellSize;

    [SerializeField] private GizmosDrawer.LineWidth _lineWidth;

    protected LevelEvents levelEvents;

    protected TestUI testUI;

    protected Coroutine touchHandler;

    protected Transform detectorsContainer;

    protected CellDetector detector;

    [Header("Spawner")]
    [SerializeField]
    private RangedDefender _rangedDefenderPrefab;

    [SerializeField]
    private Transform _defendersPlaceholder;

    [SerializeField]
    private Transform _projectilesPlaceholder;

    protected ObjectPool<Unit> _defendersPool;
    private Unit _newUnit;

    public void Init(Action<Unit> OnUnitGet, Action<Unit> OnUnitRelease)
    {
        onUnitGet += OnUnitGet;
        onUnitRelease += OnUnitRelease;

        levelEvents = LevelEvents.Instance;
        //testUI = TestUI.Instance;
        _defendersPool = new ObjectPool<Unit>(Create, Get, Release);
    }

    public void SpawnRangedDefender()
    {
        SpawnDefender();
    }

    private void SpawnDefender(/*enum type in future*/)
    {
        var camera = Camera.main;
        var spawnPosition = new Vector3();

        if (Physics.Raycast(camera.ScreenPointToRay(new Vector3(camera.pixelWidth / 2, camera.pixelHeight / 2, 0)), out RaycastHit hit, 100f, cellLayer))
        {
            var cell = hit.transform.GetComponent<Cell>();

            spawnPosition = cell.transform.position;
            spawnPosition.y = 0f;
        }

        _newUnit = _defendersPool.Get();
        _newUnit.transform.position = spawnPosition;
        _newUnit.transform.rotation = transform.rotation;
        StartPlacing();
    }

    public void StartPlacing()
    {
        print("Start placing unit /DefenderSpawner");

        levelEvents.OnPlaceStart();
        testUI.OnStartPlacing(() =>
        {
            if (CanPlace())
                EndPlacing();
        }, CancelPlacing);

        SpawnDetector(_newUnit.transform);

        touchHandler = StartCoroutine(TouchHandler(_newUnit.transform));
    }

    protected void EndPlacing()
    {
        print("End placing /DefenderSpawner");

        levelEvents.OnPlaceEnd();
        testUI.OnEndPlacing();

        StopCoroutine(touchHandler);

        var cell = detector.CurrCell;

        if (cell)
        {
            cell.SetPlaceStatus();
            detector.Deactivate();
            RangedDefender unit = (RangedDefender)_newUnit;
            unit.Init(delegate { _defendersPool.Release(unit); cell.SetUnplaceStatus(); }, delegate { print("old method spawned reach the goal"); });
            _newUnit = null;
        }
        
    }

    protected void CancelPlacing()
    {
        print("Cancel placing /DefenderSpawner");

        levelEvents.OnPlaceCancel();
        testUI.OnEndPlacing();

        StopCoroutine(touchHandler);

        detector.Deactivate();
        detector.CurrCell?.HideIndicator();

        Unit unit = _newUnit;
        _defendersPool.Release(unit);
        _newUnit = null;
    }

    protected bool CanPlace()
    {
        var cell = detector.CurrCell;

        if (cell && cell.IsObjectPlaced || cell && !cell.CanPlace)
            return false;
        return true;
    }

    protected void SpawnDetector(Transform defender)
    {
        detector = null;

        foreach (Transform child in defender)
            if(child.name == DETECTOR_CONTAINER_NAME)
            {
                detectorsContainer = child;
                if (detectorsContainer.childCount != 0)
                {
                    detector = detectorsContainer.GetChild(0).GetComponent<CellDetector>();
                    detector.Activate();
                }
                else
                    SpawnDetector();
                return;
            }

        detectorsContainer = new GameObject(DETECTOR_CONTAINER_NAME).transform;
        detectorsContainer.SetParent(defender);
        detectorsContainer.position = defender.position;

        SpawnDetector();
    }

    private void SpawnDetector()
    {
        detector = Instantiate(detectorPrafab, detectorsContainer).GetComponent<CellDetector>();
        var position = Vector3.zero;

        detector.transform.localPosition = position;
        detector.Init();
        detector.Activate();
    }

    protected IEnumerator TouchHandler(Transform defender)
    {
        var grid = TerrainGrid.Instance;
        var levelCamera = LevelCamera.Instance;
        var camera = Camera.main;
        var cellSize = grid.CellSize;

        while (true)
        {
            yield return null;

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100f, detectorLayer))
                {
                    if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, cellLayer))
                    {
                        levelCamera.DisableMove();
                        levelCamera.DisableZoom();

                        var detectedCell = hit.transform.GetComponent<Cell>();
                        var currentGridPosition = detectedCell.Position;

                        while (Input.GetMouseButton(0))
                        {
                            yield return null;

                            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, cellLayer))
                            {
                                var gridPosition = grid.GetGridPosition(hit.transform.position);
                                detectedCell = hit.transform.GetComponent<Cell>();

                                if (detectedCell.CanPlace && gridPosition != currentGridPosition)
                                {
                                    var direction = gridPosition - currentGridPosition;
                                    var position = defender.position;

                                    position += new Vector3(cellSize.x * direction.x, 0, cellSize.y * direction.y);
                                    defender.position = position;

                                    currentGridPosition = gridPosition;
                                }
                            }
                        }

                        levelCamera.EnableMove();
                        levelCamera.EnableZoom();
                    }
                }
            }
        }
    }

    private Unit Create()
    {
        Unit newUnit = Instantiate(_rangedDefenderPrefab, _defendersPlaceholder);
        newUnit.gameObject.SetActive(false);

        return newUnit;
    }

    private void Get(Unit unit)
    {
        unit.gameObject.SetActive(true);
        onUnitGet?.Invoke(unit);
    }

    private void Release(Unit unit)
    {
        unit.transform.position = Vector3.zero;
        onUnitRelease?.Invoke(unit);
        unit.gameObject.SetActive(false);
    }
}
