using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using Units;

public enum InvaderType {Ranged, Melee } // temp

public class InvaderSpawner : MonoBehaviour//?
{
    private Action<Unit> onUnitGet;
    private Action<Unit> onUnitRelease;

    [SerializeField]
    private int _spawnCount = 10;
    [SerializeField]
    private float _spawmSpeed = 1f;

    [SerializeField]
    private RangedInvader _rangedInvaderPrefab;
    [SerializeField]
    private MeleeInvader _meleeInvaderPrefab;

    [SerializeField]
    private Transform _invadersPlaceholder;
    [SerializeField]
    private Transform _invadersSpawnpoint;

    protected ObjectPool<Unit> _rangedInvadersPool;
    protected ObjectPool<Unit> _meleeInvadersPool;
    private int _rangersCount = 0;
    private int _meleeCount = 0;


    public void Init(Action<Unit> OnUnitGet, Action<Unit> OnUnitRelease)
    {
        onUnitGet += OnUnitGet;
        onUnitRelease += OnUnitRelease;

        _rangedInvadersPool = new ObjectPool<Unit>(CreateRanger, GetRanger, Release);
        _meleeInvadersPool = new ObjectPool<Unit>(CreateMelee, GetMelee, Release);
    }

    #region TempSpawner
    public void StartSpawn(string typeName) 
    {
        InvaderType type = (InvaderType)Enum.Parse(typeof(InvaderType), typeName);

        if (type == InvaderType.Ranged)
            StartCoroutine(SpawnRangedCoroutine()); 
        else if (type == InvaderType.Melee)
            StartCoroutine(SpawnMeleeCoroutine());
    }

    private IEnumerator SpawnRangedCoroutine()
    {
        for (int i = 0; i < _spawnCount; i++)
        {
            SpawnRangedInvader();

            yield return new WaitForSeconds(_spawmSpeed);
        }
    }

    private IEnumerator SpawnMeleeCoroutine()
    {
        for (int i = 0; i < _spawnCount; i++)
        {
            SpawnMeleeInvader();

            yield return new WaitForSeconds(_spawmSpeed);
        }
    }
    #endregion

    public void StartSpawn() => StartCoroutine(SpawnCoroutine());

    private IEnumerator SpawnCoroutine()
    {
        for (int i = 0; i < _spawnCount; i++)
        {
            if (i % 2 == 0)
                SpawnRangedInvader();
            else
                SpawnMeleeInvader();

            yield return new WaitForSeconds(_spawmSpeed);
        }
    }

    private void SpawnRangedInvader()
    { 
        RangedInvader newRangedInvader = (RangedInvader)_rangedInvadersPool.Get();
        newRangedInvader.transform.position = _invadersSpawnpoint.transform.position;
        newRangedInvader.Init(delegate { _rangedInvadersPool.Release(newRangedInvader); }, delegate { print("old method spawned reach the goal"); });
    }

    private void SpawnMeleeInvader()
    {
        MeleeInvader newMeleeInvader = (MeleeInvader)_meleeInvadersPool.Get();
        newMeleeInvader.transform.position = _invadersSpawnpoint.transform.position;
        newMeleeInvader.Init(delegate { _meleeInvadersPool.Release(newMeleeInvader); }, delegate { print("old method spawned reach the goal"); }) ;
    }

    private Unit CreateRanger()
    {
        Unit newUnit = Instantiate(_rangedInvaderPrefab, _invadersPlaceholder);
        
        newUnit.gameObject.SetActive(false);
        

        return newUnit;
    }

    private void GetRanger(Unit unit)
    {
        unit.gameObject.SetActive(true);
        unit.name = _rangedInvaderPrefab.name + "(" + _rangersCount + ")";
        _rangersCount++;
        onUnitGet?.Invoke(unit);
    }

    private Unit CreateMelee()
    {
        Unit newUnit = Instantiate(_meleeInvaderPrefab, _invadersPlaceholder);

        newUnit.gameObject.SetActive(false);


        return newUnit;
    }

    private void GetMelee(Unit unit)
    {
        unit.gameObject.SetActive(true);
        unit.name = _meleeInvaderPrefab.name + "(" + _meleeCount + ")";
        _meleeCount++;
        onUnitGet?.Invoke(unit);
    }

    private void Release(Unit unit)
    {
        unit.transform.DOKill();
        unit.transform.position = Vector3.zero;
        onUnitRelease?.Invoke(unit);
        unit.gameObject.SetActive(false);
    }
}
