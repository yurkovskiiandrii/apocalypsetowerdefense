using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner;
using BehaviorDesigner.Runtime;
using System;
using UnityEngine.Events;
using Units;

public class LevelController : SingletonComponent<LevelController>
{
    [Header("Main Components")]
    [SerializeField] private TerrainGrid _terrainGrid;
    [SerializeField] private LevelCamera _levelCamera;

    /*[Header("Spawners")]
    [SerializeField]
    private DefenderSpawner _defenderSpawner;
    [SerializeField]
    private InvaderSpawner _invaderSpawner;*/

    private const string DEFENDER_TAG = "Defender";
    private const string INVADER_TAG = "Invader";

    //temp solution
    private List<Unit> _defenders = new List<Unit>();
    public List<Unit> Defenders { get { return _defenders; } }

    private List<Unit> _invaders = new List<Unit>();
    public List<Unit> Invaders { get { return _invaders; } }

    //event
    private UnityEvent<Unit> _onInvaderAdd = new UnityEvent<Unit>();
    private UnityEvent<Unit> _onInvaderRemove = new UnityEvent<Unit>();
    private UnityEvent<Unit> _onDefenderAdd = new UnityEvent<Unit>();
    private UnityEvent<Unit> _onDefenderRemove = new UnityEvent<Unit>();

    private void Awake()
    {
        //Debug.Log("Awake LevelController");
        // _defenderSpawner.Init(AddDefender, RemoveDefender);
        // _invaderSpawner.Init(AddInvader, RemoveInvader);
    }

    public void Init()//Start()
    {    
        _terrainGrid.Init();
        _levelCamera.Init();
        _levelCamera.Activate();
    }

    public List<Transform> GetUnitsTransformsByTag(string tag)
    {
        List<Unit> units = GetUnitsByTag(tag);
        if (units != null)
        {
            List<Transform> transforms = new List<Transform>();
            foreach (Unit u in units)
                transforms.Add(u.transform);
            return transforms;
        }

        return null;
    }

    public int GetUnitsCountByTag(string tag)
    {
        if (tag == DEFENDER_TAG)
            return _defenders.Count;
        else if (tag == INVADER_TAG)
            return _invaders.Count;
        return -1;
    }

    public List<Unit> GetUnitsByTag(string tag)
    {
        if (tag == DEFENDER_TAG)
            return _defenders;
        else if (tag == INVADER_TAG)
            return _invaders;
        return null;
    }

    public void AddDefender(Unit unit)
    {
        _defenders.Add(unit);
        _onDefenderAdd?.Invoke(unit);
    }

    public void RemoveDefender(Unit unit)
    {
        _defenders.Remove(unit);
       _onDefenderRemove?.Invoke(unit);
    }
    
    public void AddInvader(Unit unit)
    {
        _invaders.Add(unit);
        _onInvaderAdd?.Invoke(unit);
    }

    public void RemoveInvader(Unit unit)
    {
        _invaders.Remove(unit);
        _onInvaderRemove?.Invoke(unit);
    }

    public GameObject GetUnit(int number)
    {
        if (number < _defenders.Count)
            return _defenders[number].gameObject;
        return null;
    }

    public void SubscribeOnUnitAdd(string tag, UnityAction<Unit> onUnitAdd)
    {
        if (tag == DEFENDER_TAG)
            _onDefenderAdd.AddListener(onUnitAdd);
        if (tag == INVADER_TAG)
            _onInvaderAdd.AddListener(onUnitAdd);
    }

    public void SubscribeOnUnitRemove(string tag, UnityAction<Unit> onUnitRemove)
    {
        if (tag == DEFENDER_TAG)
            _onDefenderRemove.AddListener(onUnitRemove);
        if (tag == INVADER_TAG)
            _onInvaderRemove.AddListener(onUnitRemove);
    }
}
