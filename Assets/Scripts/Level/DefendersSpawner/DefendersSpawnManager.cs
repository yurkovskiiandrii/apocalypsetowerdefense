using System;
using System.Collections;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;
using Units;

public class DefendersSpawnManager : MonoBehaviour, IPlaceableObject
{
    private const string DETECTOR_CONTAINER_NAME = "Detectors Container";

    private UnityAction onSpawnEnd;
    private UnityAction<int> onDefenderSpawn;
    private Action onUnitReachDestination;

    [Header("Components")]
    [SerializeField] protected GameObject detectorPrafab;

    [Header("Settings")]
    [SerializeField] protected LayerMask cellLayer;
    [SerializeField] protected LayerMask detectorLayer;

    [Header("Visualise")]
    [SerializeField] private bool _showCells;

    [SerializeField] private GizmosDrawer.LineWidth _lineWidth;

    protected LevelEvents _levelEvents;

    protected UnitPlacingUI placingUI;

    protected Coroutine touchHandler;

    protected Transform detectorsContainer;

    protected CellDetector detector;

    [SerializeField]
    private Transform _defendersPlaceholder;
    private TerrainGrid _terrainGrid;
    private Unit _newUnit;
    private bool _isHeroDefender;
    private Vector3 _heroNewPosition = Vector3.zero;
    private bool _isActiveUnitOrProps;
    private Vector3 _startPosUnit;
    private Cell _startCellUnit;

    public void Init(UnityAction OnSpawnEnd)
    {
        onSpawnEnd += OnSpawnEnd;
        _levelEvents = LevelEvents.Instance;
        _terrainGrid = TerrainGrid.Instance;
        _levelEvents.SubscribeOnEndWaitingTimeForWave(() => { if (_isHeroDefender) return; if (_newUnit != null && !_newUnit.IsInit) CancelPlacing(); else EndPlacing(); });
    }

    public void SubscribeSpawn(UnityAction<int> OnDefenderSpawn, Action OnUnitReachDestination)
    {
        onDefenderSpawn += OnDefenderSpawn;
        onUnitReachDestination += OnUnitReachDestination;
    }

    public void SpawnDefender(Unit prefab/*enum type in future*/)
    {
        var camera = Camera.main;
        var spawnPosition = new Vector3();

        //if (Physics.Raycast(camera.ScreenPointToRay(new Vector3(camera.pixelWidth / 2, camera.pixelHeight / 2, 0)), out RaycastHit hit, 100f))//, cellLayer))//for camera - perspective
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out RaycastHit hit, 100.0f, cellLayer))//for camera - orthographic
        {
            var cell = hit.transform.GetComponent<Cell>();
            if (cell == null)
            {
                cell = _terrainGrid.GetSellFromAllCellsPositions(hit.transform.position);
            }
            spawnPosition = cell.transform.position;
        }
        else
        {
            //Debug.Log("!!!!!Not Raycast");
            Vector3 screenCenter = new Vector3(camera.pixelWidth / 2, camera.pixelHeight / 2, 0);
            Vector3 worldCenter = new Vector3(camera.ScreenToWorldPoint(screenCenter).x, 0, 0);
            spawnPosition = _terrainGrid.GetClosestCellPositionToWorldPoint(worldCenter);
        }
        spawnPosition.y = 0f;
        _isActiveUnitOrProps = true;
        _newUnit = Instantiate(prefab, _defendersPlaceholder);
        _newUnit.transform.position = spawnPosition;
        _newUnit.transform.rotation = transform.rotation;
        placingUI = _newUnit.PlacingUI;
        _newUnit.SetPlacementMode();

        StartPlacing();
    }

    public void SetActiveUnitOrProps(bool isActive) => _isActiveUnitOrProps = isActive;

    public bool IsActiveUnitOrProps() => _isActiveUnitOrProps;

    public void ActivateHeroUnit(Unit unit)
    {
        _isActiveUnitOrProps = true;
        _heroNewPosition = Vector3.zero;
        _isHeroDefender = true;
        _newUnit = unit;
        //_newUnit.SetPlacementMode();

        StartPlacing();
    }

    public void ActivateDefenderUnit(Unit unit, UnitPlacingUI unitPlacingUI)
    {
        if (_isActiveUnitOrProps && unit != null)
            return;
        if (!WavesManager.IsStartedWaitingTimeForWave)
        {
            StartPlacingUI(unitPlacingUI);
            return;
        }

        _isActiveUnitOrProps = true;
        _isHeroDefender = false;
        _newUnit = unit;
        _newUnit.SetPlacementMode();
        placingUI = unitPlacingUI;

        var currentCell = _newUnit.CurrentCell;
        _startPosUnit = currentCell.transform.position;
        //currentCell.ShowDisableIndicator();//test
        currentCell.SetUnplaceStatus();
        _startCellUnit = currentCell;

        StartPlacing();
    }

    private void StartPlacingUI(UnitPlacingUI placingUI)
    {
        placingUI.OnStartPlacing(() =>
        {
            if (CanPlace())
                EndPlacing();
        }, CancelPlacing);
    }

    private void StopCoroutineTouchHandler()
    {
        if (touchHandler != null)
        {
            StopCoroutine(touchHandler);
            touchHandler = null;
        }
    }

    public void StartPlacing()
    {
        print("Start placing unit ");

        StopCoroutineTouchHandler();

        _terrainGrid.IsRedacting = true;

        _levelEvents.OnPlaceStart();

        if (_isHeroDefender)
        {
            print("Start placing unit/isHeroDefender ");
            touchHandler = StartCoroutine(TouchHandlerHeroNewPosition(_newUnit.transform));
            return;
        }

        if (placingUI != null)
        {
            placingUI.gameObject.SetActive(true);
            StartPlacingUI(placingUI);
        }
        SpawnDetector(_newUnit.transform);
        _terrainGrid.ShowPlacebleCells();
        touchHandler = StartCoroutine(TouchHandler(_newUnit.transform));
    }

    public void EndPlacing()
    {
        print("End placing");
        if (!_isActiveUnitOrProps || _newUnit == null)
            return;

        _levelEvents.OnPlaceEnd();

        if (placingUI != null)
            placingUI.gameObject.SetActive(false);

        StopCoroutineTouchHandler();

        if (_isHeroDefender)
        {
            HeroesManager.Instance.SetNewPositionHero(_heroNewPosition);
        }

        _terrainGrid.HideFreeCells();
        _terrainGrid.HideActiveCells();

        if (detector != null)
        {
            var cell = detector.CurrCell;
            if (cell)
            {
                if (!_newUnit)
                    return;
                if (!_isHeroDefender)
                {
                    if (!_newUnit.IsInit)
                    {
                        var unit = _newUnit;
                        //unit.Transform.position = posInCell;
                        //unit.Init(delegate { Release(unit); cell.SetUnplaceStatus(); }, onUnitReachDestination);
                        unit.Init(delegate { LevelController.Instance.RemoveDefender(unit); /*_newUnit.CurrentCell.SetUnplaceStatus*/ }, onUnitReachDestination);
                        onDefenderSpawn?.Invoke(unit.DefenderOptions.cost);
                        LevelController.Instance.AddDefender(unit);

                        //if (cell.CanRegisterObjectInCell())//test register object in cell
                        //    cell.SetRegistrationStatusOfObjectIn—ell(unit, true);
                        //else
                    }
                    else
                    {
                        Debug.Log("Defender => New position");
                    }

                    if (_newUnit.IsInit && !WavesManager.IsStartedWaitingTimeForWave)
                    {
                        Debug.Log("Defender => Returned to the starting position");
                        _newUnit.transform.position = _startPosUnit;
                        cell = _startCellUnit;
                    }
                    _newUnit.SetMainMaterial();
                }
                cell.SetPlaceStatus();
                _newUnit.CurrentCell = cell;
            }
            detector.Deactivate();
        }

        onSpawnEnd?.Invoke();
        _newUnit = null;
        placingUI = null;
        _isHeroDefender = false;
        _isActiveUnitOrProps = false;
        _terrainGrid.IsRedacting = false;
    }

    public void CancelPlacing()
    {
        if (!_isActiveUnitOrProps || _newUnit == null)
            return;
        print("Cancel placing");

        _levelEvents.OnPlaceCancel();

        if (placingUI != null)
            placingUI.gameObject.SetActive(false);

        StopCoroutineTouchHandler();

        if (detector != null)
        {
            detector.CurrCell?.HideIndicator();
            detector.Deactivate();
        }

        _terrainGrid.HideFreeCells();
        _terrainGrid.HideActiveCells();

        if (_newUnit)
        {
            if (!_isHeroDefender)
            {
                Unit unit = _newUnit;
                Release(unit);
                onSpawnEnd?.Invoke();
                _newUnit.SetMainMaterial();
            }
        }

        _newUnit = null;
        placingUI = null;
        _isHeroDefender = false;
        _isActiveUnitOrProps = false;
        _terrainGrid.IsRedacting = false;
    }

    public bool CanPlace()
    {
        var cell = detector.CurrCell;

        if (cell && cell.IsObjectPlaced || cell && !cell.CanPlace)
            return false;
        return true;
    }

    protected void SpawnDetector(Transform defender)
    {
        detector = null;

        foreach (Transform child in defender)
            if (child.name == DETECTOR_CONTAINER_NAME)
            {
                detectorsContainer = child;
                if (detectorsContainer.childCount != 0)
                {
                    detector = detectorsContainer.GetChild(0).GetComponent<CellDetector>();
                    detector.Activate();
                }
                else
                    SpawnDetector();
                return;
            }

        detectorsContainer = new GameObject(DETECTOR_CONTAINER_NAME).transform;
        detectorsContainer.SetParent(defender);
        detectorsContainer.position = defender.position;

        SpawnDetector();
    }

    private void SpawnDetector()
    {
        detector = Instantiate(detectorPrafab, detectorsContainer).GetComponent<CellDetector>();
        var position = Vector3.zero;

        detector.transform.localPosition = position;
        detector.Init();
        detector.Activate();
    }

    public IEnumerator TouchHandler(Transform defender)
    {
        Debug.Log("Start Coroutine TouchHandler / Defender");

        var levelCamera = LevelCamera.Instance;
        var camera = Camera.main;
        var cellSize = _terrainGrid.CellSize;

        while (true)
        {
            yield return null;

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), camera.transform.forward, out RaycastHit hit, 100f, detectorLayer))//for camera - orthographic
                //if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100f, detectorLayer))//for camera - perspective
                {
                    if (Physics.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), camera.transform.forward, out hit, 100f, cellLayer))//for camera - orthographic
                    //if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, cellLayer))//for camera - perspective
                    {
                        levelCamera.DisableMove();
                        levelCamera.DisableZoom();

                        var detectedCell = hit.transform.GetComponent<Cell>();
                        var currentGridPosition = detectedCell.Position;

                        while (Input.GetMouseButton(0))
                        {
                            yield return null;

                            if (Physics.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), camera.transform.forward, out hit, 100f, cellLayer))//for camera - orthographic
                            //if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, cellLayer))//for camera - perspective
                            {
                                yield return null;

                                var gridPosition = _terrainGrid.GetGridPosition(hit.transform.position);
                                detectedCell = hit.transform.GetComponent<Cell>();

                                if (detectedCell.CanPlace && gridPosition != currentGridPosition)
                                {
                                    var direction = gridPosition - currentGridPosition;
                                    var position = defender.position;

                                    position += new Vector3(cellSize.x * direction.x, 0, cellSize.y * direction.y);
                                    defender.position = position;

                                    currentGridPosition = gridPosition;
                                }
                            }
                        }

                        levelCamera.EnableMove();
                        levelCamera.EnableZoom();
                    }
                }
            }
        }
    }
    protected IEnumerator TouchHandlerHeroNewPosition(Transform hero)
    {
        Debug.Log("Start Coroutine TouchHandler / HeroNewPosition ");

        var levelCamera = LevelCamera.Instance;
        var camera = Camera.main;
        Plane plane = new Plane(Vector3.up, hero.position);

        while (true)
        {
            yield return null;

            if (Input.GetMouseButtonDown(0))
            {
                if (MouseDetectorAboveUI.IsMouseOverUI(Input.mousePosition))
                    continue;

                if (Physics.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), camera.transform.forward, out RaycastHit hit, 100f))
                {
                    if (hit.transform.gameObject == hero.gameObject)
                        continue;
                }

                Ray cursorRay = camera.ScreenPointToRay(Input.mousePosition);
                float rayDist;
                Vector3 detectedPoint = Vector3.zero;

                if (plane.Raycast(cursorRay, out rayDist))
                    detectedPoint = cursorRay.GetPoint(rayDist);

                levelCamera.DisableMove();
                levelCamera.DisableZoom();

                var gridPosition = _terrainGrid.GetNearestCellPosition(detectedPoint);
                var positionCell = _terrainGrid.GetCellPosition(gridPosition);

                _heroNewPosition = positionCell;

                levelCamera.EnableMove();
                levelCamera.EnableZoom();

                EndPlacing();
            }
        }
    }

    private void Release(Unit unit)
    {
        //unit.transform.position = Vector3.zero;
        LevelController.Instance.RemoveDefender(unit);
        //unit.gameObject.SetActive(false);
        Destroy(unit.gameObject);
    }
}

[Serializable]
public struct DefenderOptions
{
    public int cost;
    public Sprite iconSprite;
}
