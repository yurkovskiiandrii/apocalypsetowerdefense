using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Units;

public class MainTarget : MonoBehaviour
{
    private const string InvaderTag = "Invader";

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("MainTarget ---> OnTriggerEnter = " + other.gameObject.name);
        if (other.CompareTag(InvaderTag))
        {
            Unit invader = other.GetComponent<Unit>();
            invader.OnReachDestination();
            Debug.Log("MainTarget ---> OnTriggerEnter = " + other.gameObject.name);
        }
    }
}
