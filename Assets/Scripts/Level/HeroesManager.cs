using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using Units;

public class HeroesManager : SingletonComponent<HeroesManager>
{
    [SerializeField] private GameObject _heroUILabelPref;
    [SerializeField] private Transform _heroLabelParent;
    [SerializeField] private DefendersSpawnManager _spawnManager;
    [SerializeField] private LevelUI _levelUI;
    private HeroUILabelPref _activeHeroIcon;
    private TerrainGrid _terrainGrid;
    private Transform _heroContainer;
    private List<HeroController> _hero = new List<HeroController>();
    private LevelEvents _levelEvents;
    private Action _onHeroReachDestination;
    private Action _onStartActivateHero;

    public int IndexActiveHero { get; set; }//active movement or skill
    public bool IsActiveSkill { get; set; }

    public void Init()
    {
        Debug.Log("Init HeroManager ");
        _heroContainer = GetComponent<Transform>();
        _terrainGrid = TerrainGrid.Instance;
        _levelEvents = LevelEvents.Instance;

        if (_heroContainer.childCount > 0)
        {
            var index = 0;
            foreach (Transform child in _heroContainer)
            {
                if (child.gameObject.activeSelf)
                {
                    var hero = child.gameObject.GetComponent<HeroController>();
                    hero.Index = index;
                    var posInCell = GetPositionHeroInCell(hero.transform.position);
                    var cell = _terrainGrid.GetCell(posInCell);
                    cell.SetPlaceStatus();
                    hero.StartHero(posInCell);
                    hero.SubscribeOnHeroDead(() => { if (IsActiveHero(hero.Index)) DeactiveCurrentHeroInScene(); });
                    AddHero(hero);
                    index++;
                }
            }
            _levelEvents.SubscribeOnPlaceEnd(DeactiveIconCurrentHero);
            _levelEvents.SubscribeOnPlaceCancel(DeactiveIconCurrentHero);
        }
    }

    public Vector3 GetPositionHeroInCell(Vector3 position)
    {
        //Debug.Log(" 1 GetPositionHeroInCell "+position.ToString());
        Vector3 positionHero = position;
        var gridPosition = _terrainGrid.GetNearestCellPosition(position);
        var positionCell = _terrainGrid.GetCellPosition(gridPosition);
        positionHero = positionCell;
        return positionHero;
    }

    public HeroUILabelPref CreateHeroUILabel()
    {
        var heroUILabel = Instantiate(_heroUILabelPref, _heroLabelParent).GetComponent<HeroUILabelPref>();
        heroUILabel.SetActionOnClickIconOfHero(ActivateHeroUnitInScene);
        heroUILabel.SetActionOnHeroRecovered(() => { ShowHeroDeadPanel(false); });
        return heroUILabel;
    }

    public Action OnHeroReachDestination() => _onHeroReachDestination;

    public void AddHero(HeroController unit) => _hero.Add(unit);

    public void RemoveHero(HeroController unit) => _hero.Remove(unit);

    public List<HeroController> GetListHero() => _hero;

    public Unit GetHeroUnit(int number) => _hero[number].BaseRangedUnit;

    public void SetNewPositionHero(Vector3 newPosition)
    {
        _hero[IndexActiveHero].SetNewPositionInCell(newPosition);
    }

    private void ActivateHeroUnitInScene(HeroUILabelPref unitIcon)
    {
        if (IsActiveHero(unitIcon.HeroNumber))
        {
            unitIcon.DeActivateHero();
            DeactiveCurrentHeroInScene();
            return;
        }

        Unit unit = GetHeroUnit(unitIcon.HeroNumber);

        if (!unit.IsInit)
        {
            ShowHeroDeadPanel(true);
            return;
        }

        if (_spawnManager.IsActiveUnitOrProps())
        {
            _spawnManager.CancelPlacing();
            _onStartActivateHero?.Invoke();
        }

        _spawnManager.ActivateHeroUnit(unit);

        unitIcon.ActivateHeroClickImage();

        _activeHeroIcon = unitIcon;
        IndexActiveHero = unitIcon.HeroNumber;
    }

    public void ShowHeroDeadPanel(bool show)
    {
        _levelUI.ShowDefenderDeadPanel(show);
    }

    public void DeactiveIconCurrentHero()
    {
        _activeHeroIcon = null;
    }

    public void DeactiveCurrentHeroInScene()
    {
        if (IsActiveUnitOrProps())
        {
            DeactiveIconCurrentHero();
            _spawnManager.CancelPlacing();
        }
    }

    public bool IsActiveHero(int heroIndex) => (_activeHeroIcon != null && IndexActiveHero == heroIndex);

    public bool IsActiveUnitOrProps() => _spawnManager.IsActiveUnitOrProps();

    public void StopActiveSkill()
    {
        _hero[IndexActiveHero].HeroUILabel.SkillStop();
    }

    public void DefenderChangePosition(Unit unit, UnitPlacingUI unitPlacingUI)
    {
        _spawnManager.ActivateDefenderUnit(unit, unitPlacingUI);
    }

    public void EndPlacingDefenderMenu() => _spawnManager.EndPlacing();

    public void SubscribeStartHero(Action OnUnitReachDestination)
    {
        _onHeroReachDestination += OnUnitReachDestination;
    }

    public void SubscribeStartActivateHero(Action callBack) => _onStartActivateHero += callBack;
}
