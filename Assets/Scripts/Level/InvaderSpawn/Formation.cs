using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Units;
using UnityEditor.Timeline.Actions;

public class Formation : MonoBehaviour
{
    private const string INVADER_TAG = "Invader";

    private UnityAction onFormationEnds;
    private UnityAction<int, bool> onInvaderDied;
    private UnityAction onInvaderSpawned;

    private Action onUnitReachDestination;

    private List<Unit> _invaders = new List<Unit>();

    public List<Unit> Invaders { get => _invaders; }

    public int InvadersCount
    {
        get
        {
            if (_invaders.Count != 0)
                return _invaders.Count;

            return transform.childCount;
        }
    }

    //private bool _isSpawned = false;
    //private bool _isInvadersAlive = true;

    private Transform _transform;

    private void Awake()
    {
        _invaders = GetInvadersList();

        foreach (var unit in _invaders)
        {
            unit.gameObject.SetActive(false);
        }
    }

    public List<Unit> GetInvadersList()
    {
        _transform = transform;
        List<Unit> invaders = new List<Unit>();
        foreach (Transform child in _transform)
            if (child.tag == INVADER_TAG)
            {
                Unit unit = child.GetComponent<Unit>();
                invaders.Add(unit);
            }
        return invaders;
    }

    public void Spawn(UnityAction OnFormationEnds, UnityAction OnInvaderSpawned, UnityAction<int, bool> OnInvaderDied, Action OnUnitReachDestination)
    {
        //Invoke(nameof(ActivateUnits), spawnTimeDelay);

        onFormationEnds += OnFormationEnds;
        onInvaderSpawned += OnInvaderSpawned;
        onInvaderDied += OnInvaderDied;
        onUnitReachDestination += OnUnitReachDestination;

        ActivateUnits();
    }

    public void ActivateUnits()
    {
        foreach (Unit invader in _invaders)
        {
            invader.gameObject.SetActive(true);
            invader.Init(delegate (bool isKilled) { CheckInvadersAlive(invader); onInvaderDied?.Invoke(invader.DefenderOptions.cost, isKilled); }, onUnitReachDestination);
            LevelController.Instance.AddInvader(invader);
            onInvaderSpawned?.Invoke();
        }
    }
    /*private void UnitDead(bool , Unit invader)
    {
        CheckInvadersAlive(invader);
        onInvaderDied?.Invoke(invader.DefenderOptions.cost);
    }*/

    private void CheckInvadersAlive(Unit invader)
    {
        invader.gameObject.SetActive(false);
        LevelController.Instance.RemoveInvader(invader);

        if (!_invaders.Exists(x => x.gameObject.activeSelf == true))
        {
            print("formation end");
            onFormationEnds?.Invoke();
            onFormationEnds = null;
            Destroy(gameObject);
        }
    }
}
