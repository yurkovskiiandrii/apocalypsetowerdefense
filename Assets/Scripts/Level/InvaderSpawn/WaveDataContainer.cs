using UnityEngine;

[System.Serializable]
public class WaveDataContainer
{
    [SerializeField] private int _wave;
    [SerializeField] private float _spawnTimeDelay;
    [SerializeField] private bool _hasDisplayTime;

    public int Wave => _wave;

    public float SpawnTimeDelay => _spawnTimeDelay;

    public bool HasDisplayTime => _hasDisplayTime;
}
