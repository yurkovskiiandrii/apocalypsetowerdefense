using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WavesManager : MonoBehaviour
{
    [SerializeField] private LevelUI _levelUI;
    [SerializeField] private List<WaveDataContainer> _waves;
    private UnityAction<int, UnityAction> _onWaveStart;
    private UnityAction<int> _onUpdateInfoNextWave;
    private UnityAction _onWaveEnd;
    private static bool _isStartedWaitingTimeForWave = false;

    private int _waveNumber = 0;
    private int _lastWaveHasDisplayTime = 0;

    private int _spawnPointsCount;
    private int _spawnPointsFormationsLeft = 0;
    private Coroutine _timerCoroutine;
    private TerrainGrid _terrainGrid;
    public List<WaveDataContainer> Waves => _waves;
    public static bool IsStartedWaitingTimeForWave { get => _isStartedWaitingTimeForWave; }

    public void Init(List<SpawnPoint> spawnPoints, /*UnityAction OnWaveStart,*/ UnityAction OnWaveEnd)
    {
        //onWaveStart += OnWaveStart;
        _terrainGrid = TerrainGrid.Instance;
        _onWaveEnd += OnWaveEnd;
        _spawnPointsCount = spawnPoints.Count;

        foreach (SpawnPoint spawnPoint in spawnPoints)
        {
            _onUpdateInfoNextWave += spawnPoint.CheckDisplayOfFormationsFlag;
            _onWaveStart += spawnPoint.SpawnFormations;
        }
        _onUpdateInfoNextWave?.Invoke(_waveNumber);
    }

    public void StartNewWave(int waveNumber)
    {
        _waveNumber = waveNumber;

        _isStartedWaitingTimeForWave = Waves.Exists(x => x.Wave == waveNumber && x.HasDisplayTime);

        float spawnDelay = Waves.Find(x => x.Wave == waveNumber).SpawnTimeDelay;

        if (_isStartedWaitingTimeForWave)
        {
            spawnDelay = GetTimeToNextWave(waveNumber);
            StartTimerTextToNextWave(spawnDelay);
        }
        //_onWaveStart?.Invoke(_waveNumber, spawnDelay, SpawnPointFormationsLeft);
        _timerCoroutine = StartCoroutine(TimerWaveCoroutine(spawnDelay));
    }

    private void SpawnPointFormationsLeft()
    {
        _spawnPointsFormationsLeft++;
        print("spawnpoint formation end " + (_spawnPointsCount - _spawnPointsFormationsLeft) + " left");
        if (_spawnPointsFormationsLeft == _spawnPointsCount)
        {
            _spawnPointsFormationsLeft = 0;
            _onWaveEnd?.Invoke();
        }
    }

    public int GetWavesCount() => _waves.Count;

    private float GetTimeToNextWave(int currentWave)
    {
        float time = 0;
        foreach (WaveDataContainer item in Waves)
        {
            if (item.Wave > _lastWaveHasDisplayTime && item.Wave <= currentWave)
            {
                time += item.SpawnTimeDelay;

                if (item.Wave == currentWave && item.HasDisplayTime)
                {
                    if (item.HasDisplayTime)
                        _lastWaveHasDisplayTime = currentWave;
                    break;
                }
            }
        }
        return time;
    }

    private void EndWaitingTimeForWave()
    {
        Debug.Log("End Waiting Timer Wave ");

        _isStartedWaitingTimeForWave = false;

        LevelEvents.Instance.OnEndWaitingTimeForWave();

        _terrainGrid.HideFreeCellsSprite();
    }
 
    private void StartWaitingTimeForWave()
    {
        _terrainGrid.ShowFreeCellsSprite();
    }

    public void StartTimerTextToNextWave(float time)
    {
        var timer = DOVirtual.Int((int)time, 0, time, (int value) => _levelUI.ShowTimeToNextWave(value)).SetEase(Ease.Linear); ;
        timer.OnStart<Tweener>(() => { _levelUI.ShowTimeToNextWavePanel(true); StartWaitingTimeForWave(); });
        timer.onComplete += () => { _levelUI.ShowTimeToNextWavePanel(false); EndWaitingTimeForWave(); };
    }

    IEnumerator TimerWaveCoroutine(float time)
    {
        Debug.Log("StartWaveCoroutine Time = " + time);

        yield return new WaitForSeconds(time);

        _onUpdateInfoNextWave?.Invoke(_waveNumber);
        _onWaveStart?.Invoke(_waveNumber, SpawnPointFormationsLeft);
    }
}
