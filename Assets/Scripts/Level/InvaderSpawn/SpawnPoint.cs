using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField] private LayerMask _spawnPointLayer;
    [SerializeField] GameObject _flag;
    [SerializeField] private List<WaveFormations> _wavesFormations;
    public UnityEvent<SpawnPoint> _onPointClick;//LevelUI.OpenInfoSpawnPointPanel

    private int _formationsLeft = 0;
    private bool _isShowFlag = false;
    private Transform _transform;
    private Transform _invadersPlaceholder;
    private LevelCamera _levelCamera;
    private Camera _camera;
    public int CurrentWave { get; set; } = 0;

    private UnityAction _onWaveFormationsLeft;
    private UnityAction<int, bool> _onInvaderDied;
    private UnityAction _onInvaderSpawned;
    private Action _onUnitReachDestination;

    public List<WaveFormations> WavesFormations { get { return _wavesFormations; } }

    private void Awake()
    {      
        _levelCamera = LevelCamera.Instance;
        _camera = Camera.main;
    }

    public void Init(Transform invadersPlaceholder, UnityAction OnInvaderSpawned, UnityAction<int, bool> OnInvaderDied, Action OnUnitReachDestination)
    {
        _transform = transform;
        _invadersPlaceholder = invadersPlaceholder;
        _onInvaderSpawned = OnInvaderSpawned;
        _onInvaderDied = OnInvaderDied;
        _onUnitReachDestination = OnUnitReachDestination;
    }

    public void SpawnFormations(int wave, UnityAction OnWaveFormationsLeft)
    {
        CurrentWave = wave;
        _onWaveFormationsLeft = OnWaveFormationsLeft;

        List<TimeFormations> waveTimeFormations = _wavesFormations.Find(x => x.wave == wave).timeFormations;
        if (waveTimeFormations == null)
        {
            CheckFormationsLeft();
            return;
        }

        _formationsLeft = waveTimeFormations.Count;

        foreach (var timeFormation in waveTimeFormations)
        {
            Formation newFormation = Instantiate(timeFormation.formation, _transform.position, _transform.rotation, _invadersPlaceholder);
            newFormation.Spawn(RemoveFormation, _onInvaderSpawned, _onInvaderDied, _onUnitReachDestination);
        }
    }

    public void CheckDisplayOfFormationsFlag(int currentWave)
    {
        List<TimeFormations> nextWaveTimeFormations = _wavesFormations.Find(x => x.wave == currentWave + 1).timeFormations;
        _isShowFlag = nextWaveTimeFormations != null && nextWaveTimeFormations.Count > 0;
        _flag.SetActive(_isShowFlag);
    }

    private void RemoveFormation()
    {
        _formationsLeft--;
        CheckFormationsLeft();
    }

    private void CheckFormationsLeft()
    {
        print(name + " " + _formationsLeft);
        if (_formationsLeft <= 0)
        {
            _onWaveFormationsLeft?.Invoke();
        }
    }

    void Update()
    {
        if (_isShowFlag)
            TouchHandler();
    }

    private void TouchHandler()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (MouseDetectorAboveUI.IsMouseOverUI(Input.mousePosition))
                return;
            var mousePositionToWorldPoint = _camera.ScreenToWorldPoint(Input.mousePosition);

            if (Physics.Raycast(mousePositionToWorldPoint, _camera.transform.forward, out RaycastHit hit, 100f, _spawnPointLayer))
            {
                if (hit.transform.gameObject == this.gameObject)
                {
                    _onPointClick?.Invoke(this);
                    _levelCamera.DisableMove();
                    _levelCamera.DisableZoom();
                }
            }
        }
    }
}
