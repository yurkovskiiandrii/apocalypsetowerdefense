using UnityEngine;
using UnityEngine.Events;

public class LevelEvents : SingletonComponent<LevelEvents>
{
    [Header("Place objects")]
    [SerializeField] private UnityEvent _onPlaceStart;
    [SerializeField] private UnityEvent _onPlaceEnd;
    [SerializeField] private UnityEvent _onPlaceCancel;
    [SerializeField] private UnityEvent _onWaveStart;
    [SerializeField] private UnityEvent _onUpdateMoney;
    [SerializeField] private UnityEvent _onEndWaitingTimeForWave;
    //[SerializeField] private UnityEvent _onGameOver;
    //[SerializeField] private UnityEvent _onGameWin;

    public void OnPlaceStart() => _onPlaceStart?.Invoke();
    public void OnPlaceEnd() => _onPlaceEnd?.Invoke();
    public void OnPlaceCancel() => _onPlaceCancel?.Invoke();
    public void OnWaveStart() => _onWaveStart?.Invoke();
    public void OnUpdateMoney() => _onUpdateMoney?.Invoke();
    public void OnEndWaitingTimeForWave() => _onEndWaitingTimeForWave?.Invoke();
    //public void OnGameOver() => _onGameOver?.Invoke();
    //public void OnGameWin() => _onGameWin?.Invoke();
    public void SubscribeOnPlaceStart(UnityAction callBack) => _onPlaceStart.AddListener(callBack);
    public void UnsubscribeOnPlaceStart(UnityAction callBack) => _onPlaceStart.RemoveListener(callBack);

    public void SubscribeOnPlaceEnd(UnityAction callBack) => _onPlaceEnd.AddListener(callBack);
    public void UnsubscribeOnPlaceEnd(UnityAction callBack) => _onPlaceEnd.RemoveListener(callBack);

    public void SubscribeOnPlaceCancel(UnityAction callBack) => _onPlaceCancel.AddListener(callBack);
    public void UnsubscribeOnPlaceCancel(UnityAction callBack) => _onPlaceCancel.RemoveListener(callBack);

    public void SubscribeOnWaveStart(UnityAction callBack) => _onWaveStart.AddListener(callBack);

    public void SubscribeOnUpdateMoney(UnityAction callBack) => _onUpdateMoney.AddListener(callBack);

    public void SubscribeOnEndWaitingTimeForWave(UnityAction callBack) => _onEndWaitingTimeForWave.AddListener(callBack);
    public void UnsubscribeOnEndWaitingTimeForWave(UnityAction callBack) => _onEndWaitingTimeForWave.RemoveListener(callBack);

    //public void SubscribeOnGameOver(UnityAction callBack) => _onGameOver.AddListener(callBack);
    //public void SubscribeOnGameWin(UnityAction callBack) => _onGameWin.AddListener(callBack);
}
