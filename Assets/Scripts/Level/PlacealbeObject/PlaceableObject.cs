using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Events;

public class PlaceableObject : MonoBehaviour, IPlaceableObject
{
    [SerializeField] protected LayerMask propsLayer;
    [SerializeField] protected LayerMask passableLayer;

    [Header("Components")]
    [SerializeField] protected GameObject detectorPrafab;

    [Header("Settings")]
    [SerializeField] protected LayerMask cellLayer;
    [SerializeField] protected LayerMask detectorLayer;

    [Header("Visualise")]
    [SerializeField] private bool _showCells;
    [SerializeField] private GizmosDrawer.LineWidth _lineWidth;

    //Header("Custom Editor Properties")
    [HideInInspector] public int width;
    [HideInInspector] public int height;
    [HideInInspector] public bool[] boolArray = new bool[1];
    [HideInInspector] public bool isPlaced;
    [HideInInspector] public bool inContainer;
    [HideInInspector] public List<Vector2Int> cellsPositions = new List<Vector2Int>();

    [SerializeField] protected UnitPlacingUI placingUISpawnMenu;
    [Header("Prop Properties")]
    [SerializeField] protected UnitPlacingUI placingUITouchMenu;

    private UnityAction<int> _onDefenderSpawn;
    private UnityAction _onSpawnEnd;
    private TerrainGrid _grid;
    private LevelCamera _levelCamera;
    private Camera _camera;
    private Vector2 _cellSize;
    private UnitPlacingUI placingUI;
    private List<Cell> _occupiedCells = new List<Cell>();
    private bool _isInit = false;
    private HeroesManager _heroesManager;

    protected LevelEvents _levelEvents;
    protected Coroutine touchHandler;
    protected Transform detectorsContainer;
    protected List<CellDetector> detectors = new List<CellDetector>();
    protected bool _isSpawned = false;
    private Vector3 _startPosUnit;
    private List<Cell> _startCellsUnit;
    private Prop _prop;

    public virtual PlaceableObjectType Type => PlaceableObjectType.None;

    public virtual void Init(UnityAction<int> OnDefenderSpawn, UnityAction OnSpawnEnd)
    {
        _camera = Camera.main;
        _levelCamera = LevelCamera.Instance;
        _levelEvents = LevelEvents.Instance;
        _grid = TerrainGrid.Instance;
        _heroesManager = HeroesManager.Instance;
        _cellSize = _grid.CellSize;
        placingUI = placingUISpawnMenu;
        _onDefenderSpawn += OnDefenderSpawn;
        _onSpawnEnd += OnSpawnEnd;
        _isInit = true;

        _levelEvents.SubscribeOnEndWaitingTimeForWave(() => { if (_prop != null && !_prop.IsInit) CancelPlacing(); else EndPlacing(); });
    }

    public virtual void Activate()
    {

    }
    public virtual void Deactivate()
    {

    }

    //private void Update()
    //{
        //if (!_touchMenu)
        //    return;

        //if (TerrainGrid.Instance.IsRedacting || !_isInit)
        //    return;

        //if (Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit hit;
        //    if (Physics.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), _camera.transform.forward, out hit, 100f, propsLayer))//for camera - orthographic                                                                                                                               //if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, propsLayer))
        //    {
        //    Debug.Log("hit.transform " + hit.transform.name + " / hit.colider " + hit.collider.name+" / " + gameObject.name);
        //        if (hit.transform.parent.parent.gameObject == this.gameObject)
        //        {
        //            if (TryGetComponent(out Prop prop) && prop.IsInit)
        //            {
        //                _touchMenu.OpenPropMenu(prop.UpgradeConfig);

        //                if (!WavesManager.IsStartedWaitingTimeForWave)
        //                    return;

        //                StartRedacting();
        //                return;
        //            }
        //        }
        //    }
        //    else if (Physics.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), _camera.transform.forward, out hit, 100f, passableLayer))//for camera - orthographic                                                                                                                                           //else if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, passableLayer))
        //    {
        //        if (hit.transform.parent.parent.gameObject == this.gameObject)
        //        {
        //            if (TryGetComponent(out Prop prop) && !prop.IsInit)
        //            {
        //                _touchMenu.OpenPropMenu(prop.UpgradeConfig);

        //                if (!WavesManager.IsStartedWaitingTimeForWave)
        //                    return;

        //                StartRedactingDestroyed();
        //                return;
        //            }
        //        }
        //    }
        //}
    //}

    public void StartRedacting()
    {
        if (_heroesManager.IsActiveUnitOrProps())
        {
            _heroesManager.DeactiveCurrentHeroInScene();
            _levelEvents.OnPlaceCancel();
        }
        _startPosUnit = transform.position;
        _startCellsUnit = _occupiedCells;

        print("Start redacting");
        placingUI = placingUITouchMenu;
        _occupiedCells.ForEach(c => c.SetUnplaceStatus());
        _occupiedCells = new List<Cell>();
        StartPlacing();
    }

    public void StartRedactingDestroyed()
    {
        if (_heroesManager.IsActiveUnitOrProps())
        {
            _heroesManager.DeactiveCurrentHeroInScene();
            _levelEvents.OnPlaceCancel();
        }
        //_prop = gameObject.GetComponent<Prop>();
        print("Start redacting destroyed");
        //placingUI = placingUITouchMenu;
        placingUI.gameObject.SetActive(true);
        placingUI.OnStartPlacing(() =>
        {
            placingUI.gameObject.SetActive(false);
        }, CancelPlacing);
    }

    public void StartPlacing()
    {
        print("Start placing");
        StopCoroutineTouchHandler();

        _prop = gameObject.GetComponent<Prop>();
        _prop.SetPlacementMode();
        _grid.IsRedacting = true;

        _levelEvents.OnPlaceStart();

        placingUI.gameObject.SetActive(true);
        placingUI.OnStartPlacing(() =>
        {
            if (CanPlace())
                EndPlacing();
        }, CancelPlacing);

        SpawnDetectors();
        _grid.ShowPlacebleCells();

        touchHandler = StartCoroutine(TouchHandler(transform));
    }

    public void EndPlacing()
    {
        print("End placing PlaceableObject ");
        if (_prop == null) return;

        //levelEvents.OnPlaceEnd();
        placingUI.gameObject.SetActive(false);

        StopCoroutineTouchHandler();

        foreach (var detector in detectors)
        {
            var cell = detector.CurrCell;

            if (cell)
            {
                _occupiedCells.Add(cell);

                if (_isSpawned && !WavesManager.IsStartedWaitingTimeForWave)
                {
                    transform.position = _startPosUnit;
                    _occupiedCells = _startCellsUnit;
                }

                if (TryGetComponent(out Mine mine))
                    cell.SetPlace();
                else
                    cell.SetPlaceStatus();
                detector.Deactivate();
            }
        }

        if (!_isSpawned)
        {
            if (TryGetComponent(out Prop prop))
            {
                if (TryGetComponent(out Mine mine))
                {
                    mine.Init(delegate { _occupiedCells.ForEach(c => c.SetUnplace() /*c.SetUnplaceStatus()*/); TerrainGrid.Instance.UpdateNavMeshGrid(); });
                }
                else
                {
                    prop.Init(delegate { TerrainGrid.Instance.UpdateNavMeshGrid(); });
                }
            }
            _onDefenderSpawn?.Invoke(GetComponent<Prop>().DefenderOptions.cost);
        }
        _onSpawnEnd?.Invoke();
        _isSpawned = true;
        gameObject.GetComponent<Prop>().SetMainMaterial();

        _grid.HideFreeCells();
        _grid.IsRedacting = false;
        _grid.UpdateNavMeshGrid();
        _prop = null;
    }

    public void CancelPlacing()
    {
        print("Cancel placing");
        if (_prop == null) return;

        //levelEvents.OnPlaceCancel();
        placingUI.gameObject.SetActive(false);

        StopCoroutineTouchHandler();

        foreach (var detector in detectors)
        {
            if (detector.CurrCell)
            {
                detector.CurrCell.SetUnplaceStatus();
                detector.CurrCell.HideIndicator();
                detector.Deactivate();
            }
        }
        gameObject.GetComponent<Prop>().SetMainMaterial();

        _grid.HideFreeCells();
        _grid.IsRedacting = false;

        _onSpawnEnd?.Invoke();
        Destroy(gameObject);
        _prop = null;
    }
   
    private void StopCoroutineTouchHandler()
    {
        if (touchHandler != null)
        {
            StopCoroutine(touchHandler);
            touchHandler = null;
        }
    }

    public bool CanPlace()
    {
        foreach (var detector in detectors)
        {
            var cell = detector.CurrCell;

            if (cell && cell.IsObjectPlaced || cell && !cell.CanPlace)
                return false;
        }

        return true;
    }

    protected void SpawnDetectors()
    {
        var size = _grid.CellSize;
        if (!detectorsContainer)
        {
            detectorsContainer = new GameObject("Detectors Container").transform;
        }
        detectorsContainer.SetParent(transform);
        detectorsContainer.position = transform.position;

        detectors.Clear();

        foreach (var point in CalculateShape())
        {
            var detector = Instantiate(detectorPrafab, detectorsContainer).GetComponent<CellDetector>();
            var position = new Vector3(size.x * point.x, 0, size.y * point.y);

            detector.transform.localPosition = position;
            detector.Init();
            detector.Activate();

            detectors.Add(detector);
        }
    }

    protected List<Vector2Int> CalculateShape()
    {
        var shape = new List<Vector2Int>();

        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                var index = j * width + i;

                if (boolArray[index])
                    shape.Add(new Vector2Int(i, j));
            }
        }

        return shape;
    }


    public IEnumerator TouchHandler(Transform propsTransform)
    {
        Debug.Log("Start Coroutine TouchHandler / Prop");
        while (true)
        {
            yield return null;

            if (Input.GetMouseButtonDown(0))
            {
                if (MouseDetectorAboveUI.IsMouseOverUI(Input.mousePosition))
                    continue;

                if (Physics.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), _camera.transform.forward, out RaycastHit hit, 100f, detectorLayer))//for camera - orthographic                                                                                                                                                         //if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100f, detectorLayer))
                {
                    if (Physics.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), _camera.transform.forward, out hit, 100f, cellLayer))//for camera - orthographic                                                                                                                                              //if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, cellLayer))
                    {
                        _levelCamera.DisableMove();
                        _levelCamera.DisableZoom();

                        var detectedCell = hit.transform.GetComponent<Cell>();
                        var currentGridPosition = detectedCell.Position;

                        while (Input.GetMouseButton(0))
                        {
                            yield return null;

                            if (Physics.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), _camera.transform.forward, out hit, 100f, cellLayer))//for camera - orthographic
                                                                                                                                                      //if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hit, 100f, cellLayer))
                            {
                                var gridPosition = _grid.GetGridPosition(hit.transform.position);
                                detectedCell = hit.transform.GetComponent<Cell>();

                                if (detectedCell.CanPlace && gridPosition != currentGridPosition)
                                {
                                    var direction = gridPosition - currentGridPosition;
                                    var position = transform.position;

                                    position += new Vector3(_cellSize.x * direction.x, 0, _cellSize.y * direction.y);
                                    transform.position = position;

                                    currentGridPosition = gridPosition;
                                }
                            }
                        }

                        _levelCamera.EnableMove();
                        _levelCamera.EnableZoom();
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (!_showCells)
            return;

        var enableCells = new List<Vector2Int>();
        var disableCells = new List<Vector2Int>();
        var shape = CalculateShape();

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                var point = new Vector2Int(i, j);

                if (shape.Contains(point))
                    enableCells.Add(point);

                else disableCells.Add(point);
            }
        }

        GizmosDrawer.DrawCubes(transform.position, enableCells, _cellSize, Color.green, _lineWidth);
        GizmosDrawer.DrawCubes(transform.position, disableCells, _cellSize, Color.red, _lineWidth);
    }

    public List<Vector2Int> PlaceFromEditor()
    {
        var gridPosition = _grid.GetNearestCellPosition(transform.position);
        var position = _grid.GetCellPosition(gridPosition);
        var placedCellsIndexs = _grid.GetPlacedCellsIndexs();
        var cellsIndexs = new List<Vector2Int>();
        var shape = CalculateShape();

        transform.position = position;

        foreach (var point in shape)
            cellsIndexs.Add(gridPosition + point);

        foreach (var point in cellsIndexs)
            if (placedCellsIndexs.Contains(point))
                return new List<Vector2Int>();

        return cellsIndexs;
    }
}
