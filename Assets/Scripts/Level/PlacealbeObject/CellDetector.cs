using UnityEngine;
using System;

public class CellDetector : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private CapsuleCollider _collider;
    //[SerializeField] private BoxCollider _collider;
    //[Header("Settings")]
    //[SerializeField][Range(0f, 1f)] float _colliderSize;

    [SerializeField] private LayerMask _cellLayer;

    [SerializeField] private string _cellTag;

    public Cell CurrCell { get; private set; }

    private Vector2 _size;

    public void Init()
    {
        _size = TerrainGrid.Instance.CellSize;
        _collider.enabled = false;

        transform.localScale = new Vector3(_size.x, 1f, _size.y);
        //_collider.size = new Vector3(_collider.size.x * _colliderSize, _collider.size.y, _collider.size.z * _colliderSize);
    }

    public void Activate()
    {
        _collider.enabled = true;
    }

    public void Deactivate()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_cellTag))
        {
            CurrCell = other.GetComponent<Cell>();
            //Debug.Log("CurrCell Position =" + CurrCell.gameObject.transform.position);
        }
    }

    public void SetCurrCell(Cell cell)
    {
        CurrCell = cell;
    }
}
