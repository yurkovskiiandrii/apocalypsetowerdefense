using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IPlaceableObject
{
    public void StartPlacing();

    public void EndPlacing();

    public void CancelPlacing();

    public bool CanPlace();

    public IEnumerator TouchHandler(Transform transform);
}
