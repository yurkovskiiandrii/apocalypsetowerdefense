using System.Collections;
using UnityEngine;

public class LevelCamera : SingletonComponent<LevelCamera>
{
    [Header("Camera Settings")]
    [SerializeField] private Camera _camera;
    [SerializeField] private Collider _cameraLimitCollider;
    [SerializeField] private LayerMask _floorLayer;//Floor
    [SerializeField] private LayerMask _objectLayer;//CellDetector
    private float _leftLimit = 0;
    private float _rightLimit = 0;
    private float _bottomLimit = 0;
    private float _upperLimit = 0;
    private Bounds _bounds;

    [Header("Scroll Settings")]
    [SerializeField] private float _scrollSpeed;//15
    [SerializeField] private float _inputMistake;//0.25

    [SerializeField] private Vector2 _horizontalMinOffset;//-0.5;1
    [SerializeField] private Vector2 _horizontalMaxOffset;//0.5;7
    [SerializeField] private Vector2 _verticalMinOffset;//-1;-1
    [SerializeField] private Vector2 _verticalMaxOffset;//1;1

    [SerializeField] private bool _decreaseOffsetWithZoom;//true
    [SerializeField][Range(0f, 1f)] private float _minDecreaseCoef;//0.2

    [Header("Zoom Settings")]
    [SerializeField] private float _horizontalZoomSpeed;//300
    [SerializeField] private float _horizontalDefaultZoom;
    [SerializeField] private float _horizontalMinZoom;
    [SerializeField] private float _horizontalMaxZoom;

    [SerializeField] private float _verticalZoomSpeed;
    [SerializeField] private float _verticalDefaultZoom;
    [SerializeField] private float _verticalMinZoom;
    [SerializeField] private float _verticalMaxZoom;

    private float _currZoom;
    private float _defaultZoom;
    private float _zoomSpeed;
    private float _currMinZoom;
    private float _currMaxZoom;

    private Vector2 _currMinOffset;
    private Vector2 _currMaxOffset;

    private ScreenOrientation _currOrientation;

    private TerrainGrid _terrainGrid;

    private Coroutine _moveHandler;
    private Coroutine _zoomHandler;
    private Coroutine _orientationHandler;

    private Vector2 _minCameraLimits;
    private Vector2 _maxCameraLimits;

    [Header("Standalone / Zoom Settings")]
    [SerializeField] private float _zoomSensitivity = 5f;
    [SerializeField] private float _zoomElasticity = 0.2f;
    [SerializeField] private float _zoomElasticityLerpSpeed = 5f;
    [Header("Standalone / Movement Settings")]
    [SerializeField] private float _defaultDampSmoothTime = 0.2f;
    [SerializeField] private float _dampMaxSpeed = 300f;
    [SerializeField] private float _successiveVelocityMult = 0.2f;
    [SerializeField] private float _successiveVelocityThreshold = 1f;
    [SerializeField] private float _smoothTimeVelocityDependenceModif = 15;
    [SerializeField] private float _smoothTimeVelocityDependenceClamp = 0.5f;

    private int _prevTouchCount;
    private Vector3 _currentTargetPoint;
    private Vector3 _prevCursorPos;
    private Vector3 _currentDampVelocity;
    private float _currentDampSmoothTime;
    private Plane _plane;
    private bool _prevIsFocused;

    private bool _isStandalone;

    private bool _isEnableMove;
    private bool _isEnableZoom;

    public void Init()
    {
        _bounds = _cameraLimitCollider.bounds;
        _terrainGrid = TerrainGrid.Instance;

        _currentTargetPoint = transform.position;
        _currentDampSmoothTime = _defaultDampSmoothTime;
        _plane = new Plane(Vector3.up, Vector3.zero);

#if UNITY_EDITOR
        _defaultZoom = _horizontalDefaultZoom;
        _zoomSpeed = _horizontalZoomSpeed;
        _currMinZoom = _horizontalMinZoom;
        _currMaxZoom = _horizontalMaxZoom;
        _currMinOffset = _horizontalMinOffset;
        _currMaxOffset = _horizontalMaxOffset;
        _currZoom = _defaultZoom;
        _camera.fieldOfView = _currZoom;
        CalculateBounds();
        //ClampPosition();
        _isStandalone = true;
#endif
        //Debug.Log(Application.platform);

        if (!_isStandalone)
        {
            _currOrientation = Screen.orientation;
            OnOrientationChange(true);
            CalculateBounds();
            ClampPosition();
            _orientationHandler = StartCoroutine(OrientationHandler());
        }
    }

    private void Update()
    {
        if (!_isStandalone && _isEnableMove)
            UpdateMobileMove();

        if (_isStandalone)
        {
            if (_isEnableMove)
                UpdateStandaloneMove();

            if (_isEnableZoom)
                UpdateStandaloneZoom();
        }
    }

    void LateUpdate()
    {
        if (_isEnableMove)
            UpdateMovement();

        if (_isStandalone && _isEnableZoom)
            UpdateZoom();
    }

    public void Activate()
    {
        if (!_isStandalone)
            EnableMobileInput();

        _isEnableMove = true;
        _isEnableZoom = true;
    }

    private void EnableMobileInput()
    {
        EnableMove();
        EnableZoom();
    }

    public void Deactivate()
    {
        if (!_isStandalone)
            StopAllCoroutines();

        _isEnableMove = false;
        _isEnableZoom = false;
    }

    public void EnableMove()
    {
        //if (!_isStandalone)
        //    _moveHandler = StartCoroutine(MoveHandler());

        _isEnableMove = true;
    }

    public void DisableMove()
    {
        //if (!_isStandalone)
        //{
        //    if (_moveHandler != null)
        //        StopCoroutine(_moveHandler);
        //}
        _isEnableMove = false;
    }

    public void EnableZoom()
    {
        if (!_isStandalone)
            _zoomHandler = StartCoroutine(ZoomHandler());

        _isEnableZoom = true;
    }

    public void DisableZoom()
    {
        if (!_isStandalone)
        {
            if (_zoomHandler != null)
                StopCoroutine(_zoomHandler);
        }
        _isEnableZoom = false;
    }

    private void CalculateBounds()
    {
        //var gridMin = _terrainGrid.MinPosition;
        //var gridMax = _terrainGrid.MaxPosition;
        //var leftDownPoint = Raycast(new Vector2(0, 0), false);
        //var rightUpPoint = Raycast(new Vector2(_camera.pixelWidth, _camera.pixelHeight), false);
        //var zoomCoef = Mathf.Clamp((_currZoom - _currMinZoom) / (_currMaxZoom - _currMinZoom), _minDecreaseCoef, 1f);
        //zoomCoef = _decreaseOffsetWithZoom ? zoomCoef : 1f;

        //var diff = transform.position - leftDownPoint;
        ////var x = gridMin.x - diff.x + _minOffset.x * zoomCoef;
        //var x = gridMin.x + _currMinOffset.x * zoomCoef;
        //var y = gridMin.z + diff.z + _currMinOffset.y * zoomCoef;
        //_minCameraLimits = new Vector2(x, y);

        //diff = transform.position - new Vector3(leftDownPoint.x, 0, rightUpPoint.z);
        ////x = gridMax.x + diff.x + _maxOffset.x * zoomCoef;
        //x = gridMax.x + _currMaxOffset.x * zoomCoef;
        //y = gridMax.z + diff.z + _currMaxOffset.y * zoomCoef;
        //_maxCameraLimits = new Vector2(x, y);

        //new Limit
        //float distanceToObject = _camera.transform.position.y;
        //var camHeight = 2.0f * distanceToObject * Mathf.Tan(_camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        //var camWidth = camHeight * _camera.aspect;

        var camWidth = _camera.orthographicSize * 2f * Screen.width / Screen.height;
        var camHeight = _camera.orthographicSize * 2f;

        _leftLimit = _bounds.min.x + camWidth / 2;
        _rightLimit = _bounds.max.x - camWidth / 2;
        _bottomLimit = _bounds.min.z + camHeight / 2;
        _upperLimit = _bounds.max.z - camHeight / 2;

        _minCameraLimits = new Vector2(_leftLimit, _bottomLimit);
        _maxCameraLimits = new Vector2(_rightLimit, _upperLimit);
    }

    //Mobile logic
    private void ClampPosition()
    {
        var x = Mathf.Clamp(transform.position.x, _minCameraLimits.x, _maxCameraLimits.x);
        var z = Mathf.Clamp(transform.position.z, _minCameraLimits.y, _maxCameraLimits.y);

        transform.position = new Vector3(x, transform.position.y, z);
    }

    //private IEnumerator MoveHandler()
    //{
    //    var startScrollPosition = new Vector3();

    //    while (true)
    //    {
    //        yield return null;

    //        if (Input.touchCount == 1)
    //        {
    //            var touch = Input.GetTouch(0);

    //            if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Stationary)
    //            {
    //                var position = Raycast(Input.GetTouch(0).position);

    //                if (position != Vector3.zero)
    //                    startScrollPosition = position;
    //            }

    //            if (touch.phase == TouchPhase.Moved)
    //            {
    //                var currScrollPosition = Raycast(Input.GetTouch(0).position);

    //                if (CanScroll(startScrollPosition, currScrollPosition))
    //                {
    //                    var diff = currScrollPosition - startScrollPosition;

    //                    var x = Mathf.Clamp(Mathf.Lerp(transform.position.x, transform.position.x - diff.x, _scrollSpeed * Time.deltaTime), _minCameraLimits.x, _maxCameraLimits.x);
    //                    var z = Mathf.Clamp(Mathf.Lerp(transform.position.z, transform.position.z - diff.z, _scrollSpeed * Time.deltaTime), _minCameraLimits.y, _maxCameraLimits.y);

    //                    transform.position = new Vector3(x, transform.position.y, z);
    //                }
    //            }
    //        }

    //        else startScrollPosition = Vector3.zero;
    //    }
    //}

    private Vector3 Raycast(Vector2 cameraPosition, bool checkCondiiton = true)
    {
        var hitPosition = Vector3.zero;
        var ray = _camera.ScreenPointToRay(cameraPosition);

        if (checkCondiiton && CanRaycast(ray) || !checkCondiiton)
            if (Physics.Raycast(ray, out RaycastHit hit, 100f, _floorLayer))
                hitPosition = hit.point;

        return hitPosition;
    }

    private bool CanRaycast(Ray ray)
    {
        if (Physics.Raycast(ray, out RaycastHit hit, 100f, _objectLayer))
            return false;

        return true;
    }

    private bool CanScroll(Vector3 startScrollPosition, Vector3 currScrollPosition)
    {
        var diff = currScrollPosition - startScrollPosition;

        if (Mathf.Abs(diff.magnitude) < _inputMistake)
            return false;

        if (startScrollPosition == Vector3.zero)
            return false;

        return true;
    }

    private IEnumerator ZoomHandler()
    {
        while (true)
        {
            yield return null;

            if (Input.touchCount == 2)
            {
                var startZoomPosition = Raycast(Input.GetTouch(0).position, false);

                if (startZoomPosition != Vector3.zero)
                {
                    DisableMove();

                    while (Input.touchCount == 2)
                    {
                        var firstTouch = Input.GetTouch(0);
                        var secondTouch = Input.GetTouch(1);

                        var prevFirstTouchPosition = firstTouch.position - firstTouch.deltaPosition;
                        var prevSecondTouchPosition = secondTouch.position - secondTouch.deltaPosition;

                        var prevTouchDeltaMag = (prevFirstTouchPosition - prevSecondTouchPosition).magnitude;
                        var currTouchDeltaMag = (firstTouch.position - secondTouch.position).magnitude;

                        var zoomFactor = Time.deltaTime * (currTouchDeltaMag - prevTouchDeltaMag);

                        _currZoom = Mathf.Clamp(_currZoom - zoomFactor, _currMinZoom, _currMaxZoom);
                        //_camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, _currZoom, Time.deltaTime * _zoomSpeed);
                        _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _currZoom, Time.deltaTime * _zoomSpeed);
                        CalculateBounds();
                        ClampPosition();

                        yield return null;
                    }

                    EnableMove();
                }
            }
        }
    }

    private IEnumerator OrientationHandler()
    {
        while (true)
        {
            yield return null;

            if (_currOrientation != Screen.orientation)
            {
                _currOrientation = Screen.orientation;

                OnOrientationChange();
                CalculateBounds();
                ClampPosition();
            }
        }
    }

    private void OnOrientationChange(bool initCheck = false)
    {
        var zoomKoef = _currZoom / _currMaxZoom;

        if (_currOrientation == ScreenOrientation.LandscapeLeft || _currOrientation == ScreenOrientation.LandscapeRight)
        {
            _defaultZoom = _horizontalDefaultZoom;
            _zoomSpeed = _horizontalZoomSpeed;
            _currMinZoom = _horizontalMinZoom;
            _currMaxZoom = _horizontalMaxZoom;
            _currMinOffset = _horizontalMinOffset;
            _currMaxOffset = _horizontalMaxOffset;
        }

        else if (_currOrientation == ScreenOrientation.Portrait)
        {
            _defaultZoom = _verticalDefaultZoom;
            _zoomSpeed = _verticalZoomSpeed;
            _currMinZoom = _verticalMinZoom;
            _currMaxZoom = _verticalMaxZoom;
            _currMinOffset = _verticalMinOffset;
            _currMaxOffset = _verticalMaxOffset;
        }

        _currZoom = initCheck ? _defaultZoom : _currMaxZoom * zoomKoef;
        //_camera.fieldOfView = _currZoom;
        _camera.orthographicSize = _currZoom;
    }

    private void UpdateMobileMove()
    {
        if (Input.touchCount == 0)
            return;

        //Scroll
        if (Input.touchCount == 1)
        {
            Vector3 touchPos = Input.GetTouch(0).position;
            if (Input.GetTouch(0).phase == TouchPhase.Moved && _prevCursorPos != touchPos)
            {
                var delta = PlanePositionDelta(touchPos);
                AddDelta(delta);
            }

            _prevCursorPos = touchPos;
        }

        if (Input.touchCount == 0 && _prevTouchCount > 0)
            TrySuccessiveVelocity();

        _prevTouchCount = Input.touchCount;
    }

    //Standalone logic

    private void UpdateStandaloneZoom()
    {
        var zoomAxisValue = Input.GetAxis("Mouse ScrollWheel");
        if (zoomAxisValue != 0)
        {
            _currZoom -= zoomAxisValue * _zoomSensitivity;
            var elasticityMult = 1 + Mathf.Abs(_zoomElasticity);
            _currZoom = Mathf.Clamp(_currZoom, _currMinZoom / elasticityMult, _currMaxZoom * elasticityMult);
            //UpdateZoom();
        }
    }

    private void UpdateZoom()//LateUpdate
    {
        if (_currZoom > _currMaxZoom)
            _currZoom = Mathf.Lerp(_currZoom, _currMaxZoom, _zoomElasticityLerpSpeed * Time.deltaTime);
        if (_currZoom < _currMinZoom)
            _currZoom = Mathf.Lerp(_currZoom, _currMinZoom, _zoomElasticityLerpSpeed * Time.deltaTime);

        _currZoom = Mathf.Clamp(_currZoom, _currMinZoom, _currMaxZoom);
        //_camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, _currZoom, Time.deltaTime * _horizontalZoomSpeed);//perspective camera
        _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _currZoom, Time.deltaTime * _horizontalZoomSpeed);//orthographic camera
        CalculateBounds();
    }

    private void UpdateStandaloneMove()
    {
        if (Input.GetMouseButton(0))
        {
            if (_prevCursorPos == null || _prevIsFocused != Application.isFocused)
                _prevCursorPos = Input.mousePosition;

            var delta = PlanePositionDelta(Input.mousePosition);
            if (Input.mousePosition != _prevCursorPos)
                AddDelta(delta);
        }

        if (Input.GetMouseButtonUp(0))
        {
            TrySuccessiveVelocity();
        }
        _prevCursorPos = Input.mousePosition;
        _prevIsFocused = Application.isFocused;

        //UpdateMovement();
    }

    private void TrySuccessiveVelocity()
    {
        if (_currentDampVelocity.magnitude > _successiveVelocityThreshold)
        {
            _currentTargetPoint += _currentDampVelocity * _successiveVelocityMult;
            _currentTargetPoint.x = Mathf.Clamp(_currentTargetPoint.x, _minCameraLimits.x, _maxCameraLimits.x);
            _currentTargetPoint.z = Mathf.Clamp(_currentTargetPoint.z, _minCameraLimits.y, _maxCameraLimits.y);
        }
    }

    private void AddDelta(Vector3 delta)
    {
        var pos = _currentTargetPoint + delta;
        pos.x = Mathf.Clamp(pos.x, _minCameraLimits.x, _maxCameraLimits.x);
        pos.z = Mathf.Clamp(pos.z, _minCameraLimits.y, _maxCameraLimits.y);
        _currentTargetPoint = pos;
    }

    private void UpdateMovement()//LateUpdate
    {
        transform.position = Vector3.SmoothDamp(transform.position, _currentTargetPoint,
                            ref _currentDampVelocity, _currentDampSmoothTime, _dampMaxSpeed, Time.deltaTime);

        var a = _smoothTimeVelocityDependenceModif / _currentDampVelocity.magnitude;
        var b = _smoothTimeVelocityDependenceClamp;
        var clamp = a > b ? b : a;
        _currentDampSmoothTime = _defaultDampSmoothTime * (1 - clamp);
    }

    private Vector3 PlanePositionDelta(Vector3 mousePos)
    {
        //not moved
        if (_prevCursorPos == mousePos)
            return Vector3.zero;

        //delta
        var rayBefore = _camera.ScreenPointToRay(mousePos - (mousePos - _prevCursorPos));
        var rayNow = _camera.ScreenPointToRay(mousePos);
        float enterBefore;
        float enterNow;
        if (_plane.Raycast(rayBefore, out enterBefore) && _plane.Raycast(rayNow, out enterNow))
            return rayBefore.GetPoint(enterBefore) - rayNow.GetPoint(enterNow);

        //not on plane
        return Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(_leftLimit, _camera.transform.position.y, _upperLimit), new Vector3(_rightLimit, _camera.transform.position.y, _upperLimit));
        Gizmos.DrawLine(new Vector3(_leftLimit, _camera.transform.position.y, _bottomLimit), new Vector3(_rightLimit, _camera.transform.position.y, _bottomLimit));
        Gizmos.DrawLine(new Vector3(_leftLimit, _camera.transform.position.y, _upperLimit), new Vector3(_leftLimit, _camera.transform.position.y, _bottomLimit));
        Gizmos.DrawLine(new Vector3(_rightLimit, _camera.transform.position.y, _upperLimit), new Vector3(_rightLimit, _camera.transform.position.y, _bottomLimit));
    }
}