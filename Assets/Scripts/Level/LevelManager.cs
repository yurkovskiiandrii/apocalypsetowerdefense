using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private string _nextLevelName;
    [SerializeField] private int _maxHealthPoints;
    [SerializeField] private int _resouces = 500;
    [SerializeField] private float _levelStartDelay = 1f;
    [SerializeField] private LevelUI _levelUI;
    [SerializeField] private WavesManager _wavesManager;
    [SerializeField] private List<SpawnPoint> _spawnPoints = new List<SpawnPoint>();
    [SerializeField] private Transform _invadersPlaceholder;
    [SerializeField] private SpawnUI _spawnUI;
    [SerializeField] private DefendersSpawnManager _defendersSpawnManager;

    private int _wavesCount;
    private int _currentWave = 0;
    private int _healthPoints;
    private UnityAction onLevelStart;
    private UnityAction<bool> onLevelEnd;
    private LevelEvents _levelEvents;

    public int Resourses => _resouces;
    public int WavesCount { get => _wavesCount; }
    public int CurrentWave { get => _currentWave; }
    public int HealthPoints { get => _healthPoints; }

    private void Awake()
    {
        Time.timeScale = 1;
        _wavesCount = _wavesManager.GetWavesCount();
        _healthPoints = _maxHealthPoints;
        _spawnPoints.ForEach(p => p.Init(_invadersPlaceholder, OnInvaderSpawned, OnInvaderDied, OnUnitReachDestination));
        _levelUI.Init(_healthPoints, _resouces, _spawnPoints, _wavesCount, _nextLevelName);
        _spawnUI.Init(_resouces, OnDefenderSpawn);
        _wavesManager.Init(_spawnPoints, WaveEnd);

        _defendersSpawnManager.SubscribeSpawn(OnDefenderSpawn, OnUnitReachDestination);

        HeroesManager.Instance.SubscribeStartHero(OnUnitReachDestination);
        HeroesManager.Instance.SubscribeStartActivateHero(_spawnUI.StopCurrentSpawn);

        _levelEvents = LevelEvents.Instance;

        Invoke(nameof(StartLevel), _levelStartDelay);
    }

    private void Start()
    {
        LevelController.Instance.Init();
    }

    public void StartLevel()
    {
        HeroesManager.Instance.Init();
        StartNewWave();

        onLevelStart?.Invoke();
    }

    public void StartNewWave()
    {
        // _levelEvents.OnWaveStart();
        _currentWave++;
        _wavesManager.StartNewWave(_currentWave);
        print("start wave: " + _currentWave);
    }

    private void WaveStart()
    {
        print("start wave: " + _currentWave);
    }

    private void WaveEnd()
    {
        print("end wave: " + _currentWave);

        if (IsLastWave())
        {
            LevelWin();
            return;
        }

        StartNewWave();
    }

    public bool IsLastWave() => _currentWave == _wavesCount;

    private void OnInvaderSpawned()
    {
        //_levelUI.WaveProgressbarStep();
        _levelUI.CloseInfoSpawnPointPanel();
        _levelUI.ProgressbarStepByWave();
    }

    private void OnInvaderDied(int resources, bool isKilled)
    {
        if (!isKilled)
            return;

        _resouces += resources;
        OnResourcesUpdated();
    }

    private void OnUnitReachDestination()
    {
        //if (IsLosse())
        //    return;
        _healthPoints--;
        _levelUI.UpdateHp(_healthPoints);
        CheckLose();
    }

    private void OnDefenderSpawn(int resources)
    {
        UpdateResourses(resources);
    }

    public void UpdateResourses(int resources)
    {
        _resouces -= resources;
        OnResourcesUpdated();
    }

    private void OnResourcesUpdated()
    {
        _levelUI.UpdateResources(_resouces);
        _spawnUI.CheckPrices(_resouces);
        _levelEvents.OnUpdateMoney();
    }

    public bool IsEnoughMoney(int price)
    {
        return _resouces >= price;
    }

    private bool IsLosse() => _healthPoints <= 0;

    private void CheckLose()
    {
        if (IsLosse())
            LevelLose();
    }

    private void LevelWin()
    {
        _levelUI.ShowResourcesPanel(true);
        onLevelEnd?.Invoke(true);

        //_levelEvents.OnGameWin();

        Time.timeScale = 0;
    }

    private void LevelLose()
    {
        _levelUI.OpenLosePanel();
        onLevelEnd?.Invoke(false);

        //_levelEvents.OnGameOver();

        Time.timeScale = 0;
    }

    /*private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            StartLevel();
    }*/

}


[Serializable]
public struct WaveFormations
{
    public int wave;
    public List<TimeFormations> timeFormations;
}

[Serializable]
public struct TimeFormations
{
    public Formation formation;
}
