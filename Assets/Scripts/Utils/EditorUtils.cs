using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Utils
{
    public static class EditorUtils
    {
        private const string parentMenu = "Utils/";

#if UNITY_EDITOR

        [MenuItem(parentMenu + "ClearProgress")]

        private static void ClearProgress()
        {
            FileUtil.DeleteFileOrDirectory(Application.persistentDataPath);
        }

        [MenuItem(parentMenu + "DataPath")]

        private static void PersistentDataPath()
        {
            EditorUtility.RevealInFinder(Application.persistentDataPath);
        }

#endif       
    }
}