using System;
using UnityEngine;
using Ragdoll;


[CreateAssetMenu(fileName = "RagdollConfig", menuName = "GameConfig/RagdollConfig")]

public class RagdollConfig : SingletonScriptableObject<RagdollConfig>
{
    [HideInInspector] public int width = Enum.GetValues(typeof(MassObjectType)).Length;
    [HideInInspector] public int height = Enum.GetValues(typeof(RagdollPowerType)).Length;
    [HideInInspector] public float[] currentRagdollPower = new float[1];
  
    public float GetCurrentRagdollPower(RagdollPowerType ragdollPowerType, MassObjectType massObjectType)
    {
        var power = currentRagdollPower[(int)massObjectType * width + (int)ragdollPowerType];
        Debug.Log("massObjectType = " + massObjectType.ToString() + " / ragdollPowerType = " + ragdollPowerType.ToString() + "  / current Ragdoll Power = " + power);
        return power;//15
    }

    [ContextMenu("GetCurrentRagdollPowerTest")]
    public void GetCurrentRagdollPowerTest()
    {
        GetCurrentRagdollPower(RagdollPowerType.Heavy, MassObjectType.Medium);
    }
}
