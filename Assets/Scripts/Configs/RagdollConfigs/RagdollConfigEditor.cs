using UnityEditor;
using UnityEngine;
using Ragdoll;
using System;

[CustomEditor(typeof(RagdollConfig), true)]

public class RagdollConfigEditor : Editor
{
    private SerializedProperty _widthProperty;
    private SerializedProperty _heightProperty;
    private SerializedProperty _currentRagdollPowerProperty;

    private void OnEnable()
    {
        _widthProperty = serializedObject.FindProperty("width");
        _heightProperty = serializedObject.FindProperty("height");
        _currentRagdollPowerProperty = serializedObject.FindProperty("currentRagdollPower");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        MatrixRagdoll();

        serializedObject.ApplyModifiedProperties();
    }

    private void MatrixRagdoll()
    {
        _widthProperty.intValue = _widthProperty.intValue;
        _heightProperty.intValue = _heightProperty.intValue;

        GUILayout.Space(15f);
        GUILayout.Label("Matrix Ragdoll Power");
        GUILayout.Space(5f);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(150f);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(150f);
        GUILayout.Label("RagdolPowerType");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(100f);
        foreach (var item in System.Enum.GetValues(typeof(RagdollPowerType)))
        {
            GUILayout.Label(item.ToString());
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5f);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("MassObjectType");
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(5f);

        if (_currentRagdollPowerProperty.arraySize != _widthProperty.intValue * _heightProperty.intValue)
        {
            _currentRagdollPowerProperty.ClearArray();

            for (int j = 0; j < _heightProperty.intValue; j++)
            {
                for (int i = 0; i < _widthProperty.intValue; i++)
                {
                    var index = j * _widthProperty.intValue + i;

                    _currentRagdollPowerProperty.InsertArrayElementAtIndex(index);
                    _currentRagdollPowerProperty.GetArrayElementAtIndex(index).floatValue = 0;
                }
            }
        }

        for (int j = 0; j < _heightProperty.intValue; j++)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(((MassObjectType)j).ToString());
            GUILayout.Space(20);

            for (int i = 0; i < _widthProperty.intValue; i++)
            {
                var index = j * _widthProperty.intValue + i;
                var property = _currentRagdollPowerProperty.GetArrayElementAtIndex(index);

                property.floatValue = EditorGUILayout.FloatField(property.floatValue, new GUILayoutOption[]
                {
                   GUILayout.Width(25),
                });
                GUILayout.Space(15);
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}
