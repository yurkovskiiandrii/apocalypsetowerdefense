using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelProgressionConfig", menuName = "GameConfig/LevelProgressionConfig")]

public class LevelProgressionConfig : ScriptableObject
{
    public string unitName;
    public List<LevelProgression> levelProgression = new List<LevelProgression>();

    public string GetUnitName => unitName;

    public int GetCurrentUpgradeIndex() => levelProgression.FindLastIndex(x => x.open);

    public LevelProgression GetCurrentProgress() => levelProgression[GetCurrentUpgradeIndex()];

    public bool CanBeUpgraded() => GetCurrentUpgradeIndex() < levelProgression.Count - 1;

    public int GetNextUpgradePriceCoins() => levelProgression[NextIndex()].priceCoins;

    public LevelProgression GetNextProgress() => levelProgression[NextIndex()];

    public LevelProgressionConfig Clone()
    {

        var levelProgressionClone = new List<LevelProgression>();

        foreach (var item in levelProgression)
        {
            levelProgressionClone.Add(item.Clone());
        }
        LevelProgressionConfig clone = ScriptableObject.CreateInstance<LevelProgressionConfig>();
        clone.levelProgression = levelProgressionClone;
        clone.unitName = unitName;
        return clone;
    }
    public int NextIndex()
    {
        int index = GetCurrentUpgradeIndex();
        if (CanBeUpgraded())
            index += 1;
        return index;
    }

    public int GetTotalCostOfUpgrades()
    {
        var totalCost = 0;
        foreach (var item in levelProgression)
        {
            if (item.open)
                totalCost += item.priceCoins;
        }
        return totalCost;
    }

    //public int GetNextUpgradePriceDiamonds() => levelProgression[GetCurrentUpgradeIndex() + 1].priceDiamonds;
}
