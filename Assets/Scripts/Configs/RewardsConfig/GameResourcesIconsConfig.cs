using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameResourcesIconsConfig", menuName = "GameConfig/GameResourcesIconsConfig")]
public class GameResourcesIconsConfig : SingletonScriptableObject<GameResourcesIconsConfig>
{
    public List<GameResourcesIcon> resourcesIcons;

    public Sprite GetSprite(string nameResource)
    {
        var sprite = resourcesIcons.Find(x => x.nameResource == nameResource).resourcesSprite;
        if (sprite != null)
            return sprite;
        return resourcesIcons[0].resourcesSprite;
    }
}


