using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelRewards", menuName = "GameConfig/LevelRewards")]

public class LevelRewardsConfig : ScriptableObject
{
    public LevelRewards rewards;

    public (int star, List<GameResource> rewards) GetLevelRewards(int healt)
    {
        return rewards.GetRewards(healt);
    }

    private void OnValidate()
    {
        rewards.ValidateStarName();
    }
}
