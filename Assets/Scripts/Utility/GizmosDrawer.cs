using System.Collections.Generic;
using UnityEngine;

public static class GizmosDrawer
{
    public enum LineWidth
    {
        None,
        Small,
        Medium,
        Huge,
    }

    public static void DrawCubes(Vector3 positon, List<Vector2Int> gridPositions, Vector2 cellSize, Color color, LineWidth lineWidth = LineWidth.Small)
    {
        Gizmos.color = color;

        foreach (var point in gridPositions)
        {
            var center = positon + new Vector3(point.x * cellSize.x, 0f, point.y * cellSize.y);

            DrawCube(center, cellSize, CalculateLineWidth(lineWidth));
        }
    }

    private static void DrawCube(Vector3 center, Vector2 size, float lineWidth)
    {
        for (var i = -lineWidth; i <= 0; i += 0.005f)
        {
            Gizmos.DrawWireCube(center, new Vector3(size.x, 0f, size.y) + new Vector3(i, 0f ,i));
        }
    }

    private static float CalculateLineWidth(LineWidth lineWidth)
    {
        var result = lineWidth == LineWidth.None ? 0f
            : lineWidth == LineWidth.Small ? 0.05f
            : lineWidth == LineWidth.Medium ? 0.1f : 0.2f;

        return result;
    }

}
