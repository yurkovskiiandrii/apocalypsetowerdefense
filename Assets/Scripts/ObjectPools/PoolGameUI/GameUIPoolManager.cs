using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class GameUIPoolManager : SingletonComponent<GameUIPoolManager>, IPoolManager
{
    private Dictionary<GameObject, ObjectPool<Object>> _dictionaryPoolGameUI = new Dictionary<GameObject, ObjectPool<Object>>();
    private string parentObjectName = "_PoolGameUI";

    public T GetObjectFromPool<T>(T prefabGameUI) where T : MonoBehaviour
    {
        if (!_dictionaryPoolGameUI.ContainsKey(prefabGameUI.gameObject))
            _dictionaryPoolGameUI.Add(prefabGameUI.gameObject, new PoolGameUI<T>().InitPool(prefabGameUI, CreateParentForPool(prefabGameUI.name)));

        return _dictionaryPoolGameUI[prefabGameUI.gameObject].Get() as T;
    }

    public void ReleaseObjectToPool(GameObject prefabGameUIKey, Object objectGameUI)
    {
        if (_dictionaryPoolGameUI.TryGetValue(prefabGameUIKey, out ObjectPool<Object> value))
            value.Release(objectGameUI);
    }

    public Transform CreateParentForPool(string nameGameUI)
    {
        var parent = new GameObject(nameGameUI + parentObjectName);
        parent.transform.SetParent(transform);
        return parent.transform;
    }
}
