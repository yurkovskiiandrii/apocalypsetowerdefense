using Unity.VisualScripting;
using UnityEngine;
using Units;
using UnityEngine.Pool;


public class PoolGameUI<T> : BasePool where T : MonoBehaviour
{
    private Transform _parentUI;
    private T _gameUIPrefab;

    public override ObjectPool<Object> InitPool(Object gameUIPrefab, Transform parent)
    {
        _gameUIPrefab = gameUIPrefab as T;
        _parentUI = parent;
        return new ObjectPool<Object>(Create, Get, Release);
    }

    public T Create()
    {
        var newGameUI = MonoBehaviour.Instantiate(_gameUIPrefab, _parentUI);
        newGameUI.GameObject().SetActive(false);
        return newGameUI;
    }

    public override void Get(Object gameUI)
    {
        gameUI.GameObject().SetActive(true);
    }

    public override void Release(Object gameUI)
    {
        gameUI.GameObject().SetActive(false);
        gameUI.GameObject().transform.SetParent(_parentUI);
    }
}