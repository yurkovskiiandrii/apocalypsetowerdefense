using System.Collections;
using UnityEngine;

public interface IPoolManager
{
    public T GetObjectFromPool<T>(T prefab) where T : MonoBehaviour;
  
    public void ReleaseObjectToPool(GameObject prefabKey, Object poolObject);

    public Transform CreateParentForPool(string name);
}
