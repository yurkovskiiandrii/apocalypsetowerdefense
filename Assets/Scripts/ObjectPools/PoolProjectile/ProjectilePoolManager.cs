using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class ProjectilePoolManager : SingletonComponent<ProjectilePoolManager>, IPoolManager
{
    private Dictionary<GameObject, ObjectPool<Object>> _dictionaryPoolProjectiles = new Dictionary<GameObject, ObjectPool<Object>>();
    private string parentObjectName = "_Placeholder";

    public T GetObjectFromPool<T>(T prefabProjectile) where T : MonoBehaviour
    {
        if (!_dictionaryPoolProjectiles.ContainsKey(prefabProjectile.gameObject))
            _dictionaryPoolProjectiles.Add(prefabProjectile.gameObject, new PoolProjectile<T>().InitPool(prefabProjectile, CreateParentForPool(prefabProjectile.name)));

        return _dictionaryPoolProjectiles[prefabProjectile.gameObject].Get() as T;
    }

    public void ReleaseObjectToPool(GameObject prefabProjectileKey, Object objectProjectile)
    {
        if (_dictionaryPoolProjectiles.TryGetValue(prefabProjectileKey, out ObjectPool<Object> value))
            value.Release(objectProjectile);
    }

    public Transform CreateParentForPool(string nameProjectile)
    {
        var parentProjectiles = new GameObject(nameProjectile + parentObjectName);
        parentProjectiles.transform.SetParent(transform);
        return parentProjectiles.transform;
    }
}
