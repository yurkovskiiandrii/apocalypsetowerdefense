using UnityEngine;
using UnityEngine.Pool;
using Projectile;
using Unity.VisualScripting;

public class PoolProjectile<T> : BasePool where T : MonoBehaviour
{
    private Transform _projectilesPlaceholder;
    private T _projectilePrefab;

    public override ObjectPool<Object> InitPool(Object projectilePrefab, Transform parentPool)
    {
        _projectilePrefab = projectilePrefab as T;
        _projectilesPlaceholder = parentPool;

        var projectilesPool = new ObjectPool<Object>(Create, Get, Release);
        return projectilesPool;
    }

    public T Create()
    {
        var newProjectile = MonoBehaviour.Instantiate(_projectilePrefab, _projectilesPlaceholder);
        newProjectile.GetComponent<BaseProjectile>().Init();
        newProjectile.GameObject().SetActive(false);
        return newProjectile;
    }

    public override void Get(Object projectile)
    {
        projectile.GameObject().SetActive(true);
    }

    public override void Release(Object projectile)
    {
        projectile.GameObject().SetActive(false);
    }
}
