using UnityEngine;
using UnityEngine.Pool;

public abstract class BasePool
{
    public abstract ObjectPool<Object> InitPool(Object prefab, Transform parent);

    public abstract void Get(Object objectPool);

    public abstract void Release(Object objectPool);
}
