using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlaceableObject), true)]
public class PlaceableObjectEditor : Editor
{
    private SerializedProperty _widthProperty;
    private SerializedProperty _heightProperty;
    private SerializedProperty _boolArrayProperty;
    private SerializedProperty _isPlacedProperty;
    private SerializedProperty _inContainerProperty;
    private SerializedProperty _cellsPositionsProperty;

    private void OnEnable()
    {
        _widthProperty = serializedObject.FindProperty("width");
        _heightProperty = serializedObject.FindProperty("height");
        _boolArrayProperty = serializedObject.FindProperty("boolArray");
        _isPlacedProperty = serializedObject.FindProperty("isPlaced");
        _inContainerProperty = serializedObject.FindProperty("inContainer");
        _cellsPositionsProperty = serializedObject.FindProperty("cellsPositions");
    }   

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        serializedObject.Update();

        Shape();
        PlaceButton();
        MoveToContainer();
 
        serializedObject.ApplyModifiedProperties();
    }

    private void Shape()
    {
        GUILayout.Space(10f);
        GUILayout.Label("Shape");

        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Width");
            GUILayout.Label("Height");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
            _widthProperty.intValue = EditorGUILayout.IntField(_widthProperty.intValue);
            _heightProperty.intValue = EditorGUILayout.IntField(_heightProperty.intValue);
        EditorGUILayout.EndHorizontal();

        if (_boolArrayProperty.arraySize != _widthProperty.intValue * _heightProperty.intValue)
        {
            _boolArrayProperty.ClearArray();

            for (int j = 0; j < _heightProperty.intValue; j++)
            {
                for (int i = 0; i < _widthProperty.intValue; i++)
                {
                    var index = j * _widthProperty.intValue + i;

                    _boolArrayProperty.InsertArrayElementAtIndex(index);
                    _boolArrayProperty.GetArrayElementAtIndex(index).boolValue = true;
                }
            }
        }
        
        for (int j = _heightProperty.intValue - 1; j != -1; j--)
        {
            EditorGUILayout.BeginHorizontal();
                for (int i = 0; i < _widthProperty.intValue; i++)
                    {
                        var index = j * _widthProperty.intValue + i;
                        var property = _boolArrayProperty.GetArrayElementAtIndex(index);

                        property.boolValue = EditorGUILayout.Toggle(property.boolValue, new GUILayoutOption[]
                        {
                            GUILayout.Width(15),
                        });
                }
            EditorGUILayout.EndHorizontal();
        } 
    }

    private void PlaceButton()
    {
        EditorGUILayout.Space(10f);

        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Placed And Moved To Container"); 
            EditorGUILayout.Toggle(_isPlacedProperty.boolValue && _inContainerProperty.boolValue);
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Place"))
        {
            _isPlacedProperty.boolValue = false;
            _cellsPositionsProperty.ClearArray();

            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();

            var positions = ((PlaceableObject)target).PlaceFromEditor();

            if (positions.Count > 0)
            {
                _isPlacedProperty.boolValue = true;
                _cellsPositionsProperty.ClearArray();

                for (int i = 0; i < positions.Count; i++)
                {
                    _cellsPositionsProperty.InsertArrayElementAtIndex(i);
                    _cellsPositionsProperty.GetArrayElementAtIndex(i).vector2IntValue = positions[i];
                }

                TerrainGrid.Instance.UpdateNavMeshGrid();

                Debug.Log("Successfully Placed");
            }

            else
            {
                Debug.LogError("Not Placed");
            }
        }
    }

    private void MoveToContainer()
    {
        if (GUILayout.Button("Move To Container"))
        {
            var placeableObject = (PlaceableObject)target;
            var container = placeableObject.Type == PlaceableObjectType.Props ? TerrainGrid.Instance.PropsContainer : TerrainGrid.Instance.DecorationContainer;

            if (placeableObject.transform.parent != container)
            {
                placeableObject.transform.SetParent(container);
                _inContainerProperty.boolValue = true;
            }
        }
    }
}
