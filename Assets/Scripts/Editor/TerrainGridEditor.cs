using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGrid))]
public class TerrainGridEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GenerateFieldButton();
    }

    private void GenerateFieldButton()
    {
        GUILayout.Space(10f);

        if (GUILayout.Button("Generate Field"))
        {
            var terrianGrid = (TerrainGrid)target;

            terrianGrid.GenerateFied();
        }
    }
}
