using UnityEngine;

[System.Serializable]

public class GameResourcesIcon
{
    public string nameResource;
    public Sprite resourcesSprite;
}
