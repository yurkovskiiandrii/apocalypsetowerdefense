using System.Collections.Generic;
using UnityEngine;

public class RewardsController : MonoBehaviour
{
    [SerializeField] private LevelManager _levelManager;
    [SerializeField] private LevelRewardsConfig _levelRewardsConfig;
    //[SerializeField] private int _currentLevel;
    private SaveManager _saveManager;

    //public int CurrentLevel { get => _currentLevel; }

    private void Awake()
    {
        _saveManager = SaveManager.GetInstance();
        _saveManager.Init(_levelRewardsConfig);

        //LoadGameResources();
    }

    public (int star, List<GameResource> rewards) GetLevelRewards()
    {
        return _levelRewardsConfig.GetLevelRewards(_levelManager.HealthPoints);
    }

    public bool IsChanceToGetReward(GameResource gameResource)
    {
        return Random.Range(0.0f, 1.0f) < gameResource.GetChance();
    }

    private void LoadGameResources()
    {
        Debug.Log("Load Game Resources");

        //if (_saveManager.GetRewards().Count == 0)
        //    return;

        SpawnItemsGameResources();
    }

    private void SpawnItemsGameResources()
    {
        //foreach (Transform child in _gameResourcesParent)
        //{
        //    Destroy(child.gameObject);
        //}

        //foreach (var item in _saveManager.GetRewards())
        //{
        //    var gameResourcesItem = Instantiate(_gameResourcesPrefab, _gameResourcesParent);
        //    int count = _saveManager.GetResourceCount(item.nameResource);
        //    var sprite = GameResourcesIconsConfig.Instance.GetSprite(item.nameResource);
        //    //gameResourcesItem.GetComponent<...>().SetParameters(count,sprite);
        //}
    }
}
