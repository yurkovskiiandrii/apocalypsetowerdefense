using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class LevelRewards
{
    public int level;
    public List<Star> stars = new List<Star>();

    public int Level { get => level; }

    public (int star, List<GameResource> rewards) GetRewards(int healt)
    {
        int star = GetRewardStars(healt);
        List<GameResource> reward = GetRewardResources(star);
        return (star, reward);
    }

    public int GetRewardStars(int healt)
    {
        int starLevel = 1;
        for (int i = stars.Count - 1; i >= 0; i--)
        {
            if (healt >= stars[i].minHealt)
            {
                starLevel = i + 1;
                return starLevel;
            }
        }
        return starLevel;
    }

    public List<GameResource> GetRewardResources(int star)
    {
        List<GameResource> reward = new List<GameResource>();
        for (int i = 0; i < star; i++)
        {
            foreach (var item in stars[i].reward)
            {
                Debug.Log("item "+ item.nameResource);
                reward.Add(item);
            }
        }
        return reward;
    }
   
    public void ValidateStarName()
    {
        string starName = "Star";

        for (int y = 0; y <= stars.Count - 1; y++)
        {
            if (stars[y].name != null)
            {
                stars[y].name = $"{y + 1} {starName}";
            }
        }
    }

}

[System.Serializable]
public class Star
{
    [HideInInspector]
    public string name;
    public int minHealt;
    public List<GameResource> reward;
}

