using UnityEngine;

[System.Serializable]

public class GameResource
{
    public string nameResource;
    public int value;
    public int count;
    [Range(0.0f, 1.0f)]
    public float chance;

    public int GetCount() => count;

    public int GetValue() => value;

    public float GetChance() => chance;
}
