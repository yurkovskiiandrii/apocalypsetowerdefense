using UnityEngine;

public class RotationLimiter : MonoBehaviour//rotate limiter for UI world space
{
    private Transform _transform;
    //private Quaternion _startRotation;
    private Camera _camera;

    private void Start()
    {
        _transform = transform;
        _camera = Camera.main;
        //_startRotation = _transform.rotation;
    }

    private void LateUpdate()
    {
        //_transform.rotation = _startRotation;
        _transform.LookAt(transform.position + _camera.transform.rotation * Vector3.forward, _camera.transform.rotation * Vector3.up);
    }
}
