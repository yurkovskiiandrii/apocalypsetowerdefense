using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private string _sceneLevel1Name;
    public void Play()
    {
        SceneManager.LoadScene(_sceneLevel1Name);
    }
}
