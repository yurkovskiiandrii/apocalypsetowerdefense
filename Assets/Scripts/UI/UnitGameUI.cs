using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitGameUI : MonoBehaviour
{
    [SerializeField] private GameObject _healthPointsBar;
    [SerializeField] private TMP_Text _timerText;
    [SerializeField] protected Slider _hpSlider;
   
    public TMP_Text GetTimerText() => _timerText;

    public Slider GetSliderHpBar() => _hpSlider;

    public void ActivateHealthPointsBar(bool isActive)
    {
        _healthPointsBar.SetActive(isActive);
    }
}
