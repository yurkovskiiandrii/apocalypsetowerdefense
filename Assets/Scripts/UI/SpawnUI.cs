using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Units;


public class SpawnUI : MonoBehaviour
{
    [SerializeField] private LevelUI _levelUI;

    [SerializeField] private int _iconsCount = 16;
    [SerializeField] private PlaceIconUI _placeIconPrefab;
    [SerializeField] private UnitIconUI _unitIconPrefab;
    [SerializeField] private PropsIconUI _propsIconPrefab;
    [SerializeField] private Sprite _defaultSprite;

    [SerializeField] private DefendersSpawnManager _spawnManager;
    [SerializeField] private Transform _propsParent;
    [SerializeField] private GridLayoutGroup _defendersGrid;
    [SerializeField] private GameObject _terrain;

    [SerializeField] private List<Unit> _units;// temp list in future
    [SerializeField] private List<Prop> _props;// temp from game in future

    private UnityAction<int> onDefenderSpawn;

    private PlaceIconUI _activeIcon;
    private PlaceableObject _activeObject;
    private LevelEvents _levelEvents;

    public void Init(int resources, UnityAction<int> OnDefenderSpawn)
    {
        onDefenderSpawn += OnDefenderSpawn;
        _spawnManager.Init(StopSpawn);

        _levelEvents = LevelEvents.Instance;
        _levelEvents.SubscribeOnPlaceEnd(StopSpawn);
        _levelEvents.SubscribeOnPlaceCancel(StopSpawn);

        for (int i = 0; i < _iconsCount; i++)
        {
            if (i < _units.Count)
            {
                UnitIconUI placeIconUI = Instantiate(_unitIconPrefab, _defendersGrid.transform); /*_defendersGrid.transform.GetChild(i).GetComponent<UnitIconUI>();*/
                placeIconUI.Init(_units[i], StartSpawnUnit, value => _levelUI.ShowWaveIsInProgressTextPanel(value));
                placeIconUI.CheckPrice(resources);
            }
            else if (i >= _units.Count && i < _units.Count + _props.Count)
            {
                PropsIconUI placeIconUI = Instantiate(_propsIconPrefab, _defendersGrid.transform);
                placeIconUI.Init(_props[i - _units.Count], StartSpawnProps, value => _levelUI.ShowWaveIsInProgressTextPanel(value));
                placeIconUI.CheckPrice(resources);
            }
            else
            {
                PlaceIconUI placeIconUI = Instantiate(_placeIconPrefab, _defendersGrid.transform);
                placeIconUI.InitDefaultIcon(_defaultSprite);
            }
        }
        /*foreach (Transform child in _defendersGrid.transform)
        {
            UnitIconUI placeIconUI = child.GetComponent<UnitIconUI>();
            placeIconUI.Init(_unit, StartSpawnUnit);
            placeIconUI.CheckPrice(resources);
        }*/
    }

    public void CheckPrices(int currentResources)
    {
        foreach (Transform child in _defendersGrid.transform)
        {
            if (child.gameObject.activeSelf)
            {
                if (child.TryGetComponent(out UnitIconUI unitIconUI))
                    unitIconUI.CheckPrice(currentResources);
                if (child.TryGetComponent(out PropsIconUI propsIconUI))
                    propsIconUI.CheckPrice(currentResources);
            }
        }
    }

    private void StartSpawnUnit(Unit prefab, UnitIconUI unitIcon)
    {
        StartSpawn();

        _spawnManager.SpawnDefender(prefab);
        _activeIcon = unitIcon;
    }

    private void StartSpawnProps(Prop prefab, PropsIconUI unitIcon)
    {
        StartSpawn();

        _activeObject = TerrainGrid.Instance.SpawnObject(prefab.gameObject, _propsParent, onDefenderSpawn, StopSpawn);
        _activeIcon = unitIcon;
    }

    private void StartSpawn()
    {
        if (_spawnManager.IsActiveUnitOrProps())
        { _spawnManager.CancelPlacing(); }

        _spawnManager.SetActiveUnitOrProps(true);

        if (_activeIcon != null)
            StopCurrentSpawn();
    }

    public void StopCurrentSpawn()
    {
        //_spawnManager.CancelPlacing();

        if (_activeIcon != null)
        {
            _activeIcon.SetActiveFrame(false);
            _activeIcon = null;
        }

        if (_activeObject != null)
        {
            _activeObject.CancelPlacing();
            _activeObject = null;
        }
    }

    private void StopSpawn()
    {
        Debug.Log(" StopSpawn()");
        if (_activeIcon != null)
        {
            _activeIcon.SetActiveFrame(false);
            _activeIcon = null;
            _activeObject = null;
        }
    }
}
