using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using HeroSkills;
using DG.Tweening;
using UnityEngine.Events;

public class HeroUILabelPref : MonoBehaviour
{
    [Header("Hero settings")]
    [SerializeField] private Image _imageHero;
    [SerializeField] private Button _activateHeroButton;
    [SerializeField] private GameObject _imageHeroClick;
    [SerializeField] private Image _timerProgressBarHero;
    [SerializeField] private TextMeshProUGUI _recoveryTextHero;

    [Header("Skill settings")]
    [SerializeField] private Image _imageSkill;
    [SerializeField] private Button _activateSkillButton;
    [SerializeField] private GameObject _imageSkillClick;
    [SerializeField] private Image _timerProgressBarSkill;
    [SerializeField] private TextMeshProUGUI _rechargeTextSkill;


    private UnityAction<HeroUILabelPref> _onIconHeroClick;
    private Action _onHeroSelected;
    private Action _onHeroNotSelected;
    private Action _onHeroRecovered;
    private Action _onSkillStarted;
    private Action _onSkillEnded;
    private Action _onSkillStopped;
    private ActiveSkill _heroSkill;
    private float _coolDownTime = 0;
    private Tweener _tweenerTimerBrogressBar;
    private Tweener _tweenerTimerText;
    private HeroesManager _heroesManager;
    private LevelEvents _levelEvents;

    public int HeroNumber { get; set; }

    public void InitPassiveSkill(Sprite iconSkill)
    {
        _imageSkill.sprite = iconSkill;
        _activateSkillButton.enabled = false;
    }

    public void InitActiveSkill(Sprite iconSkill, float coolDownTime, Action onSkillStarted, Action onSkillEnded, Action onSkillStopped)
    {
        _imageSkill.sprite = iconSkill;
        _coolDownTime = coolDownTime;

        _onSkillStarted = onSkillStarted;
        _onSkillEnded = onSkillEnded;
        _onSkillStopped = onSkillStopped;

        _activateSkillButton.enabled = true;
        _activateSkillButton.onClick.AddListener(ActivateSkill);
    }

    public void InitHeroSettigs(Sprite iconHero, int heroNumber, Action onHeroSelected, Action onHeroNotSelected)
    {
        _onHeroSelected = onHeroSelected;
        _onHeroNotSelected = onHeroNotSelected;
        _heroesManager = HeroesManager.Instance;
        _imageHero.sprite = iconHero;

        HeroNumber = heroNumber;
        _activateHeroButton.onClick.AddListener(ActivateHero);

        _levelEvents = LevelEvents.Instance;
        _levelEvents.SubscribeOnPlaceEnd(DeActivateHero);
        _levelEvents.SubscribeOnPlaceCancel(DeActivateHero);
    }

    public void SetActionOnClickIconOfHero(UnityAction<HeroUILabelPref> onIconHeroClick)
    {
        _onIconHeroClick = onIconHeroClick;
    }

    public void SetActionOnHeroRecovered(Action OnHeroRecovered)
    {
        _onHeroRecovered = OnHeroRecovered;
    }

    //Skill mettods
    [ContextMenu("Test Skill")]
    private void ActivateSkill()//button Hero Skill
    {
        //Debug.Log("UILabel ActivateSkil Click ");
        _onSkillStarted?.Invoke();
    }

    public void StartSkill()
    {
        if (_heroesManager.IsActiveUnitOrProps())
        {
            _heroesManager.DeactiveCurrentHeroInScene();
            DeActivateHero();
        }

        _heroesManager.IsActiveSkill = true;
        _heroesManager.IndexActiveHero = HeroNumber;
        _imageSkillClick.SetActive(true);
    }

    public void SkillEnd()
    {
        _imageSkillClick.SetActive(false);
        _onSkillEnded?.Invoke();
    }

    public void SkillStop()
    {
        _imageSkillClick.SetActive(false);
        _onSkillStopped?.Invoke();
        _heroesManager.IsActiveSkill = false;
    }

    public void StartSkillTimersCoroutines()
    {
        StartTimersCoroutines(_coolDownTime, _rechargeTextSkill, _timerProgressBarSkill, SkillEnd);
    }

    public void StopSkillsCoroutine()
    {
        _rechargeTextSkill.gameObject.SetActive(false);
        _timerProgressBarSkill.gameObject.SetActive(false);
        _imageSkillClick.SetActive(false);

        _tweenerTimerBrogressBar.Kill();
        _tweenerTimerText.Kill();

        //Debug.Log("StopSkillsCoroutine()    tweener.DOKill();");
        _heroesManager.IsActiveSkill = false;
    }

    //Hero metods
    public void ActivateHero()//button Hero
    {
        if (_heroesManager.IsActiveSkill)
        {
            _heroesManager.StopActiveSkill();
        }
        _onIconHeroClick?.Invoke(this);
    }

    public void DeActivateHero()//event when clicking on another hero or click skill
    {        
        DeActivateHeroClickImage();
    }

    public void ActivateHeroClickImage()
    {
        _imageHeroClick.SetActive(true);
        _onHeroSelected?.Invoke();
    }

    public void DeActivateHeroClickImage()
    {
        _imageHeroClick.SetActive(false);
        _onHeroNotSelected?.Invoke();
    }

    public void HeroDead()
    {
        StopSkillsCoroutine();
        DeActivateHero();
    }

    public void StartTimersCoroutinesHeroDead()
    {
        StartTimersCoroutines(RecoveryController.RecoveryTime, _recoveryTextHero, _timerProgressBarHero, HeroRecovered);
    }

    private void HeroRecovered()
    {
        _onHeroRecovered?.Invoke();
    }

    private void ActivateButtons(bool isActive)
    {
        _activateHeroButton.enabled = isActive;
        _activateSkillButton.enabled = isActive;
    }

    private void StartTimersCoroutines(float time, TextMeshProUGUI text, Image progressBar, Action timerEnded)
    {
        _tweenerTimerText = DOVirtual.Int((int)time, 0, time, (int value) => text.text = (value).ToString()).SetEase(Ease.Linear);
        _tweenerTimerText.OnStart<Tweener>(() => { text.text = time.ToString(); text.gameObject.SetActive(true); });
        _tweenerTimerText.onComplete += () => { timerEnded?.Invoke(); text.gameObject.SetActive(false); };

        _tweenerTimerBrogressBar = DOVirtual.Float(1, 0, time, (float value) => progressBar.fillAmount = value).SetEase(Ease.Linear);
        _tweenerTimerBrogressBar.OnStart<Tweener>(() => { progressBar.fillAmount = 1; progressBar.gameObject.SetActive(true); });
        _tweenerTimerBrogressBar.onComplete += () => { progressBar.gameObject.SetActive(false); };
    }
}
