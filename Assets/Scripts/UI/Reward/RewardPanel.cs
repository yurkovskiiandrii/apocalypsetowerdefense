using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class RewardPanel : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private RewardsController _rewardsController;
    [SerializeField] private string _sceneMainMenuName;
    [Header("Chest parameters")]
    [SerializeField] private GameObject _chest;
    [SerializeField] private float _chestDelay = 2.0f;
    [SerializeField] private Transform _chestEndPoint;
    [SerializeField] private Vector3 _endScaleChest = new Vector3(0.25f, 0.25f, 0.25f);
    [SerializeField] private string _startChestState;
    [SerializeField] private string _changeScaleState;
    [SerializeField] private string _moveChestState;
    [SerializeField] private UnityEvent _onShowingNewReward;
    [Header("Resources parameters")]
    [SerializeField] private GameObject _gameResourcesPrefab;
    [SerializeField] private Transform _gameResourcesSpawnParent;
    [SerializeField] private Transform _allRewardsParent;
    [SerializeField] private Transform _resourcesStartPoint;
    [SerializeField] private Transform _resourcesEndPoint;
    [SerializeField] private Vector3 _startScaleResources = new Vector3(0.1f, 0.1f, 0.1f);
    [SerializeField] private Vector3 _endScaleResources = new Vector3(1.5f, 1.5f, 1.5f);
    [Header("Stars Rewards")]
    [SerializeField] private float _showStarDelay = 0.5f;
    [SerializeField] private List<GameObject> _stars;

    private GameResourcesIconsConfig _gameResourcesIconsConfig;
    private Animator _chestAnimator;
    private SaveManager _saveManager;
    private List<GameResource> _levelRewards;
    private List<GameResource> _levelRewardsReceived;
    private Coroutine _showChestTimerCoroutine;
    private WaitForSecondsRealtime _chestWaitTime;
    private WaitForSecondsRealtime _showResourcesWaitTime;
    private WaitForSecondsRealtime _showAllRewardsWaitTime;
    private WaitForSecondsRealtime _showStarWaitTime;
    private float _durationDOTweenMoveAndScale = 1.0f;
    private float _showAllRewardsDelay = 0.5f;
    private bool _isShowChest = false;
    private bool _isShowReward = false;
    private int _star;

    private void Start()
    {
        _saveManager = SaveManager.GetInstance();
        _gameResourcesIconsConfig = GameResourcesIconsConfig.Instance;
        _chestAnimator = _chest.GetComponent<Animator>();
        _chestWaitTime = new WaitForSecondsRealtime(_chestDelay);
        _showResourcesWaitTime = new WaitForSecondsRealtime(_durationDOTweenMoveAndScale);
        _showAllRewardsWaitTime = new WaitForSecondsRealtime(_showAllRewardsDelay);
        _showStarWaitTime = new WaitForSecondsRealtime(_showStarDelay);

        var resultLevel = _rewardsController.GetLevelRewards();
        _star = resultLevel.star;
        _levelRewards = resultLevel.rewards;
        _levelRewardsReceived = new List<GameResource>();
        Debug.Log("Your WIN is " + _star + " STARS");

        StartCoroutine(ShowResultStars(_star));
        ShowReward();
    }

    private IEnumerator ShowResultStars(int star)
    {
        yield return _showResourcesWaitTime;
        for (int i = 0; i < star; i++)
        {
            _stars[i].SetActive(true);
            yield return _showStarWaitTime;
        }
    }
    private void ShowReward()
    {
        _chest.SetActive(true);
        _chestAnimator.SetTrigger(_startChestState);
        _showChestTimerCoroutine = StartCoroutine(ShowChestTimer());
    }

    private IEnumerator ShowChestTimer()
    {
        yield return _chestWaitTime;
        StartAnimationChest();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_isShowReward)
        {
            LoadMainMenu();
            return;
        }

        if (_isShowChest)
            return;
        StopTimerCoroutine(_showChestTimerCoroutine);
        StartAnimationChest();
    }

    private void StartAnimationChest()
    {
        _isShowChest = true;
        StartAnimationMoveAndScale(_chest, _chestEndPoint.position, _endScaleChest, () => { _chestAnimator.SetTrigger(_moveChestState); }, () => { HideChest(); });
    }

    private void HideChest()
    {
        _chestAnimator.SetTrigger(_changeScaleState);
        StartCoroutine(SpawnIconRewars());
    }

    private IEnumerator SpawnIconRewars()
    {
        yield return null;

        for (int i = 0; i < _levelRewards.Count; i++)
        {
            var resource = _levelRewards[i];
            var isLast = i == _levelRewards.Count - 1;

            if (!_rewardsController.IsChanceToGetReward(resource))
            {
                Debug.Log(" Reward \"" + resource.nameResource + "\" not received !");
                if (isLast)
                    StartCoroutine(ShowAllRewars());
                continue;
            }
            Debug.Log("Congratulations! Reward \"" + resource.nameResource + "\" received .");
           
            _onShowingNewReward?.Invoke();

            _levelRewardsReceived.Add(resource);
            var gameResourcesItem = Instantiate(_gameResourcesPrefab, _gameResourcesSpawnParent);
            gameResourcesItem.transform.position = _resourcesStartPoint.position;
            gameResourcesItem.transform.localScale = _startScaleResources;

            int count = resource.GetCount();
            var sprite = _gameResourcesIconsConfig.GetSprite(resource.nameResource);
            gameResourcesItem.GetComponent<RewardItem>().Init(sprite, count);

            _saveManager.SetResourceCount(resource.nameResource, count);

            StartAnimationMoveAndScale(gameResourcesItem, _resourcesEndPoint.position, _endScaleResources,
                () => { }, () => { },
                delegate { HideResources(gameResourcesItem, isLast); });

            yield return _showResourcesWaitTime;
        }
    }

    private void StartAnimationMoveAndScale(GameObject gameObject, Vector3 endPoint, Vector3 endScaleResources, Action timerStart, Action timerEnded1, Action<GameObject, bool> timerEnded2 = null, bool isLastElement = false)
    {
        var tweenerMove = DOTween.Sequence().SetUpdate(true);
        tweenerMove.Append(gameObject.transform.DOMove(endPoint, _durationDOTweenMoveAndScale))
             .Insert(0, gameObject.transform.DOScale(endScaleResources, tweenerMove.Duration()));

        tweenerMove.OnStart(() => { timerStart?.Invoke(); });
        tweenerMove.OnComplete(() => { timerEnded1?.Invoke(); timerEnded2?.Invoke(gameObject, isLastElement); });
    }

    private void HideResources(GameObject resources, bool isLastElement)
    {
        resources.SetActive(false);

        if (isLastElement)
            StartCoroutine(ShowAllRewars());
    }

    private IEnumerator ShowAllRewars()
    {
        List<GameResource> allRewards = new List<GameResource>();

        for (int i = 0; i < _levelRewardsReceived.Count; i++)
        {
            var resources = _levelRewardsReceived[i];
            if (allRewards.Exists(x => x.nameResource == resources.nameResource))
                continue;

            allRewards.Add(resources);
            yield return _showAllRewardsWaitTime;
            int count = GetCountResources(_levelRewardsReceived, resources.nameResource);
            var sprite = _gameResourcesIconsConfig.GetSprite(resources.nameResource);

            var gameResourcesItem = Instantiate(_gameResourcesPrefab, _allRewardsParent);
            gameResourcesItem.GetComponent<RewardItem>().Init(sprite, count);

            var canvasGroup = gameResourcesItem.GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0f;
            DOVirtual.Float(0, 1, _showAllRewardsDelay, (float value) => canvasGroup.alpha = value).SetEase(Ease.Linear).SetUpdate(true);
        }

        _isShowReward = true;
    }
    public int GetCountResources(List<GameResource> rewards, string name)
    {
        var count = 0;

        for (int i = 0; i < rewards.Count; i++)
        {
            if (rewards[i].nameResource == name)
            {
                count += rewards[i].GetCount();
            }
        }
        return count;
    }


    public void LoadMainMenu()
    {
        SceneManager.LoadScene(_sceneMainMenuName);
    }

    private void StopTimerCoroutine(Coroutine coroutine)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
            coroutine = null;
        }
    }
}
