using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardItem : MonoBehaviour
{
    [SerializeField] private Image _icon;
    [SerializeField] private TMP_Text _count;
    [SerializeField] private string _startState;

    public void Init(Sprite resourcesSprite, int resourcesCount)
    {
        _icon.sprite = resourcesSprite;
        _count.text = resourcesCount.ToString();
    }
}
