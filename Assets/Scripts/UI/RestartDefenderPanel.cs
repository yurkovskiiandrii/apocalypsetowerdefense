using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RestartDefenderPanel : MonoBehaviour
{
    [SerializeField] private TMP_Text _unitNameText;
    [SerializeField] private TMP_Text _currentProgressNumberText;
    [SerializeField] private TMP_Text _timeText;
    [SerializeField] private GameObject _noMoneyPanel;
    [SerializeField] private Button _reviveButtonHaveMoney;
    [SerializeField] private Button _reviveButtonNoMoney;
    [SerializeField] private TMP_Text _priceTextHaveMoney;
    [SerializeField] private TMP_Text _priceTextNoMoney;

    private UpgradeManager _upgradeManager;
    private Action _onBuingRecovery;
    private float _timeToShowNoMoneyPanel = 0.5f;
    private string _currentProgressNumberTextFormat;
    private string _timeTextFormat;
    private int _price;

    void Start()
    {
        LevelEvents.Instance.SubscribeOnUpdateMoney(UpdateInfoButton);
        _reviveButtonHaveMoney.onClick.AddListener(StartRecovery);
        _reviveButtonNoMoney.onClick.AddListener(ShowNoMoneyPanel);
    }

    private void StartRecovery()
    {
        _onBuingRecovery?.Invoke();
        _upgradeManager.UpdateResourses(_price);
        gameObject.SetActive(false);
    }

    private void ShowNoMoneyPanel()
    {
        _noMoneyPanel.SetActive(true);
        Invoke(nameof(CloseNoMoneyPanel), _timeToShowNoMoneyPanel);
    }

    public void Init(string nameUnit, int currentIndexUpgrade, float recoveryTime, int price, Action onBuingRecovery)
    {
        _currentProgressNumberTextFormat = _currentProgressNumberText.text;
        _timeTextFormat = _timeText.text;

        _upgradeManager = UpgradeManager.Instance;
        _currentProgressNumberText.text = string.Format(_currentProgressNumberTextFormat, (currentIndexUpgrade + 1));
        _unitNameText.text = nameUnit;
        _timeText.text = string.Format(_timeTextFormat, recoveryTime);
        _onBuingRecovery = onBuingRecovery;
        _price = price;
        UpdateInfoButton();
    }

    private void UpdateInfoButton()
    {
        _reviveButtonHaveMoney.gameObject.SetActive(_upgradeManager.IsEnoughMoney(_price));
        _reviveButtonNoMoney.gameObject.SetActive(!_upgradeManager.IsEnoughMoney(_price));

        _priceTextHaveMoney.text = _price.ToString();
        _priceTextHaveMoney.gameObject.SetActive(_upgradeManager.IsEnoughMoney(_price));
        _priceTextNoMoney.text = _price.ToString();
        _priceTextNoMoney.gameObject.SetActive(!_upgradeManager.IsEnoughMoney(_price));
    }

    private void CloseNoMoneyPanel()
    {
        _noMoneyPanel.SetActive(false);
    }
}
