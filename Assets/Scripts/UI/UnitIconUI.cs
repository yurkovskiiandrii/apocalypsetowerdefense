using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Units;

public class UnitIconUI : PlaceIconUI, IPointerDownHandler
{
    private Unit _unitPrefab;
    private UnityAction<Unit, UnitIconUI> _onIconClick;

    public void Init(Unit unitPrefab, UnityAction<Unit, UnitIconUI> onIconClick, UnityAction<Transform> onNoClickEvent)
    {
        base.Init(onNoClickEvent);
        _unitPrefab = unitPrefab;
        _objectImage.sprite = _unitPrefab.DefenderOptions.iconSprite;
        _value.text = unitPrefab.DefenderOptions.cost.ToString();
        _onIconClick += onIconClick;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);

        if (_isEnabled)
        {
            if (!WavesManager.IsStartedWaitingTimeForWave)
            {
                _onNoClickEvent?.Invoke(_transform);
                return;
            }
            _onIconClick?.Invoke(_unitPrefab, this);
            SetActiveFrame(true);
        }
    }

    public void CheckPrice(int currentResources)
    {
        if (currentResources >= _unitPrefab.DefenderOptions.cost)
        {
            _isEnabled = true;
            _objectImage.color = _normalFrameColor;
            _value.color = _normalValueColor;
            return;
        }

        _isEnabled = false;
        _objectImage.color = _disabledFrameColor;
        _value.color = _disabledValueColor;
    }
}
