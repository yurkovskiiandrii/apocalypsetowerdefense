using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public static class MouseDetectorAboveUI
{
    public static bool IsMouseOverUI(Vector3 mousePosition)
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = mousePosition;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, raycastResults);

        if (raycastResults.Count > 0)
        {
            foreach (RaycastResult result in raycastResults)
            {
                if (result.gameObject.TryGetComponent(out CanvasGroup canvasGroup))
                {
                    //Debug.Log("Hit Is Mouse Over UI / " + result.gameObject.name);
                    return true;
                }
            }
            //Debug.Log("Hit Is NOT Mouse Over UI: ");
        }
        return false;
    }
}
