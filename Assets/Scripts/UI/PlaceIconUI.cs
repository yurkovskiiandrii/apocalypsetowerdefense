using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlaceIconUI : MonoBehaviour
{
    [SerializeField] protected TMP_Text _value;
    [SerializeField] protected Image _objectImage;
    [SerializeField] protected Image _frameImage;

    [Header("Frame Color")]
    [SerializeField] protected Color _normalFrameColor;
    [SerializeField] protected Color _disabledFrameColor;

    [Header("Value Color")]
    [SerializeField] protected Color _normalValueColor;
    [SerializeField] protected Color _disabledValueColor;

    //protected Func<bool> _hasClick;
    protected UnityAction<Transform> _onNoClickEvent;
    protected bool _isEnabled = false;
    protected Transform _transform;

    public void Init(UnityAction<Transform> onNoClickEvent, Sprite sprite = null)
    {
        if (sprite != null)
            _objectImage.sprite = sprite;

        _transform = transform;
        _isEnabled = true;
        SetActiveFrame(false);

        _onNoClickEvent = onNoClickEvent;
    }

    public void InitDefaultIcon(Sprite sprite = null)
    {
        if (sprite != null)
            _objectImage.sprite = sprite;

        SetActiveFrame(false);
    }
    public virtual void OnPointerDown(PointerEventData eventData)
    {
        HeroesManager heroesManager = HeroesManager.Instance;
        if (heroesManager.IsActiveSkill)
        {
            heroesManager.StopActiveSkill();
        }
    }

    public void SetActiveFrame(bool isActive)
    {
        _frameImage.gameObject.SetActive(isActive);
    }

    protected void EnabledFrameColor(bool isEnabled)
    {
        _isEnabled = isEnabled;
        _objectImage.color = isEnabled ? _normalFrameColor : _disabledFrameColor;
        _value.color = isEnabled ? _normalValueColor : _disabledValueColor;
    }
}
