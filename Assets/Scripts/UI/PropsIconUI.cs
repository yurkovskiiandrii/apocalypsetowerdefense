using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PropsIconUI : PlaceIconUI, IPointerDownHandler
{
    private Prop _propPrefab;
    private UnityAction<Prop, PropsIconUI> _onIconClick;

    public void Init(Prop propPrefab, UnityAction<Prop, PropsIconUI> onIconClick, UnityAction<Transform> onNoClickEvent)
    {
        base.Init(onNoClickEvent);
        _propPrefab = propPrefab;
        _objectImage.sprite = _propPrefab.DefenderOptions.iconSprite;
        _value.text = propPrefab.DefenderOptions.cost.ToString();
        _onIconClick += onIconClick;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);

        if (_isEnabled)
        {
            if (!WavesManager.IsStartedWaitingTimeForWave)
            {
                _onNoClickEvent?.Invoke(_transform);
                return;
            }
            _onIconClick?.Invoke(_propPrefab, this);
            SetActiveFrame(true);
        }
    }

    public void CheckPrice(int currentResources)
    {
        if (currentResources >= _propPrefab.DefenderOptions.cost)
        {
            EnabledFrameColor(true);
            return;
        }

        EnabledFrameColor(false);
    }
}
