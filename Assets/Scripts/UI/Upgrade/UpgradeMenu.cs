using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour
{
    [Header("Upgrade Button /Touch Menu")]
    [SerializeField] private bool _isTouchMenuPlacingAndUpgrade;
    [SerializeField] private Button _upgradeButtonHaveMoney;
    [SerializeField] private Button _upgradeButtonNoMoney;
    [SerializeField] private TMP_Text _priceUpgradeTextHaveMoney;
    [SerializeField] private TMP_Text _priceUpgradeTextNoMoney;
    [Header("Upgrade Menu")]
    [SerializeField] private GameObject _upgradeInfoPanel;
    [SerializeField] private GameObject _buttonPanel;
    [SerializeField] private GameObject _maxUpgradeText;
    [SerializeField] protected TMP_Text _nameText;
    [SerializeField] private TMP_Text _progressNumberText;
    [SerializeField] private Button _promoteButtonHaveMoney;
    [SerializeField] private Button _promoteButtonNoMoney;
    [SerializeField] private TMP_Text _priceTextHaveMoney;
    [SerializeField] private TMP_Text _priceTextNoMoney;
    //[SerializeField] private Sprite _resoucesSprite;
    //[SerializeField] private Image _resoucesImage;
    //[SerializeField] private Sprite _promoteYesSprite;
    //[SerializeField] private Sprite _promoteNoSprite;
    //[SerializeField] private Button _promoteButton;

    protected LevelProgression nextProgress;
    private UpgradeManager _upgradeManager;
    private LevelEvents _levelEvents;
    protected LevelProgressionConfig _currentConfig;
    private int _price;
    private string _lvlText = "Lvl ";
    private string _progressTextFormat;

    protected virtual void Start()
    {
        _levelEvents = LevelEvents.Instance;
        _levelEvents.SubscribeOnUpdateMoney(() =>
        {
            if (_currentConfig)
            {
                if (_isTouchMenuPlacingAndUpgrade)
                    UpdateUpgradeButtonOnTouchMenu();
                UpdatePromoteButton();
            }
        });

        //_resoucesImage.sprite = _resoucesSprite;
        _promoteButtonHaveMoney.onClick.AddListener(Upgrade);

        if (_isTouchMenuPlacingAndUpgrade)
        {
            _upgradeButtonHaveMoney.onClick.AddListener(ToggleUpgradeMenu);
            _upgradeButtonNoMoney.onClick.AddListener(ToggleUpgradeMenu);
        }
    }

    public virtual void Init(LevelProgressionConfig levelProgressionConfig)
    {
        _upgradeManager = UpgradeManager.Instance;
        _progressTextFormat = _progressNumberText.text;

        _currentConfig = levelProgressionConfig;
        _nameText.text = _currentConfig.GetUnitName;

        if (_isTouchMenuPlacingAndUpgrade)
        {
            _upgradeInfoPanel.SetActive(false);
            UpdateUpgradeButtonOnTouchMenu();
            return;
        }
        OpenUpgradeMenu();
    }

    protected virtual void OpenUpgradeMenu()
    {
        _upgradeInfoPanel.SetActive(true);
        UpdatePromoteButton();
        SetNextUpgradeParameters();
    }

    private void UpdateUpgradeButtonOnTouchMenu()
    {
        _price = _currentConfig.GetNextUpgradePriceCoins();
        var isEnoughMoney = _upgradeManager.IsEnoughMoney(_price);

        _upgradeButtonHaveMoney.gameObject.SetActive(isEnoughMoney);
        _upgradeButtonNoMoney.gameObject.SetActive(!isEnoughMoney);

        _priceUpgradeTextHaveMoney.text = _price.ToString();
        _priceUpgradeTextHaveMoney.gameObject.SetActive(isEnoughMoney);
        _priceUpgradeTextNoMoney.text = _price.ToString();
        _priceUpgradeTextNoMoney.gameObject.SetActive(!isEnoughMoney);
    }

    protected virtual void UpdatePromoteButton()
    {
        _price = _currentConfig.GetNextUpgradePriceCoins();
        var isEnoughMoney = _upgradeManager.IsEnoughMoney(_price);
        //_promoteButton.GetComponent<Image>().sprite = _levelManager.IsEnoughMoney(_price) ? _promoteYesSprite : _promoteNoSprite;

        _promoteButtonHaveMoney.gameObject.SetActive(isEnoughMoney);
        _promoteButtonNoMoney.gameObject.SetActive(!isEnoughMoney);

        _priceTextHaveMoney.text = _price.ToString();
        _priceTextHaveMoney.gameObject.SetActive(isEnoughMoney);
        _priceTextNoMoney.text = _price.ToString();
        _priceTextNoMoney.gameObject.SetActive(!isEnoughMoney);

        var canBeUpgraded = _upgradeManager.CanBeUpgraded(_currentConfig);
        _buttonPanel.SetActive(canBeUpgraded);
        _maxUpgradeText.SetActive(!canBeUpgraded);
    }

    protected virtual void SetNextUpgradeParameters()
    {
        var currentUpgradeIndex = _currentConfig.GetCurrentUpgradeIndex();

        if (_upgradeManager.CanBeUpgraded(_currentConfig))
            _progressNumberText.text = string.Format(_progressTextFormat, (currentUpgradeIndex + 1).ToString(), (_currentConfig.NextIndex() + 1).ToString());
        else
            _progressNumberText.text = _lvlText + (currentUpgradeIndex + 1).ToString();
        
        nextProgress = _currentConfig.GetNextProgress();
    }

    protected virtual void Upgrade()
    {
        if (!_upgradeManager.IsEnoughMoney(_price))
            return;

        _upgradeManager.UpgradeForCoins(_price, nextProgress);
        SetNextUpgradeParameters();

        if (_isTouchMenuPlacingAndUpgrade)
            UpdateUpgradeButtonOnTouchMenu();

        UpdatePromoteButton();
        Debug.Log("UpgradeIndex = " + _currentConfig.GetCurrentUpgradeIndex() + gameObject.name);
    }

    public void CloseUpgradeMenu()
    {
        _upgradeInfoPanel.SetActive(false);
    }

    private void ToggleUpgradeMenu()
    {
        if (!_upgradeInfoPanel.activeInHierarchy)
        {
            OpenUpgradeMenu();
        }
        else
            CloseUpgradeMenu();
    }
}
