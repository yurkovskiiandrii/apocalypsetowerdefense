using TMPro;
using UnityEngine;
using Units;
public class DefenderUpgradeMenu : UpgradeMenu
{
    [Header("Upgrade Parameters")]
    [SerializeField] private TMP_Text _attackDamageText;
    [SerializeField] private TMP_Text _attackSpeedText;
    [SerializeField] private TMP_Text _attackRangeText;
    [SerializeField] private TMP_Text _healthPointsText;
    [SerializeField] private TMP_Text _armorText;
    [SerializeField] private TMP_Text _speedByCellText;

    private Unit _unit;

    protected override void Start()
    {
        base.Start();
    }

    public void Init(Unit unit)
    {
        base.Init(unit.UpgradeConfig);

        _unit = unit;
        _unit.SubscribeOnDeath(CloseUpgradeMenu);
    }

    protected override void OpenUpgradeMenu()
    {
        base.OpenUpgradeMenu();
    }

    protected override void UpdatePromoteButton()
    {
        base.UpdatePromoteButton();
    }

    protected override void SetNextUpgradeParameters()
    {
        base.SetNextUpgradeParameters();

        var attackCooldown = nextProgress.AttackCooldown;
        var attackSpeed = RoundUpToTwoDecimalPlaces(1 / attackCooldown);
        _attackSpeedText.text = attackSpeed.ToString();
        _attackDamageText.text = nextProgress.Damage.ToString();
        _attackRangeText.text = nextProgress.AttackRange.ToString();
        _healthPointsText.text = nextProgress.MaxHp.ToString();
        _armorText.text = nextProgress.Armor.ToString();
        _speedByCellText.text = nextProgress.Speed.ToString();
    }

    float RoundUpToTwoDecimalPlaces(float number) => Mathf.Ceil(number * 100) / 100;

    protected override void Upgrade()
    {
        if (!_unit.IsInit)
            return;

        base.Upgrade();
    }
}
