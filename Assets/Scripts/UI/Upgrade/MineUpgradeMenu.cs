using TMPro;
using UnityEngine;

public class MineUpgradeMenu : UpgradeMenu
{
    [Header("Upgrade Parameters")]
    [SerializeField] private TMP_Text _attackDamageText;
    [SerializeField] private TMP_Text _activationTimeText;
    [SerializeField] private TMP_Text _detonationTimeText;

    protected override void Start()
    {
        base.Start();
    }

    public override void Init(LevelProgressionConfig levelProgressionConfig)
    {
        base.Init(levelProgressionConfig);
    }

    protected override void OpenUpgradeMenu()
    {
        base.OpenUpgradeMenu();
    }

    protected override void UpdatePromoteButton()
    {
        base.UpdatePromoteButton();
    }

    protected override void SetNextUpgradeParameters()
    {
        base.SetNextUpgradeParameters();

        _attackDamageText.text = nextProgress.Damage.ToString();
        _activationTimeText.text = nextProgress.ActivationTime.ToString();
        _detonationTimeText.text = nextProgress.DetonationTime.ToString();
    }

    protected override void Upgrade()
    {
        base.Upgrade();
    }
}
