using TMPro;
using UnityEngine;

public class PropUpgradeMenu : UpgradeMenu
{
    [Header("Upgrade Parameters")]
    [SerializeField] private TMP_Text _healthPointsText;
    [SerializeField] private TMP_Text _armorText;

    protected override void Start()
    {
        base.Start();
    }

    public override void Init(LevelProgressionConfig levelProgressionConfig)
    {
        base.Init(levelProgressionConfig);
    }

    protected override void OpenUpgradeMenu()
    {
        base.OpenUpgradeMenu();
    }

    protected override void UpdatePromoteButton()
    {
        base.UpdatePromoteButton();
    }

    protected override void SetNextUpgradeParameters()
    {
        base.SetNextUpgradeParameters();

        _healthPointsText.text = nextProgress.MaxHp.ToString();
        _armorText.text = nextProgress.Armor.ToString();
    }

    float RoundUpToTwoDecimalPlaces(float number) => Mathf.Ceil(number * 100) / 100;

    protected override void Upgrade()
    {
        base.Upgrade();
    }
}
