using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Units;

public class UpgradeInfoPanel : MonoBehaviour//Test Defender UpgradeMenu 
{
    [SerializeField] private GameObject _upgradeInfoPanel;
    [SerializeField] private TMP_Text _unitNameText;
    [SerializeField] private TMP_Text _progressNumberText;
    [SerializeField] private TMP_Text _attackDamageText;
    [SerializeField] private TMP_Text _attackSpeedText;
    [SerializeField] private TMP_Text _attackRangeText;
    [SerializeField] private TMP_Text _healthPointsText;
    [SerializeField] private TMP_Text _armorText;
    [SerializeField] private TMP_Text _speedByCellText;
    [SerializeField] private Sprite _resoucesSprite;
    [SerializeField] private Image _resoucesImage;
    //[SerializeField] private Sprite _promoteYesSprite;
    //[SerializeField] private Sprite _promoteNoSprite;
    //[SerializeField] private Button _promoteButton;
    [SerializeField] private Button _promoteButtonHaveMoney;
    [SerializeField] private Button _promoteButtonNoMoney;
    [SerializeField] private TMP_Text _priceTextHaveMoney;
    [SerializeField] private TMP_Text _priceTextNoMoney;

    private Unit _unit;
    private UpgradeManager _upgradeManager;
    private LevelEvents _levelEvents;
    private LevelProgressionConfig _currentConfig;
    private Transform _transform;
    private int _price;
    private string _lvlText = "Lvl ";
    private string _progressTextFormat;

    private Quaternion _startRotation;

    private void Start()
    {
        _transform = transform;

        _upgradeManager = UpgradeManager.Instance;
        _levelEvents = LevelEvents.Instance;
        _levelEvents.SubscribeOnUpdateMoney(() => { if (_upgradeInfoPanel.activeInHierarchy) UpdateInfoButton(); });

        _resoucesImage.sprite = _resoucesSprite;
        _promoteButtonHaveMoney.onClick.AddListener(Upgrade);

        _startRotation = _transform.localRotation;
        _transform.rotation = _startRotation;

        _progressTextFormat = _progressNumberText.text;
    }

    void Update()
    {
        _transform.rotation = _startRotation;
    }

    public void Init(Unit unit)
    {
        _currentConfig = unit.UpgradeConfig;
        _unitNameText.text = _currentConfig.GetUnitName;
        _unit = unit;

        _unit.SubscribeOnDeath(CloseUpgradeInfoPanel);
        ActivatingUpgradeMenu();
    }

    public void ActivatingUpgradeMenu()
    {
        _upgradeInfoPanel.SetActive(true);
        UpdateInfoButton();
        SetNextUpgradeParameters();
    }

    private void UpdateInfoButton()
    {
        _price = _currentConfig.GetNextUpgradePriceCoins();
        //_promoteButton.GetComponent<Image>().sprite = _levelManager.IsEnoughMoney(_price) ? _promoteYesSprite : _promoteNoSprite;

        _promoteButtonHaveMoney.gameObject.SetActive(_upgradeManager.IsEnoughMoney(_price));
        _promoteButtonHaveMoney.interactable = _upgradeManager.CanBeUpgraded(_currentConfig);

        _promoteButtonNoMoney.gameObject.SetActive(!_upgradeManager.IsEnoughMoney(_price));

        _priceTextHaveMoney.text = _price.ToString();
        _priceTextHaveMoney.gameObject.SetActive(_upgradeManager.IsEnoughMoney(_price));
        _priceTextNoMoney.text = _price.ToString();
        _priceTextNoMoney.gameObject.SetActive(!_upgradeManager.IsEnoughMoney(_price));
    }

    private void SetNextUpgradeParameters()
    {
        if (_upgradeManager.CanBeUpgraded(_currentConfig))
            _progressNumberText.text = string.Format(_progressTextFormat, (_currentConfig.GetCurrentUpgradeIndex() + 1).ToString(), (_currentConfig.NextIndex() + 1).ToString());
        else
            _progressNumberText.text = _lvlText + (_currentConfig.GetCurrentUpgradeIndex() + 1).ToString();

        var attackCooldown = _currentConfig.GetNextProgress().AttackCooldown;
        var attackSpeed = RoundUpToTwoDecimalPlaces(1 / attackCooldown);
        _attackSpeedText.text = attackSpeed.ToString();
        _attackDamageText.text = _currentConfig.GetNextProgress().Damage.ToString();
        _attackRangeText.text = _currentConfig.GetNextProgress().AttackRange.ToString();
        _healthPointsText.text = _currentConfig.GetNextProgress().MaxHp.ToString();
        _armorText.text = _currentConfig.GetNextProgress().Armor.ToString();
        _speedByCellText.text = _currentConfig.GetNextProgress().Speed.ToString();
    }

    float RoundUpToTwoDecimalPlaces(float number) => Mathf.Ceil(number * 100) / 100;

    private void Upgrade()
    {
        if (!_upgradeManager.IsEnoughMoney(_price) || !_unit.IsInit)
            return;

        _upgradeManager.UpgradeForCoins(_price, _currentConfig.GetNextProgress());
        UpdateInfoButton();
        SetNextUpgradeParameters();
        Debug.Log("UpgradeIndex = " + _currentConfig.GetCurrentUpgradeIndex() + gameObject.name);
    }

    public void CloseUpgradeInfoPanel()
    {
        _upgradeInfoPanel.SetActive(false);
    }
}
