using System;
using UnityEngine;
using Units;

public class TouchMenu : MonoBehaviour
{
    [SerializeField] GameObject _menu;
    [SerializeField] GameObject _waveIsInProgressText_Panel;
    [Header("Defenders settings")]
    [SerializeField] UnitPlacingUI _unitPlacingUI;
    [Header("Hero settings")]
    [SerializeField] GameObject _heroMenuButton;
    private float _delayClosingWaveIsInProgressPanel = 1.0f;
    public UnitPlacingUI UnitPlacingUI => _unitPlacingUI;

    private void Start()
    {
        LevelEvents.Instance.SubscribeOnEndWaitingTimeForWave(() => _menu.SetActive(false));
    }

    public void OpenDefenderMenu(Unit unit)
    {
        if (!WavesManager.IsStartedWaitingTimeForWave)
        {
            ShowWaveIsInProgressText();
            return;
        }
        HeroesManager.Instance.DefenderChangePosition(unit, UnitPlacingUI);
        OpenDefenderUpgradeMenu(unit);
    }

    public void OpenDefenderUpgradeMenu(Unit unit)
    {
        _menu.SetActive(true);
        _menu.GetComponent<DefenderUpgradeMenu>().Init(unit);
    }

    public void ShowHeroMenu(Unit unit)//OnClick HeroMenuButton
    {
        OpenDefenderUpgradeMenu(unit);
    }

    public void ShowButtonHeroMenu()
    {
        if (!WavesManager.IsStartedWaitingTimeForWave)
            return;
        _heroMenuButton.SetActive(!_heroMenuButton.activeSelf);
    }

    public void OpenPropMenu(LevelProgressionConfig levelProgression, Action onStartPlacing = null)
    {
        if (!WavesManager.IsStartedWaitingTimeForWave)
        {
            ShowWaveIsInProgressText();
            return;
        }
        _menu.SetActive(true);
        _menu.GetComponent<UpgradeMenu>().Init(levelProgression);
        onStartPlacing?.Invoke();
    }

    public void ShowWaveIsInProgressText()
    {
        _waveIsInProgressText_Panel.SetActive(true);
        Invoke(nameof(CloseWaveIsInProgressText), _delayClosingWaveIsInProgressPanel);
    }

    private void CloseWaveIsInProgressText() => _waveIsInProgressText_Panel.SetActive(false);
}
