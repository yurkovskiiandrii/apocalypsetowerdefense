using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static Unity.Burst.Intrinsics.X86.Avx;

public class LevelUI : MonoBehaviour
{
    [Range(0.01f, 1f)]
    [SerializeField] private float _progressBarStepSpeed;
    [SerializeField] private TMP_Text _hpText;
    [SerializeField] private TMP_Text _resoucesText;
    [SerializeField] private TMP_Text _timeToNextWaveText;
    [SerializeField] private Slider _wavesProgressBar;
    [SerializeField] private Image _wavePointImagePrefab;
    [Header("Panels")]
    [SerializeField] private GameObject _winPanel;
    [SerializeField] private GameObject _losePanel;
    [SerializeField] private GameObject _rewardPanel;
    //[SerializeField] private GameObject _invadersWavePanel;
    [SerializeField] private GameObject _defenderDeadPanel;
    [SerializeField] private GameObject _infoSpawnPointPanel;
    [SerializeField] private GameObject _timeToNextWavePanel;
    [SerializeField] private GameObject _waveIsInProgressText_Panel;

    private RectTransform _progressBarHandle;
    private Action _onExitUpgradePanel;
    private float _stepByWave;
    private float[] _stepsByWaves;
    private int[] _invadersByWaveCount;
    private int _currentWave = 0;
    private int _invadersByWaveDead = 0;
    private string _nextLevelName;
    private string _timeToNextWaveTextFormat;
    private float _delayClosingWaveIsInProgressPanel = 1.0f;

    public void Init(int hpCount, int resourcesCount, List<SpawnPoint> spawnPoints, int wavesCount, string nextLevelName)
    {
        _hpText.text = hpCount.ToString();
        _resoucesText.text = resourcesCount.ToString();
        _stepsByWaves = new float[wavesCount];

        _nextLevelName = nextLevelName;

        _progressBarHandle = _wavesProgressBar.handleRect;

        _timeToNextWaveTextFormat = _timeToNextWaveText.text;

        CalculateWavesBar(spawnPoints, wavesCount);
    }

    public void ShowTimeToNextWave(float time)
    {
        _timeToNextWaveText.text = string.Format(_timeToNextWaveTextFormat, time.ToString());
    }

    public void ShowTimeToNextWavePanel(bool show)
    {
        _timeToNextWavePanel.SetActive(show);
    }

    private void CalculateWavesBar(List<SpawnPoint> spawnPoints, int wavesCount)
    {
        int invadersCount = 0;
        _invadersByWaveCount = new int[wavesCount];
        for (int i = 0; i < wavesCount; i++)
            _invadersByWaveCount[i] = 0;

        foreach (SpawnPoint spawnPoint in spawnPoints)
            foreach (var waveFormations in spawnPoint.WavesFormations)
                foreach (var timeFormation in waveFormations.timeFormations)
                {
                    invadersCount += timeFormation.formation.InvadersCount;
                    _invadersByWaveCount[waveFormations.wave - 1] += timeFormation.formation.InvadersCount;
                }

        _stepByWave = _wavesProgressBar.maxValue / invadersCount;

        _wavesProgressBar.value = 0;
        float progressBarPartByWaves = _wavesProgressBar.maxValue / wavesCount;
        for (int i = 0; i < wavesCount; i++)

        {
            _stepsByWaves[i] = progressBarPartByWaves / _invadersByWaveCount[i];

            _wavesProgressBar.value += progressBarPartByWaves;

            Image wavePoint = Instantiate(_wavePointImagePrefab, _wavesProgressBar.transform);
            _wavesProgressBar.handleRect = wavePoint.GetComponent<RectTransform>();
            _wavesProgressBar.handleRect = _progressBarHandle;
            _progressBarHandle.SetSiblingIndex(_wavesProgressBar.transform.childCount - 1);
        }
        _wavesProgressBar.value = 0;
    }

    private void OldCalculateWavesBar(List<SpawnPoint> spawnPoints, int wavesCount)
    {
        int invadersCount = 0;
        _invadersByWaveCount = new int[wavesCount];
        for (int i = 0; i < wavesCount; i++)
            _invadersByWaveCount[i] = 0;

        foreach (SpawnPoint spawnPoint in spawnPoints)
            foreach (var waveFormations in spawnPoint.WavesFormations)
                foreach (var waveFormation in waveFormations.timeFormations)
                {
                    invadersCount += waveFormation.formation.InvadersCount;
                    _invadersByWaveCount[waveFormations.wave - 1] += waveFormation.formation.InvadersCount;
                }

        _stepByWave = _wavesProgressBar.maxValue / invadersCount;

        //print("all invaders count: " + invadersCount);
        //print("invaders count by waves");
        _wavesProgressBar.value = 0;
        for (int i = 0; i < wavesCount; i++)
        {
            _wavesProgressBar.value += _stepByWave * _invadersByWaveCount[i];
            Image wavePoint = Instantiate(_wavePointImagePrefab, _wavesProgressBar.transform);
            _wavesProgressBar.handleRect = wavePoint.GetComponent<RectTransform>();
            _wavesProgressBar.handleRect = null;
            //print("wave[" + i + "]: " + invadersByWaveCount[i]);
        }
        _wavesProgressBar.value = 0;
    }

    public void UpdateHp(int newHpCount) => _hpText.text = newHpCount.ToString();

    public void UpdateResources(int newResourcesCount) => _resoucesText.text = newResourcesCount.ToString();

    public void WaveProgressbarStep() => _wavesProgressBar.value += _stepByWave;

    public void ProgressbarStepByWave()
    {
        //_wavesProgressBar.value += _stepsByWaves[_currentWave];
        StartCoroutine(ProgressbarStepCoroutine());
        _invadersByWaveDead++;
        if (_invadersByWaveDead >= _invadersByWaveCount[_currentWave])
        {
            _invadersByWaveDead = 0;
            _currentWave++;
        }
    }

    private IEnumerator ProgressbarStepCoroutine()
    {
        float result = _wavesProgressBar.value + _stepsByWaves[_currentWave];
        float newStep = _stepsByWaves[_currentWave] * _progressBarStepSpeed;
        float partValue = _wavesProgressBar.value;
        while (partValue < result)
        {
            _wavesProgressBar.value += newStep;
            partValue += newStep;
            yield return new WaitForSeconds(0.1f);
        }
    }
    public void ShowResourcesPanel(bool show) => _rewardPanel.SetActive(show); 

    public void OpenWinPanel() => _winPanel.SetActive(true);

    public void OpenLosePanel() => _losePanel.SetActive(true);

    //public void ShowInvadersWavePanel(bool show) => _invadersWavePanel.SetActive(show);

    public void ShowDefenderDeadPanel(bool show) => _defenderDeadPanel.SetActive(show);

    public void ShowWaveIsInProgressTextPanel(Transform transform)
    {
        CancelInvoke(nameof(CloseWaveIsInProgressTextPanel));

        _waveIsInProgressText_Panel.transform.SetParent(transform, false);
        _waveIsInProgressText_Panel.SetActive(true);

        Invoke(nameof(CloseWaveIsInProgressTextPanel), _delayClosingWaveIsInProgressPanel);
    }

    private void CloseWaveIsInProgressTextPanel() => _waveIsInProgressText_Panel.SetActive(false);

    public void OpenInfoSpawnPointPanel(SpawnPoint spawnPoint)//onClick Event SpawnPoint
    {
        _infoSpawnPointPanel.SetActive(true);
        _infoSpawnPointPanel.GetComponent<SpawnPointGameUI>().Init(spawnPoint);
    }

    public void CloseInfoSpawnPointPanel()
    {
        _infoSpawnPointPanel.GetComponent<SpawnPointGameUI>().CloseInfoSpawnPointPanel();
    }

    public void RestartLevel()
    {
        Debug.Log("Restart LEVEL");
        string sceneName = SceneManager.GetActiveScene().name;
        DOTween.Clear();
        SceneManager.LoadScene(sceneName);
        Time.timeScale = 1;
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(_nextLevelName);
        Time.timeScale = 1;
    }
}
