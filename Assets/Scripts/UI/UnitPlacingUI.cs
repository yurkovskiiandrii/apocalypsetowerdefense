using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UnitPlacingUI : MonoBehaviour
{
    [SerializeField] private Button _placeButton;
    [SerializeField] private Button _cancelButton;
    [Header("Menu Placing and Upgrade")]
    [SerializeField] private TouchMenu _touchMenu;

    public void OnStartPlacing(UnityAction onPlaceButtonClick, UnityAction onCancelButtonClick)
    {
        _placeButton.onClick.RemoveAllListeners();
        _cancelButton.onClick.RemoveAllListeners();

        _placeButton.onClick.AddListener(() => onPlaceButtonClick?.Invoke());
        _cancelButton.onClick.AddListener(() => onCancelButtonClick?.Invoke());
    }
}
