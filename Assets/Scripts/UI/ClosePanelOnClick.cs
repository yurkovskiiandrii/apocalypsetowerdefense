using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Units;

public class ClosePanelOnClick : MonoBehaviour
{
    [SerializeField] private string _panelTag;
    [SerializeField] private UnityEvent _onClose;
    [Header("Prop Properties")]
    [SerializeField] protected LayerMask _propsLayer;
    [SerializeField] protected Prop _prop;
    [Header("Defender Properties")]
    [SerializeField] private LayerMask _unitLayer;
    [SerializeField] protected Unit _defender;

    private Camera _camera;
    private PointerEventData _pointerEventData;

    void Start()
    {
        _camera = Camera.main;
        _pointerEventData = new PointerEventData(EventSystem.current);
    }

    void Update()
    {
        TouchHandler();
    }

    private void TouchHandler()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var mousePositionToWorldPoint = _camera.ScreenToWorldPoint(Input.mousePosition);

            if (Physics.Raycast(mousePositionToWorldPoint, _camera.transform.forward, out RaycastHit hit, 100f, _propsLayer))
            {
                if (hit.transform.parent.parent.gameObject == _prop.gameObject)
                    return;
            }

            if (Physics.Raycast(mousePositionToWorldPoint, _camera.transform.forward, out hit, 100f, _unitLayer))
            {
                if (hit.transform.gameObject == _defender.gameObject)
                    return;
            }

            if (IsMouseOverUI(Input.mousePosition))
                return;

            _onClose?.Invoke();
            gameObject.SetActive(false);
        }
    }

    private bool IsMouseOverUI(Vector3 mousePosition)
    {
        if (string.IsNullOrEmpty(_panelTag))
            return false;
        _pointerEventData.position = mousePosition;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(_pointerEventData, raycastResults);

        foreach (RaycastResult result in raycastResults)
        {
            if (result.gameObject.CompareTag(_panelTag))
                return true;
        }
        return false;
    }
}
