using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemSpawnPoint : MonoBehaviour
{
    [SerializeField] private Image _icon;
    [SerializeField] private TMP_Text _invadersCount;
    //[SerializeField] private TMP_Text _totalForce;

    public void Init(Sprite spriteIcon, int invadersCount)//(int totalForce, Sprite spriteIcon, int invadersCount)
    {
        _icon.sprite = spriteIcon;
        _invadersCount.text = invadersCount.ToString();

        //_totalForce.text = totalForce.ToString();
    }
}
