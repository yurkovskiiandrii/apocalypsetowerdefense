using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Units;

public class SpawnPointGameUI : MonoBehaviour
{
    [SerializeField] private Button _closeButton;
    [SerializeField] private Transform _itemSpawnPointParent;
    [SerializeField] private GameObject _itemSpawnPointPrefab;
    [SerializeField] private TMP_Text _totalForceContText;

    private void Awake()
    {
        _closeButton.onClick.AddListener(CloseInfoSpawnPointPanel);
    }

    public void Init(SpawnPoint spawnPoint)
    {
        if (spawnPoint.WavesFormations.Count == 0)
        {
            CloseInfoSpawnPointPanel();
            return;
        }

        DestroyInfoPanels();

        List<TimeFormations> waveTimeFormations = spawnPoint.WavesFormations.Find(x => x.wave == spawnPoint.CurrentWave + 1).timeFormations;

        if (waveTimeFormations == null || waveTimeFormations.Count == 0)
        {
            CloseInfoSpawnPointPanel();
            return;
        }

        var invadersWave = new List<Unit>();
        var invadersAllFormation = new List<Unit>();
        var totalForce = 0;

        foreach (var item in waveTimeFormations)
        {
            var invadersFormation = item.formation.GetInvadersList();
            foreach (Unit invader in invadersFormation)
            {
                invadersAllFormation.Add(invader);
            }
        }

        foreach (Unit invader in invadersAllFormation)
        {
            if (invadersWave.Exists(x => x.Name == invader.Name))
                continue;

            invadersWave.Add(invader);

            var infoInvader = GetCountAndTotalForceInvader(invadersAllFormation, invader.Name);
            totalForce += infoInvader.totalForce;
            var countInvader = infoInvader.count;

            var infoPanel = Instantiate(_itemSpawnPointPrefab, _itemSpawnPointParent).GetComponent<ItemSpawnPoint>();
            infoPanel.Init(invader.SpriteInvader, countInvader);
        }
        _totalForceContText.text = totalForce.ToString();
    }

    public (int totalForce, int count) GetCountAndTotalForceInvader(List<Unit> invaders, string name)
    {
        var totalForce = 0;
        var count = 0;

        for (int i = 0; i < invaders.Count; i++)
        {
            if (invaders[i].Name == name)
            {
                totalForce += invaders[i].ForceInvader;
                count++;
            }
        }
        return (totalForce, count);
    }

    public void CloseInfoSpawnPointPanel()
    {
         var levelCamera = LevelCamera.Instance;

        levelCamera.EnableMove();
        levelCamera.EnableZoom();

        gameObject.SetActive(false);
    }

    private void DestroyInfoPanels()
    {
        foreach (Transform child in _itemSpawnPointParent)
        {
            Destroy(child.gameObject);
        }
    }
}
