using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TimeScaleManager
{
    public class TimeManager : MonoBehaviour
    {
        [SerializeField] private Button _accelerationTimeButton;
        [SerializeField] private TextMeshProUGUI _nextTimeScaleText;
        [SerializeField] private TextMeshProUGUI _currentTimeScaleText;
        [SerializeField] private float[] _timeScales = { 1, 2, 4, 8 };
        private string _nextTimeScaleTextFormat;
        private string _currentTimeScaleTextFormat;
        private float _currentTimeScale;
        private float _nextTimeScale;
        private int _currentIndex = 0;
        //private const float _timeScale1 = 1;
        //private const float _timeScale2 = 2;
        //private const float _timeScale4 = 4;
        //private const float _timeScale8 = 8;

        void Start()
        {
            _nextTimeScaleTextFormat = _nextTimeScaleText.text;
            _currentTimeScaleTextFormat = _currentTimeScaleText.text;

            _accelerationTimeButton.onClick.AddListener(ToggleAccelerationTime);

            _currentIndex = 0;
            ToggleAccelerationTime();
        }

        private void ToggleAccelerationTime()
        {
            _currentTimeScale = _timeScales[_currentIndex];
            _nextTimeScale = GetNextTimeScale();//GetTimeScale(_currentTimeScale);
            Time.timeScale = _currentTimeScale;
            TimeScaleText();
        }
      
        private float GetNextTimeScale()
        {
            _currentIndex = (_currentIndex + 1) % _timeScales.Length;
            return _timeScales[_currentIndex];
        }

        private void TimeScaleText()
        {
            _nextTimeScaleText.text = string.Format(_nextTimeScaleTextFormat, _nextTimeScale);
            _currentTimeScaleText.text = string.Format(_currentTimeScaleTextFormat, _currentTimeScale);
        }

        //private float GetTimeScale(float timeScale) => timeScale switch
        //{
        //    _timeScale1 => _timeScale2,
        //    _timeScale2 => _timeScale4,
        //    _timeScale4 => _timeScale8,
        //    _timeScale8 => _timeScale1,
        //    _ => _timeScale1
        //};

    }
}
