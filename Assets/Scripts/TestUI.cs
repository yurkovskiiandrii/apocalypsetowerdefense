using UnityEngine.UI;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class TestUI : SingletonComponent<TestUI>
{
    [SerializeField] private GameObject _firstContainer;
    [SerializeField] private GameObject _secondContainer;

    [SerializeField] private Button _placeButton;
    [SerializeField] private Button _cancelButton;

    [SerializeField] private Button _playButton;

    public void OnStartGame(Action onPlayButtonClick)
    {
        _playButton.onClick.RemoveAllListeners();
        _playButton.onClick.AddListener(() => onPlayButtonClick?.Invoke());
    }

    public void OnStartPlacing(Action onPlaceButtonClick, Action onCancelButtonClick)
    {
        _firstContainer.SetActive(false);
        _secondContainer.SetActive(true);

        _placeButton.onClick.RemoveAllListeners();
        _cancelButton.onClick.RemoveAllListeners();

        _placeButton.onClick.AddListener(() => onPlaceButtonClick?.Invoke());
        _cancelButton.onClick.AddListener(() => onCancelButtonClick?.Invoke());
    }

    public void OnEndPlacing()
    {
        _firstContainer.SetActive(true);
        _secondContainer.SetActive(false);
    }

    public void OnRestartButtonClick()
    {
        var currentSceneName = SceneManager.GetActiveScene().name;

        SceneManager.LoadScene(currentSceneName);
    }
}
