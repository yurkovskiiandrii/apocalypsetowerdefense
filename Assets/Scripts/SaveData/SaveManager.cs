
using System.Collections.Generic;
using UnityEngine;

public class SaveManager
{
    private SaveManager() { }

    private static SaveManager _instance;

    private SaveDataJson _saveDataJson = new SaveDataJson();

    private LevelEvents _levelEvents;

    private RewardsData _rewardsData;

    public static SaveManager GetInstance()
    {
        if (_instance == null)
            _instance = new SaveManager();

        return _instance;
    }

    public void Init(LevelRewardsConfig resourcesCollectionConfig)
    {
        _levelEvents = LevelEvents.Instance;
        LoadData(resourcesCollectionConfig);
    }

    public void Save()
    {
        _saveDataJson.Save(_rewardsData);
    }

    public void SetResourceCount(string nameResource, int number)
    {
        if (_rewardsData.rewards.Exists(x => x.nameResource == nameResource))
            _rewardsData.rewards[GetResourcesIndex(nameResource)].count += number;
        else
            _rewardsData.rewards.Add(new ResourceData(nameResource, number));
        MonoBehaviour.print("Added Game Resources : " + nameResource + " / count: " + number);

        Save();
    }

    public List<ResourceData> GetRewards()
    {
        return _rewardsData.rewards;
    }

    public int GetResourceCount(string nameResource)
    {
        int number = 0;

        var resourceData = _rewardsData.rewards.Find(x => x.nameResource == nameResource);
        if (resourceData != null)
            number = resourceData.count;

        return number;
    }

    public int GetResourcesIndex(string nameResource)
    {
        return _rewardsData.rewards.IndexOf(_rewardsData.rewards.Find(x => x.nameResource == nameResource));
    }

    private RewardsData SetNewResourcesDatas(LevelRewardsConfig rewardsCollectionConfig)
    {
        RewardsData newResourcesDatas = new RewardsData();

        //test
        //foreach (var gameResources in rewardsCollectionConfig.rewards[0].gameResources)
        //    newResourcesDatas.rewards.Add(new ResourceData(gameResources.nameResource));

        return newResourcesDatas;
    }

    private void LoadData(LevelRewardsConfig resourcesCollectionConfig)
    {
        _rewardsData = _saveDataJson.Load<RewardsData>() ?? SetNewResourcesDatas(resourcesCollectionConfig);
        //Save();//test
    }
}
