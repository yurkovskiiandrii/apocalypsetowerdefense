using System.Collections.Generic;

[System.Serializable]

public class RewardsData
{
    public List <ResourceData> rewards;

    public RewardsData()
    {
        rewards = new List<ResourceData>();
    }
}

[System.Serializable]

public class ResourceData
{
    public string nameResource;
    public int count;

    public ResourceData(string nameResources, int count = 0)
    {
        this.nameResource = nameResources;
        this.count = count;
    }

    public ResourceData() { }
}
