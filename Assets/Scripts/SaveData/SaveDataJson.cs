using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public class SaveDataJson
{
    public void Save<T>(T accessToken)
    {
        string filePath = GenerateFilePath<T>();
        File.WriteAllText(filePath, JsonConvert.SerializeObject(accessToken));
        MonoBehaviour.print("SaveDataJson / Game Resources : " + File.ReadAllText(filePath));
    }

    public T Load<T>()
    {
        string filePath = GenerateFilePath<T>();

        if (!File.Exists(filePath))
            return default(T);
        
        MonoBehaviour.print("SaveDataJson / Game Resources : " + File.ReadAllText(filePath));
        return JsonConvert.DeserializeObject<T>(File.ReadAllText(filePath));
    }

    string GenerateFilePath<T>() => Application.persistentDataPath + "/" + typeof(T) + ".json";

    //save PlayerPrefs
    
    //public void Save<T>(T accessToken)
    //{
    //    string textPath = JsonConvert.SerializeObject(accessToken, Formatting.None);
    //    PlayerPrefs.SetString("Save/" + typeof(T), textPath);
    //}
    //
    //public T Load<T>()
    //{
    //    if (!PlayerPrefs.HasKey("Save/" + typeof(T)))
    //        return default(T);

    //    string filePath = PlayerPrefs.GetString("Save/" + typeof(T));
    //    return JsonConvert.DeserializeObject<T>(filePath);
    //}
}


